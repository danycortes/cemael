﻿<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- Begin Head -->
    <head>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cemael-Libros</title>
        <meta name="keywords" content="HTML5 Theme" />
        <meta name="description" content="Cemael">
        <meta name="author" content="cwa.mx">

        <!-- Web Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">

        <!-- Vendor Styles -->
        <link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/animate.min.css");?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/themify/themify.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/scrollbar/scrollbar.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/magnific-popup/magnific-popup.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/swiper/swiper.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/cubeportfolio/css/cubeportfolio.min.css");?>"  rel="stylesheet" type="text/css"/>

        <!-- Theme Styles -->
        <link href="<?php echo base_url("assets/css/style.css");?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/global.css");?>" rel="stylesheet" type="text/css"/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    </head>
    <!-- End Head -->

    <!-- Body -->
    <body>

        <!--========== HEADER ==========-->
             <!--========== HEADER ==========-->
        <header class="navbar-fixed-top s-header js__header-sticky js__header-overlay">
            <!-- Navbar -->
            <div class="s-header__navbar">
                <div class="s-header__container">
                    <div class="s-header__navbar-row">
                        <div class="s-header__navbar-row-col">
                            <!-- Logo -->
                            <div class="s-header__logo">
                                <a href="" class="s-header__logo-link">
                                    <img class="s-header__logo-img s-header__logo-img-default" src="<?php echo base_url("assets/images/logo.png");?>" alt="Cemael Logo">
                                    <img class="s-header__logo-img s-header__logo-img-shrink" src="<?php echo base_url("assets/images/logo-dark.png");?>" alt="Cemael Logo">
                                </a>
                            </div>
                            <!-- End Logo -->
                        </div>
                        <div class="s-header__navbar-row-col">
                            <!-- Trigger -->
                            <!-- <a href="javascript:void(0);" class="s-header__trigger js__trigger">
                                <span class="s-header__trigger-icon"></span>
                                <svg x="0rem" y="0rem" width="3.125rem" height="3.125rem" viewbox="0 0 54 54">
                                    <circle fill="transparent" stroke="#fff" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
                                </svg>
                            </a> -->
                            <!-- End Trigger -->
                        </div>
			<div >
						
						<br><br><a class="text-uppercase g-font-size-600--xs g-font-weight--700 g-color--orange g-letter-spacing--2 " href="<?php echo base_url() ?>">Regresar</a>   
						
						</div>
                    </div>
                </div>
            </div>
            <!-- End Navbar -->

            
        </header>

           
        <!--========== END HEADER ==========-->


     
      
   
        <!-- End Portfolio -->

        <!-- Testimonials -->
        <div id="Libros" class="js__parallax-window" style="background: url(assets/images/slider/Imagen3.jpg) 40% 0 no-repeat fixed;">
            <div class="container g-text-center--xs g-padding-y-10--xs g-padding-y-125--sm">
                <br><br><br>
                <p class=" g-font-size-32--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-50--xs">Libros</p>
				
			   <div class="s-swiper js__swiper-testimonials">
                    <!-- Swiper Wrapper -->
                    <div class="swiper-wrapper g-margin-b-10--xs">
						<div class="swiper-slide g-padding-x-10--sm g-padding-x-150--lg">
                            <div class="g-padding-x-10--xs g-padding-x-50--lg">
							
                             <div class="g-margin-b-40--xs">
							 <img class="photol" src="assets/images/libros/2.jpg" alt="Image">
							 <br>
                                    <p class="g-font-size-22--xs g-font-size-28--sm g-color--white">" Viktor E. Frankl. Una biografía "</p>
                                </div>
                                <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                                <h3 class="g-font-size-15--xs g-font-size-18--sm g-letter-spacing--1"><a data-toggle="modal" data-target="#l2" >Leer Más</a></h3>
                            
							</div>
                        </div>
					
                        <div class="swiper-slide g-padding-x-10--sm g-padding-x-150--lg">
                            <div class="g-padding-x-10--xs g-padding-x-50--lg">
							
								<div class="g-margin-b-40--xs">
								<img class="photol" src="assets/images/libros/1.jpg" alt="Image">
								<br>
                                    <p class="g-font-size-22--xs g-font-size-28--sm g-color--white">" Vivir con Sentido " </p>
                                </div>
                                <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                               <!--  <h4 class="g-font-size-15--xs g-font-size-18--sm g-color--white-opacity-light g-margin-b-5--xs"><a data-toggle="modal" data-target="#myModal2" >Leer Más</a></h4> -->
								<h3 class="g-font-size-15--xs g-font-size-18--sm g-letter-spacing--1"><a data-toggle="modal" data-target="#l1" >Leer Más</a></h3>
                            
							</div>
                        </div>
                        
						<div class="swiper-slide g-padding-x-10--sm g-padding-x-150--lg">
                            <div class="g-padding-x-10--xs g-padding-x-50--lg">
							
                             <div class="g-margin-b-40--xs">
							 <br>
							 <img class="photol" src="assets/images/libros/3.jpg" alt="Image">
							 <br>
                                    <p class="g-font-size-22--xs g-font-size-28--sm g-color--white">" Vivir la propia vida "</p>
                                </div>
                                <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                                <h3 class="g-font-size-15--xs g-font-size-18--sm g-letter-spacing--1"><a data-toggle="modal" data-target="#l3" >Leer Más</a></h3>
                            
							</div>
                        </div>
						<div class="swiper-slide g-padding-x-10--sm g-padding-x-150--lg">
                            <div class="g-padding-x-10--xs g-padding-x-50--lg">
							
                             <div class="g-margin-b-40--xs">
							 <br>
							 <img class="photol" src="assets/images/libros/4.jpg" alt="Image">
							 <br>
                                    <p class="g-font-size-22--xs g-font-size-28--sm g-color--white">" European Psychotherapy "</p>
                                </div>
                                <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                                <h3 class="g-font-size-15--xs g-font-size-18--sm g-letter-spacing--1"><a data-toggle="modal" data-target="#l4" >Leer Más</a></h3>
                            
							</div>
                        </div>
						<div class="swiper-slide g-padding-x-10--sm g-padding-x-150--lg">
                            <div class="g-padding-x-10--xs g-padding-x-50--lg">
							
                             <div class="g-margin-b-40--xs">
							 <br>
							 <img class="photol" src="assets/images/libros/5.jpg" alt="Image">
							 <br>
                                    <p class="g-font-size-22--xs g-font-size-28--sm g-color--white">" Manual de la Escala Existencial "</p>
                                </div>
                                <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                                <h3 class="g-font-size-15--xs g-font-size-18--sm g-letter-spacing--1"><a data-toggle="modal" data-target="#l5" >Leer Más</a></h3>
                            
							</div>
                        </div>
						<div class="swiper-slide g-padding-x-10--sm g-padding-x-150--lg">
                            <div class="g-padding-x-10--xs g-padding-x-50--lg">
							
								<div class="g-margin-b-40--xs">
								<img class="photol" src="assets/images/libros/6.jpg" alt="Image">
								<br>
                                    <p class="g-font-size-22--xs g-font-size-28--sm g-color--white">" El hombre en busca de sentido " </p>
                                </div>
                                <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                               <!--  <h4 class="g-font-size-15--xs g-font-size-18--sm g-color--white-opacity-light g-margin-b-5--xs"><a data-toggle="modal" data-target="#myModal2" >Leer Más</a></h4> -->
								<h3 class="g-font-size-15--xs g-font-size-18--sm g-letter-spacing--1"><a data-toggle="modal" data-target="#l6" >Leer Más</a></h3>
                            
							</div>
                        </div>
						<div class="swiper-slide g-padding-x-10--sm g-padding-x-150--lg">
                            <div class="g-padding-x-10--xs g-padding-x-50--lg">
							
								<div class="g-margin-b-40--xs">
								<img class="photol" src="assets/images/libros/7.jpg" alt="Image">
								<br>
                                    <p class="g-font-size-22--xs g-font-size-28--sm g-color--white">" Psicoanálisis y Existencialismo " </p>
                                </div>
                                <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                               <!--  <h4 class="g-font-size-15--xs g-font-size-18--sm g-color--white-opacity-light g-margin-b-5--xs"><a data-toggle="modal" data-target="#myModal2" >Leer Más</a></h4> -->
								<h3 class="g-font-size-15--xs g-font-size-18--sm g-letter-spacing--1"><a data-toggle="modal" data-target="#l7" >Leer Más</a></h3>
                            
							</div>
                        </div>
						<!-- <div class="swiper-slide g-padding-x-10--sm g-padding-x-150--lg">
                            <div class="g-padding-x-10--xs g-padding-x-50--lg">
							
								<div class="g-margin-b-40--xs">
								<img class="photol" src="assets/images/libros/8.jpg" alt="Image">
								<br>
                                    <p class="g-font-size-22--xs g-font-size-28--sm g-color--white">" Las Psicoterapias Existenciales " </p>
                                </div>
                                <div class="center-block g-hor-divider__solid--white-opacity-lightest g-width-100--xs g-margin-b-30--xs"></div>
                              
								<h3 class="g-font-size-15--xs g-font-size-18--sm g-letter-spacing--1"><a data-toggle="modal" data-target="#l8" >Leer Más</a></h3>
                            
							</div>
                        </div> -->
                       
						
                    </div>
                    <!-- End Swipper Wrapper -->

                    <!-- Arrows -->
                    <div class="g-font-size-22--xs g-color--white-opacity js__swiper-fraction"></div>
                   <!--  <a href="javascript:void(0);" class="g-display-none--xs g-display-inline-block--sm s-swiper__arrow-v1--right s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-right js__swiper-btn--next"></a>
                    <a href="javascript:void(0);" class="g-display-none--xs g-display-inline-block--sm s-swiper__arrow-v1--left s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-left js__swiper-btn--prev"></a>
                     -->
				<!--	<a href="javascript:void(0);" class="s-swiper__arrow-v1--right s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-right js__swiper-btn--next"></a>
					<a href="javascript:void(0);" class="s-swiper__arrow-v1--left s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-left js__swiper-btn--prev"></a>
					-->
					<a href="javascript:void(0);" class="s-swiper__arrow-v1--right s-icon s-icon--md s-icon--white-brd g-radius--circle js__swiper-btn--next">></a>
				   <a href="javascript:void(0);" class="s-swiper__arrow-v1--left s-icon s-icon--md s-icon--white-brd g-radius--circle  js__swiper-btn--prev"><</a>
					<!-- End Arrows -->
                </div>
            </div>
			
			
				 
				  
				  <!-- Modal l2-->
				  <div class="modal fade" id="l2" role="dialog">
					<div class="modal-dialog " >
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="g-font-size-25--xs">Viktor E. Frankl. Una biografía</h3>
						
						
						</div>
					    <div class="scroll">
						<div class="modal-body">
						  
								<div id="form1">
									<div class="container">

									  <form class="form-signin5">
									  
										<div id="form">
										<form action='#' id="myForm" method='post' name="myForm"> <!-- -->
									
											<div class="g-media__body g-padding-x-10--xs">
											  <br><br>
												   <h3 class="g-font-size-20--xs">Por Alfried Längle.Edit Herder. Barcelona</h3><br><br><br>
												   <p class="g-margin-b-0--xs justify" >Su vida, su obra, su trascendencia.Fue el fundador de la «tercera escuela de psicoterapia de Viena», sobrevivió al holocausto y rechazó !a teoría de la culpa colectiva. El sufrimiento, la desesperación y la voluntad de sentido han sido los temas principales de su labor terapéutica, de sus conferencias y publicaciones, entre las que destaca El hombre en busca de sentido. El psicoterapeuta austríaco Alfried Lángle, que fue durante muchos años estrecho colaborador de Frankl, ha escrito un retrato conmovedor de la persona de Viktor Frankl y un sabio análisis de su legado científico.</p>
												   
											</div>
										 </form> 
										</div>
									 
									 </form> 
								 </div>	 
								</div> <!-- /container -->
						  
						</div>
						 <div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					  </div><!-- end scroll-->
					  </div>
					  
					</div>
				  </div>
				  <!-- end modal-->
				   <!-- Modal l1-->
				  <div class="modal fade" id="l1" role="dialog">
					<div class="modal-dialog " >
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="g-font-size-25--xs">Vivir con Sentido</h3>
						
						
						</div>
						<div class="scroll">
						<div class="modal-body">
						  
								<div id="form1">
									<div class="container">

									  <form class="form-signin4">
									  
										<div id="form">
										<form action='#' id="myForm" method='post' name="myForm"> <!-- -->
									
											<div class="g-media__body g-padding-x-10--xs">
											  
											<br><br>
												   <h3 class="g-font-size-20--xs">Por Alfried Längle. Edit Lumen. Buenos Aires.</h3><br><br>
												   <p class="g-margin-b-0--xs justify" >De todas las preguntas del ser humano, la más significativa y de mayor trascendencia es “¿para qué?”. Se concentra en ella la esencia del ser humano. En esta pregunta, culmina la búsqueda del espíritu. El sentido es la respuesta a la ardiente pregunta: ¿para qué vivir? El ser humano no quiere “dejarse vivir” apáticamente, con una pasividad ciega: quiere saber y sentir para qué está, para qué hacer algo. El Análisis Existencial es el análisis hacia una vida digna de ser vivida. El presente libro ofrece un resumen integral del conocimiento del sentido, tomado de todas las obras alemanas de Viktor Frankl, para una aplicación en el ámbito no terapéutico, extraclínico. Este libro no pretende enseñar, quiere demostrar. Dado que se ocupa del sentido, se trata de una confrontación personal con la propia vida, con la vida concreta, pues más allá del sufrimiento, la felicidad sólo puede estar en el hallazgo del sentido de cada momento.</p>
												   
											</div>
										 </form> 
										</div>
									 
									 </form> 
								 </div>	 
								</div> <!-- /container -->
						  
						</div>
						 <div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
                       </div><!-- end scroll-->
					  </div>
					  
					</div>
				  </div>
				  <!-- end modal-->
				  
				  <!-- Modal l3-->
				  <div class="modal fade" id="l3" role="dialog">
					<div class="modal-dialog " >
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="g-font-size-25--xs">Vivir la propia vida</h3>
						
						
						</div>
						<div class="scroll">
						<div class="modal-body">
						  
								<div id="form1">
									<div class="container">

									  <form class="form-signin5">
									  
										<div id="form">
										<form action='#' id="myForm" method='post' name="myForm"> <!-- -->
									
											<div class="g-media__body g-padding-x-10--xs">
											  <br><br>
												   <p class="g-margin-b-0--xs justify" >Este libro es una introducción al Análisis Existencial en una aproximación cercana a la praxis.El Análisis Existencial es un enfoque de psicoterapia, pero también una forma de mirar y comprender la vida desde una perspectiva personal y dialogal, donde las preguntas son claves (llaves) que abren las puertas al mundo del otro, a veces desconocido para la propia persona que es preguntada. Usted, como lector, puede seguir los casos individuales y así también encontrar caminos para su propia vida. Los autores son psicoterapeutas analítico-existenciales, con acentos diferentes en su propia praxis. Lo puedes adquirir en la librería Qué Leo en Santiago Centro, Merced 453, Feria Chilena del Libro o escribiendo a comunicaciones@icae.cl</p>
												   
											</div>
										 </form> 
										</div>
									 
									 </form> 
								 </div>	 
								</div> <!-- /container -->
						  
						</div>
						 <div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
						 </div><!-- end scroll-->
					  </div>
					  
					</div>
				  </div>
				  <!-- end modal-->
				  				  <!-- Modal l4-->
				  <div class="modal fade" id="l4" role="dialog">
					<div class="modal-dialog " >
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header g-media__body g-padding-x-35--xs">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="g-font-size-25--xs">European Psychotherapy</h3>
						
						
						</div>
						<div class="modal-body">
						  
								<div id="form1">
									<div class="container">

									  <form class="form-signin2 g-media__body g-padding-x-20--xs">
									  
										<!-- <div id="form"> -->
										<form action='#' id="myForm" method='post' name="myForm"> <!-- -->
									
											<div class="g-media__body g-padding-x-0--xs">
											  <br><br>
												   <h3 class="g-font-size-20--xs">CIP-Meiden</h3><br><br><br>
												   <p class="g-margin-b-0--xs justify" >Revista cientifica de Psicoterapia Europea para la investigacion Psicoterapeutica y la Practica.</p>
												   
											</div>
										 </form> 
										<!-- </div> -->
									 
									 </form> 
								 </div>	 
								</div> <!-- /container -->
						  
						</div>
						 <div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>

					  </div>
					  
					</div>
				  </div>
				  <!-- end modal-->
				   <!-- Modal l5-->
				  <div class="modal fade" id="l5" role="dialog">
					<div class="modal-dialog " >
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
							  <h3 class="g-font-size-25--xs">Manual de la Escala Existencial</h3>
						</div>
						<div class="scroll">
						<div class="modal-body">
						  
								<div id="form1">
									<div class="container">

									  <form class="form-signin3">
									  
										<div id="form">
										<form action='#' id="myForm" method='post' name="myForm"> <!-- -->
										<div class="g-media__body g-padding-x-10--xs">
										
											   <h3 class="g-font-size-20--xs">Por Andrés Gottfried</h3>
											   <p class="g-margin-b-0--xs justify" > La Escala Existencial (EE) es una técnica creada y validada en Austria por Längle, Orgler y Kundi (2000) que se basa en la teoría de Viktor Frankl y en el “Método de hallazgo de Sentido” de Längle (1986) que permite dividir y ordenar en 4 pasos el hallazgo del sentido: percibir, valorar, decidir y actuar.
																					La Escala Existencial contiene 46 ítems que investiga los cuatro pasos antes descritos, por ello está estructurada en cuatro subescalas:
											   </p>
											   <ul style="list-style-type:none" >
												<li class="g-margin-b-0--xs ">1 Autodistanciamiento (AD): investiga la capacidad para organizar el espacio interior, cuya manifestación es la posibilidad de conquistar la distancia de sí mismo, de los deseos, representaciones, temores, motivos, lo que permite la libre captación del mundo aún en ocasiones desfavorables. </li>
												<li  class="g-margin-b-0--xs ">2 Autotrascendencia (AT): evalúa la capacidad de resonancia afectiva y la captación de valores cuya condición es previa para el compromiso con el mundo. </li>
												<li  class="g-margin-b-0--xs ">3 Libertad (L): investiga la capacidad de decisión que tiene la persona al encontrarse ante una posibilidad real de acción, acorde con una jerarquía valorativa</li>
												<li  class="g-margin-b-0--xs " >4 Responsabilidad (R): evalúa la disposición para comprometerse a partir de una decisión libre y siendo consciente de la obligación, como así también de las tareas y valores que dicha decisión implica.</li>
												</ul>
												<p class="g-margin-b-0--xs justify" > El presente texto es el resultado de los trabajos de investigación a la Escala Existencial realizados por Gottfried, Boado de Landaboure, Traverso y Moreno, bajo la dirección del creador del instrumento Längle (Viena, Austria) quién supervisó y trabajó en equipo en el proceso de construcción en las fases de: traducción, diagramación y categorización. </p>
										  
										</div>
										</form> 
										</div>
									 
									 </form> 
								 </div>	 
									</div> <!-- /container -->
						  
						</div>
						<div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					   </div><!-- end scroll-->
					  </div>
					    
					  
					</div>
				  </div>
				  <!-- end modal-->
				   <!-- Modal l6-->
				  <div class="modal fade" id="l6" role="dialog">
					<div class="modal-dialog " >
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="g-font-size-25--xs">El hombre en busca de sentido</h3>
						
						
						</div>
						<div class="scroll">
						<div class="modal-body">
						  
								<div id="form1">
									<div class="container">

									  <form class="form-signin5">
									  
										<div id="form">
										<form action='#' id="myForm" method='post' name="myForm"> <!-- -->
									
											<div class="g-media__body g-padding-x-0--xs">
											  
													<br>
												   <h3 class="g-font-size-20--xs">Por Viktor Frankl. Edit. Herder</h3><br><br>
												   <p class="g-margin-b-0--xs justify" >Este libro se centra en varios hallazgos cruciales del doctor Frankl que ponen de manifiesto nuestro deseo inconsciente de descubrir un sentido definitivo a la vida, tanto si deriva de una fuente espiritual como si proviene de otro tipo de inspiración o influencia. Se trata de un tema de especial relevancia, sobre todo teniendo en cuenta que la sensación de que nuestra vida carece de un significado auténtico ha penetrado considerablemente en los cimientos de la sociedad contemporánea. Como demuestran tanto el caso del adolescente que sufre ante la inseguridad y la duda como el del anciano que padece aislamiento y rechazo, lo cierto es que la cultura actual parece definitivamente sumida en la vulnerabilidad y la desesperación.</p>
												   
											</div>
										 </form> 
										</div>
									 
									 </form> 
								 </div>	 
								</div> <!-- /container -->
						  
						</div>
						 <div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
						 </div><!-- end scroll-->
					  </div>
					  
					</div>
				  </div>
				  <!-- end modal-->
				   <!-- Modal l7-->
				  <div class="modal fade" id="l7" role="dialog">
					<div class="modal-dialog " >
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="g-font-size-25--xs">Psicoanálisis y Existencialismo</h3>
						
						
						</div>
						<div class="scroll">
						<div class="modal-body">
						  
								<div id="form1">
									<div class="container">

									  <form class="form-signin4">
									  
										<div id="form">
										<form action='#' id="myForm" method='post' name="myForm"> <!-- -->
									
											<div class="g-media__body g-padding-x-10--xs">
											  
											
												   <h3 class="g-font-size-20--xs">Por Viktor Frankl. Edit. Fondo de Cultura Económia. México.</h3><br>
												   <p class="g-margin-b-0--xs justify" >El análisis de la existencia humana que subyace a toda terapia que pretenda fundarse científicamente en las técnicas más avanzadas no puede ignorar el inestable coeficiente de filosofía que la compromete con los grandes temas del pensamiento, pues la psicoterapia no es una mera aplicación instrumental del conocimiento, sino una investigación en las condiciones del ser íntegro del hombre. No se reduce a un problema de medicación ni a una guía de conducta; esta visión compleja, global, de las tareas de la psicoterapia la tuvo Viktor E. Frankl, profesor de neurología y psiquiatría en la Universidad de Viena, profesor de logoterapia en la Universidad Internacional de San Diego, California, y catedrático de Harvard, Stanford, Pittsburgh y Dallas. Sus proposiciones acerca de un “análisis existencial”, de una logoterapia, de una científica y penetrante “cura de almas” hicieron de su obra una de las lecturas más fecundas para los interesados en el tema y filósofos, humanistas, sociólogos, etcétera, al extremo de que sus veinte libros han sido traducidos a catorce idiomas (incluidos el chino y el japonés). Es la suya una obra digna heredera de Freud, Jung y Adler.</p>
												   
											</div>
										 </form> 
										</div>
									 
									 </form> 
								 </div>	 
								</div> <!-- /container -->
						  
						</div>
						 <div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div><!-- end scroll-->
					  </div>
					  
					</div>
				  </div>
				  <!-- end modal-->
				  	   <!-- Modal l8-->
				  <div class="modal fade" id="l8" role="dialog">
					<div class="modal-dialog " >
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="g-font-size-25--xs">Las Psicoterapias Existenciales</h3>
						
						
						</div>
						<div class="modal-body">
						  
								<div id="form1">
									<div class="container">

									  <form class="form-signin4">
									  
										<div id="form">
										<form action='#' id="myForm" method='post' name="myForm"> <!-- -->
									
											<div class="g-media__body g-padding-x-10--xs">
											  
												   <h3 class="g-font-size-20--xs"Compilador: Efrén Martínez. Edit. Manual Moderno. Bogotá.</h3><br>
												   <p class="g-margin-b-0--xs justify" >Este libro constituye el primer texto en el mundo hispanoparlante que reúne en un mismo material, a prestigiosos terapeutas de los sistemas de terapia existencial más importantes de la actualidad, convirtiéndose así, en un libro introductorio para todas aquellas personas interesadas en tener un acercamiento serio a las diferentes posturas de este enfoque. En él, se exponen los planteamientos de los principales terapeutas existenciales de la llamada Escuela Inglesa de Psicoterapia Existencial, se abordan los planteamientos de los terapeutas existenciales norteamericanos, se revisa la sistematización del pensamiento de Viktor Frankl en una forma de Psicoterapia Centrada en el Sentido, se incluyen los planteamientos de Biswanger con su análisis del Dasein, una propuesta terapéutica de origen latinoamericano con las ideas de Emilio Romero, así como una aproximación a la psicoterapia analítico-existencial de Alfried Längle, concluyendo con algunos planteamientos acerca de la integración de estas formas de terapia.</p>
												   
											</div>
										 </form> 
										</div>
									 
									 </form> 
								 </div>	 
								</div> <!-- /container -->
						  
						</div>
						 <div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>

					  </div>
					  
					</div>
				  </div>
				  <!-- end modal-->
				  
			
        </div>
        <!-- End Testimonials -->


      

        

      
       
        <!--========== END PAGE CONTENT ==========-->

      

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>

        <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
        <!-- Vendor -->
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.migrate.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/bootstrap/js/bootstrap.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.smooth-scroll.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.back-to-top.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/scrollbar/jquery.scrollbar.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/magnific-popup/jquery.magnific-popup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/swiper/swiper.jquery.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/waypoint.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/counterup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.parallax.min.js");?>"></script>
        <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsXUGTFS09pLVdsYEE9YrO2y4IAncAO2U"></script> -->
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.wow.min.js");?>"></script>

        <!-- General Components and Settings -->
        <script type="text/javascript" src="<?php echo base_url("assets/js/global.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/header-sticky.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/scrollbar.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/magnific-popup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/swiper.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/counter.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/portfolio-3-col.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/parallax.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/google-map.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/wow.min.js");?>"></script>
		
		 <script type="text/javascript" src="<?php echo base_url("assets/js/scriptss.js");?>"></script>
		 <script type="text/javascript" src="<?php echo base_url("assets/js/script.js");?>"></script>
        <!--========== END JAVASCRIPTS ==========-->

    </body>
    <!-- End Body -->
</html>
