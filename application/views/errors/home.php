﻿<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cemael</title>
        <meta name="keywords" content="HTML5 Theme" />
        <meta name="description" content="Cemael">
        <meta name="author" content="cwa.mx">

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">

        <link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/animate.min.css");?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/themify/themify.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/scrollbar/scrollbar.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/magnific-popup/magnific-popup.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/swiper/swiper.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/cubeportfolio/css/cubeportfolio.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/font-awesome.min.css"); ?>" rel="stylesheet">

        <link href="<?php echo base_url("assets/css/style.css");?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/global.css");?>" rel="stylesheet" type="text/css"/>

        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    </head>

    <body>
        <header class="navbar-fixed-top s-header js__header-sticky js__header-overlay">
            <div class="s-header__navbar">
                <div class="s-header__container">
                    <div class="s-header__navbar-row">
                        <div class="s-header__navbar-row-col">       
                            <div class="s-header__logo">
                                <a href="" class="s-header__logo-link">
                                    <img class="s-header__logo-img s-header__logo-img-default" src="<?php echo base_url("assets/images/logo.png");?>" alt="Cemael Logo">
                                    <img class="s-header__logo-img s-header__logo-img-shrink" src="<?php echo base_url("assets/images/logo-dark.png");?>" alt="Cemael Logo">
                                </a>
                            </div>                            
                        </div>
                        <div class="s-header__navbar-row-col">
                            <a class="s-header__trigger js__trigger">
                                <span class="s-header__trigger-icon"></span>
                                <svg x="0rem" y="0rem" width="3.125rem" height="3.125rem" viewbox="0 0 54 54">
                                    <circle fill="transparent" stroke="#fff" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div  class="s-header-bg-overlay js__bg-overlay ">
                <nav class="s-header__nav js__scrollbar">
                    <div class="container-fluid">                             
                        <ul class="list-unstyled s-header__nav-menu">
                            <li class="s-header__nav-menu-item "><a  class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger"   href="#js__scroll-to-section">¿QUIÉNES SOMOS?</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger" href="#Analisis_E">¿QUE ES ANÁLISIS EXISTENCIAL?</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger" href="#Cursos_2">CURSOS</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger" href="#Terapias">TERAPIAS</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger" href="books"  target="_blank" >LIBROS</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger" href="blog"  target="_blank">ARTÍCULOS</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger" href="http://cemael.com/blog/blog"  target="_blank">BLOG</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider" href="http://online.fliphtml5.com/ultx/totr/#p=1"  target="_blank">REVISTA</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger" href="links" target="_blank" >LINKS</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger" href="videos" target="_blank" >VIDEOS</a></li>
                            <li class="s-header__nav-menu-item"><a class="s-header__nav-menu-link s-header__nav-menu-link-divider -is-active js__trigger" href="#Contacto" target="_blank">CONTACTO</a></li> 
                        </ul>
                    </div>
                </nav>

                <ul class="list-inline s-header__action s-header__action--rb">
                    <li class="s-header__action-item">
                        <a class="s-header__action-link" href= "https://www.facebook.com/CEMAELPosgrados/?ref=br_rs">
                            <i class="g-padding-r-5--xs orangeu fa fa-facebook"> <span class="g-display-none--xs g-display-inline-block--sm">Facebook</span></i>
                        </a>
                    </li>
                </ul>
            </div>
        </header>

        <!--========== SWIPER SLIDER ==========-->
        <div class="s-swiper js__swiper-one-item">
            <div class="swiper-wrapper">
                <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('<?php echo base_url("assets/images/slider/Imagen5.jpg");?>');">
                    <div class="container g-text-center--xs g-ver-center--xs">
                        <div class="g-margin-b-30--xs">
                            <div class="g-margin-b-30--xs">
                                <h2 class="g-font-size-35--xs g-font-size-45--sm g-font-size-55--md g-color--white">Especialidad en Análisis Existencial</h2>
                                <h3 style="color: white;">Inscripción: Enero 2019</h3>
                                <h3 style="color: white;">Inicio de clases: Febrero 22 y 23 viernes de 16 a 21 hrs. y sábado de 9 a 19 hrs.</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('<?php echo base_url("assets/images/slider/Imagen1.jpg");?>');">
                    <div class="container g-text-center--xs g-ver-center--xs">
                        <div class="g-margin-b-30--xs">
                            <h1 class="g-font-size-35--xs g-font-size-45--sm g-font-size-55--md g-color--white"><br></h1>
                        </div>
                    </div>
                </div>
                <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('<?php echo base_url("assets/images/slider/Imagen2.jpg");?>');">
                    <div class="container g-text-center--xs g-ver-center--xs">
                        <div class="g-margin-b-30--xs">
                            <div class="g-margin-b-30--xs">
                                <h2 class="g-font-size-35--xs g-font-size-45--sm g-font-size-55--md g-color--white">Inducción <h2>
                                <h3 style="color: white;"> <a data-toggle="modal" data-target="#myModaluno" > Registrate.</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('<?php echo base_url("assets/images/slider/Imagen3.jpg");?>');">
                    <div class="container g-text-center--xs g-ver-center--xs">
                        <div class="g-margin-b-30--xs">
                            <div class="g-margin-b-30--xs">
                                <h2 class="g-font-size-35--xs g-font-size-45--sm g-font-size-55--md g-color--white">Especialidad en Análisis Existencial</h2>
                                <h3 style="color: white;">Psicoterapia fenomenológica y centrada en la persona.</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('<?php echo base_url("assets/images/slider/Imagen4.jpg");?>');">
                    <div class="container g-text-center--xs g-ver-center--xs">
                        <div class="g-margin-b-30--xs">
                            <div class="g-margin-b-30--xs">
                                <h2 class="g-font-size-35--xs g-font-size-45--sm g-font-size-55--md g-color--white">Consultoría, Coaching en Análisis Existencial y Logoterapia</h2>
                                <h3 style="color: white;"> Explora las motivaciones fundamentales de la existencia.</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Swiper Wrapper -->

             <a href="javascript:void(0);" class="s-swiper__arrow-v1--right s-icon s-icon--md s-icon--white-brd g-radius--circle js__swiper-btn--next">></a>
             <a href="javascript:void(0);" class="s-swiper__arrow-v1--left s-icon s-icon--md s-icon--white-brd g-radius--circle  js__swiper-btn--prev"><</a>

            <a href="#Cursos_2" class="s-scroll-to-section-v1--bc g-margin-b-15--xs">
                <span class="g-font-size-15--xs g-color--d ">V</span>
                <p class="text-uppercase g-color--d g-letter-spacing--3 g-margin-b-0--xs">Leer Más</p>
            </a>

            <!-- Modal registro 1-->
            <div class="modal fade" id="myModaluno" role="dialog">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-header g-media__body g-padding-x-35--xs">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Solo ingresa la siguiente información:</h4>
                        </div>
                        <div class="modal-body">
                            <div id="form1">
                                <div class="container">
                                    <div class="form-signin2 g-media__body g-padding-x-20--xs">
                                        <form method="post" id="formularioUno" action="#">
                                            <br><input type="text" name="name" id="name" class="form-control" placeholder="Nombre Completo"/>
                                            <br><input type="number" name="telefono" id="telefono" class="form-control" placeholder="Teléfono"  />
                                            <br><input type="email" name="email" id="email" class="form-control" placeholder="Email " />
                                            <br><button id="submit" type="button" class="btn1" >Enviar</button>
                                        </form>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--========== END SWIPER SLIDER ==========-->

        <!--========== PAGE CONTENT ==========-->
        <div id="js__scroll-to-section" class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-text-center--xs g-margin-b-100--xs">
                <h2 class="g-font-size-32--xs g-font-size-36--md">¿Quiénes somos?<br> CEMAEL Centro Mexicano de Análisis Existencial y Logoterapia.</h2>
            </div>
            <div class="row g-margin-b-60--xs g-margin-b-70--md">
                <div class="col-sm-12 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="clearfix">
                         <div class="wow bounceInDown" >
                            <div class="g-media__body g-padding-x-20--xs">
                                <p class="g-margin-b-0--xs ">
                                    Tiene como objetivo la formación de especialización en Análisis Existencial y Logoterapia, siguiendo el enfoque desarrollado por la Sociedad Internacional de Logoterapia y Análisis Existencial con sede en Viena (GLE-Internacional) y aplicando los programas de formación oficiales desarrollados e impartidos por esa sociedad científica. Es así que los títulos llevan la doble certificación de CEMAEL y de GLE-Internacional.
                                    Actualmente hay dos programas de formación: el de Psicoterapia con especialidad en Análisis Existencial, abierto para psicólogos y psiquiatras y el de Consultoría (counseling y coaching) con especialidad en Logoterapia y Análisis Existencial, abierto a profesionales en las áreas social, educacional y organizacional-administrativa.
                                    Como complemento a la labor formativa, CEMAEL ofrece atención de psicoterapia individual. Asimismo ofrece talleres de autoexperiencia (auto-conocimiento y desarrollo personal).
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row g-margin-b-60--xs g-margin-b-70--md">
                <div class="col-sm-6 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="clearfix">
                        <div class="">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".1s">
                                <img class="photo" src="assets/images/fotos/elena.png">
                            </div>
                        </div>
                        <div class="wow bounceInLeft" >
                            <div class="g-media__body g-padding-x-20--xs">
                                <br>
                                <h3 class="g-font-size-20--xs">Ma. Elena Ramírez N.</h3>
                                <p class="g-margin-b-0--xs">
                                    <ul>
                                        <li type="disc">Psicóloga Clínica.</li>
                                        <li type="disc">Especialidad en Análisis Existencial y Logoterapia. (Viktor Frankl.)</li>
                                        <li type="disc">Especialidad en Análisis Existencial y Logoterapia. (Alfried Langle. GLE Mexico.)</li>
                                        <li type="disc">Especialidad en Psicoterapia Existencial.</li>
                                        <li type="disc">Especialidad en manejo y terapia grupal.</li>
                                        <li type="disc">Práctica privada</li>
                                        <li type="disc">Docencia.</li>
                                        <li type="disc">Supervisora.</li>
                                        <li type="disc">Cel. 55 2314 9747.</li>
                                        <li type="disc">E-mail: me.ramirez@cemael.com</li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="clearfix">
                        <div class=" ">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".2s">
                                <img class="photo" src="assets/images/fotos/beatriz.png">
                            </div>
                        </div>
                        <div class="wow bounceInRight" >
                            <div class="g-media__body g-padding-x-20--xs">
                                <br>
                                <h3 class="g-font-size-20--xs">Beatriz Ávila Amezquita</h3>
                                <p class="g-margin-b-0--xs">
                                    <ul>
                                    <li type="disc">Psicóloga.</li>
                                    <li type="disc">Especialidad en Análisis Existencial y Logoterapia.(Viktor Frankl.)</li>
                                    <li type="disc">Especialidad en Análisis Existencial y Logoterapia. (Alfried Langle. GLE Mexico.)</li>
                                    <li type="disc">Especialidad en Psicoterapia Existencial.</li>
                                    <li type="disc">Especialidad en manejo y terapia grupal.</li>
                                    <li type="disc">Práctica privada</li>
                                    <li type="disc">Docencia.</li>
                                    <li type="disc">Cel. 55 1951 0110.</li>
                                    <li type="disc">E-mail: beavila@cemael.com</li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Features -->

        <!-- Culture -->
        <div id="Analisis_E" class="g-promo-section">
            <div class="container g-padding-y-80--xs g-padding-y-125--sm"> 
                <div class="row g-margin-b-60--xs g-margin-b-70--md">
                    <div class="g-media__body g-padding-x-20--xs">
                        <div class="col-md-4 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                            <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                                <h2 class="g-font-size-30--xs g-font-size-50--sm g-font-size-40--md">¿Qué es el análisis existencial? </h2>
                            </div>
                        </div>
                        <div class="wow bounceInRight" data-wow-duration=".3" data-wow-delay=".4s">
                            <div class="col-md-7 col-md-offset-1">
                                <p class="g-margin-b-0--xs justify">
                                    El Análisis Existencial se puede definir como una psicoterapia fenomenológica y centrada en la persona, cuyo objetivo es el de orientar a la persona (mental y emocionalmente) a enfrentar experiencias libres, a tomar decisiones auténticas y a producir una forma de lidiar con la vida y el mundo de una forma realmente responsable. De este modo, el Análisis Existencial se puede aplicar en casos de desórdenes psicosociales, psicosomáticos y psicológicos, tanto experienciales como conductuales.
                                    El proceso terapéutico se realiza por la vía de un análisis fenomenológico de las emociones como el centro de las experiencias. El trabajo biográfico así como la escucha empática del terapeuta contribuyen a un aumento en el acceso y comprensión de las emociones. Análisis Existencial es una psicoterapia para el tratamiento de problemas y trastornos psíquicos tales como:
                                    <ul >
                                        <li type="disc" class="g-margin-b-0--xs ">Angustias.</li>
                                        <li type="disc" class="g-margin-b-0--xs ">Depresiones</li>
                                        <li type="disc" class="g-margin-b-0--xs ">Adicciones</li>
                                        <li type="disc" class="g-margin-b-0--xs " >Psicosis</li>
                                        <li type="disc" class="g-margin-b-0--xs ">Enfermedades psicosomáticas.</li>
                                    </ul>
                                </p>
                                <p class="g-margin-b-0--xs justify">
                                    El objetivo del Análisis Existencial: ayudar al ser humano a encontrar una forma de vivir donde pueda dar su aprobación interior a su propio actuar (“vivir afirmativamente”). Como psicoterapia fenomenológica personal, el trabajo está dirigido hacia una vivencia libre (espiritual y emocional), hacia tomas de posición auténticas y hacia una relación autorresponsable con la vida (propia) y el mundo.
                                .</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div class="row g-margin-b-60--xs g-margin-b-70--md">
                    <div class="g-media__body g-padding-x-20--xs">
                        <div class="col-md-4 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                            <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                                <h2 class="g-font-size-30--xs g-font-size-50--sm g-font-size-40--md">Visión del ser humano </h2>
                            </div>
                        </div>
                        <div class="wow bounceInRight" data-wow-duration=".3" data-wow-delay=".4s">
                            <div class="col-md-7 col-md-offset-1">
                                <br>
                                <p class="g-margin-b-0--xs justify">
                                    El ser persona humana se entiende aquí como un permanente “estar-en-pregunta”, es decir, preguntarse por valores vivenciados o anhelados (relaciones, tareas, etc.), que a uno por eso no les son indiferentes ya que con ellos “algo pasa” : en relación al valor de la propia vida y en relación al valor de la situación vivida. El ser humano no sólo es un interrogador y un desafiante; en la comprensión existencial el ser humano está verdaderamente ahí para, ante sus preguntas vitales, encontrar sus respuestas, responder a su vida en libertad.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div class="row g-margin-b-60--xs g-margin-b-70--md">
                    <div class="g-media__body g-padding-x-20--xs">
                        <div class="col-md-4 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                            <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                                <h2 class="g-font-size-30--xs g-font-size-50--sm g-font-size-40--md">Desarollo del Análisis Existencial </h2>
                            </div>
                        </div>
                        <div class="wow bounceInRight" data-wow-duration=".3" data-wow-delay=".4s">
                            <div class="col-md-7 col-md-offset-1">
                                <br>
                                <p class="g-margin-b-0--xs justify">
                                   El Análisis Existencial se funda sobre la antropología de M. Scheler y V. Frankl. Como método psicoterapéutico el Análisis Existencial fue desarrollado en los años 80 y 90 en el marco de la Sociedad de Logoterapia y Análisis Existencial (Viena), particularmente por A. Längle. Como tal el Análisis Existencial ha sido reconocido como método psicoterapéutico autónomo por el Ministerio de Salud de Austria, al igual que en Suiza, el país Checo y Rumania.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div class="rowg-margin-b-60--xs g-margin-b-70--md">
                    <div class="g-media__body g-padding-x-20--xs">
                        <div class="col-md-4 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                            <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                                <h2 class="g-font-size-30--xs g-font-size-50--sm g-font-size-40--md">¿ Qué quiere decir existencia ? </h2>
                            </div>
                        </div>
                        <div class="wow bounceInRight" data-wow-duration=".3" data-wow-delay=".4s">
                            <div class="col-md-7 col-md-offset-1">
                                <br>
                                <p class="g-margin-b-0--xs justify">
                                    En el centro del Análisis Existencial está el concepto de “existencia”, que quiere decir, una vida construida con pleno sentido, en libertad, fidelidad a sí mismo (autenticidad) y responsabilidad. “Existencia” significa por lo tanto más que desarrollos e intercambios automatizados. Existir significa confrontación e intercambio dialógico (encuentro) entre la persona y su mundo. Si este proceso profundamente humano es obstruido o impedido, la persona no podrá actuar justa y consecuentemente consigo mismo ni con su entorno. Comenzará a sufrir –y a veces también los otros- la enajenación.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div class="row g-margin-b-60--xs g-margin-b-70--md">
                    <div class="g-media__body g-padding-x-20--xs">
                        <div class="col-md-4 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                            <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                                <h2 class="g-font-size-30--xs g-font-size-50--sm g-font-size-40--md">Las cuatro motivaciones Fundamentales Existenciales </h2>
                            </div>
                        </div>
                        <div class="wow bounceInRight" data-wow-duration=".3" data-wow-delay=".4s">
                            <div class="col-md-7 col-md-offset-1">
                                <br>
                                <p class="g-margin-b-0--xs justify">
                                    Mientras Frankl consideraba la búsqueda de sentido como la motivación más profunda de los seres humanos, recientemente el Análisis Existencial ha distinguido otras tres motivaciones existenciales (o personales) que preceden a la motivación por el sentido y que mueven al ser humano profunda y constantemente:			
                                </p>
                                <ul style="list-style-type:none" >
                                    <br><li class="g-margin-b-0--xs ">1 MF: Al ser humano lo moviliza la pregunta fundamental de la existencia : Yo soy – pero ¿puedo ser y estar (como persona total)? ¿Tengo el suficiente espacio, protección y sostén? Una persona experimenta especialmente esto cuando se siente aceptado, lo que le permitirá a su vez tener una actitud personal de auto aceptación. – La carencia de esto conduce a la ansiedad (angustia)</li>
                                    <br><li  class="g-margin-b-0--xs ">2 MF: Al ser humano lo moviliza la pregunta fundamental de la existencia : Yo estoy vivo – pero ¿me gusta vivir? ¿Experimento plenitud, afecto y aprecio por aquello que tiene valor en mi vida? La dedicación requiere sentir lo valioso en la propia vida. Este valor fundamental consiste en un profundo darse cuenta de que es bueno existir (“que yo soy y estoy aquí”).- La carencia de esto conduce a la depresión.</li>
                                    <br><li  class="g-margin-b-0--xs ">3 MF: Al ser humano lo moviliza la pregunta fundamental de la existencia : Yo soy yo – pero ¿me siento libre para ser yo mismo/a? ¿Experimento atención, justicia, aprecio, estima, respeto, mi propio valor? – Carencias a este nivel, conducen a complejos de síntomas histriónicos así como a los principales trastornos de personalidad.</li>
                                    <br><li  class="g-margin-b-0--xs " >4 MF: Al ser humano lo moviliza la pregunta fundamental de la existencia : Yo estoy aquí – pero ¿para qué es bueno? ¿Qué puedo hacer hoy, para que mi vida sea parte de una totalidad con sentido? – ¿Para qué vivo?- Carencias en este nivel conducen a las adicciones y la dependencia. El principal tema existencial es la interacción (“diálogo”). Los grupos de Análisis Existencial interactúan alrededor de estos cuatro desafíos existenciales: el mundo, la vida, el yo, el sentido y el futuro.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div class="row g-margin-b-60--xs g-margin-b-70--md">
                    <div class="g-media__body g-padding-x-20--xs">
                        <div class="col-lg-12 col-md-12 g-text-center--xs g-margin-b-100--xs">
                            <div >
                                <br><br><br>
                                <h3 class="g-font-size-40--xs">Alfried Längle.</h3>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-4 col-md-push-6">
                            <div class="wow pulse" data-wow-duration=".3" data-wow-delay=".1s">
                                <br>
                                <img class="photog" src="assets/images/fotos/a.jpg">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8 col-md-pull-6 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                            <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".4s">
                                <p class="g-margin-b-0--xs justify">
                                    Alfried Längle, M.D., Ph.D., Dr. h.c., nació en 1951 en Austria donde aún vive. Estudió medicina y psicología en la Universidades de Innsbruck, Roma, Toulouse y Viena, doctorándose en ambas disciplinas. Después de años de trabajo de hospital en medicina general y psiquiatría y en el departamento de pacientes ambulatorios de psiquiatría social comenzó una práctica privada en psicoterapia, medicina general y psicología clínica en Viena (desde 1982). En la misma época, mantuvo una cercana colaboración con Viktor Frankl (1983-1991). Asistió a las clases universitarias de Frankl durante muchos años y trabajó junto a él en muchos campos relevantes de la Logoterapia. Es el fundador y presidente (desde 1983) de la Sociedad Internacional de Logoterapia y Análisis Existencial (Viena) www.existenzanalyse.org, cuyo presidente honorario fue Viktor Frankl hasta 1990. Por esa época, Frankl renunció a la presidencia honoraria debido a los nuevos desarrollos de Längle en el campo del Análisis Existencial (métodos, implicación de la auto-exploración existencial en los seminarios de formación, rechazando el uso exclusivo del paradigma del sentido en psicoterapia al implementar también el acceso biográfico.
                                    <br>
                                    Junto a seminarios intensivos y ciclos de conferencias ha sido docente estable en las Universidades de Viena (desde 1984), Innsbruck (1994), Graz (1995) y profesor invitado regularmente a dar cursos en Universidades de Moscú, Praga, Main, Temesvar, Buenos Aires, Mendoza y Tucumán. Fundador de la escuela de formación de Psicoterapia Analítico-Existencial, que entretanto ha sido estatalmente aprobada en Austria, Suiza, República Checa y Rumania. Es socio honorario en numerosas sociedades de psicoterapia, ente otros de la Sociedad Chilena de Psicología Clínica. En 2000 recibió el grado de doctor honorario, en reconocimiento por sus desarrollos en el campo del Análisis Existencial, de la facultad de medicina de la universidad de Temesvar (Rumania) y en 2004 un doctorado honorario de la Facultad de Psicología de la Universidad de las Américas en Santiago de Chile. Es el actual Vicepresidente de la International Federation of Psychotherapy (IFP).
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div class="row g-margin-b-60--xs g-margin-b-70--md">
                    <div class="g-media__body g-padding-x-20--xs">
                        <div class="col-md-4 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                            <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                                <h2 class="g-font-size-30--xs g-font-size-50--sm g-font-size-40--md">¿Qué es logoterapia? </h2>
                            </div>
                        </div>
                        <div class="wow bounceInRight" data-wow-duration=".3" data-wow-delay=".4s">
                            <div class="col-md-7 col-md-offset-1">
                                <p class="g-margin-b-0--xs justify">
                                    La  búsqueda de sentido de vida en un contexto más amplio; es una profundización Existencial. La frustración de no encontrar el sentido,  se ha convertido en un síntoma de nuestros tiempos "vacío existencial" según Viktor Frankl.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div class="row g-margin-b-60--xs g-margin-b-70--md">
                    <div class="g-media__body g-padding-x-20--xs">
                        <div class="col-md-4 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                            <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                                <h2 class="g-font-size-30--xs g-font-size-50--sm g-font-size-40--md">Base de la Logoterapia </h2>
                            </div>
                        </div>
                        <div class="wow bounceInRight" data-wow-duration=".3" data-wow-delay=".4s">
                            <div class="col-md-7 col-md-offset-1">
                                <p class="g-margin-b-0--xs justify">
                                    La logoterapia se basa en la comprobación que la vida humana, aún bajo condiciones difíciles y extremas, lleva en sí la posibilidad de una construcción plena de sentido, por la realización de:
                                </p>
                                <ul >
                                    <li type="disc" class="g-margin-b-0--xs ">Valores vivenciales (naturaleza, arte, encuentro)</li>
                                    <li type="disc" class="g-margin-b-0--xs "> Valores creadores (creatividad, acciones)</li>
                                    <li type="disc" class="g-margin-b-0--xs ">Valores de actitud (relacionar una actitud a un dolor inevitable)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div class="row g-margin-b-60--xs g-margin-b-70--md">
                    <div class="g-media__body g-padding-x-20--xs">
                        <div class="col-lg-12 col-md-12 g-text-center--xs g-margin-b-100--xs">
                            <div>
                                <br><br><br>
                                <h3 class="g-font-size-40--xs">Viktor Frankl.</h3>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-4 col-md-push-6">
                            <div class="wow pulse" data-wow-duration=".3" data-wow-delay=".1s">
                                <br>
                                <img class="photog" src="assets/images/fotos/victor.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8 col-md-pull-6 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                            <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".4s">
                            <p class="g-margin-b-0--xs justify">
                                Viktor Frankl (1905-1997 Neurólogo y psiquiatra) Fundó este “tercer enfoque vienés de psicoterapia” como complemento al psicoanálisis de Sigmund Freud y la psicología individual de Alfred Adler A través de numerosas publicaciones desde los años 30, encontraron reconocimiento internacional su visión del ser humano y su enfoque terapéutico. En los años 80 y 90 la propuesta de Frankl fue elaborada en forma sistemática y metódica y establecida como una escuela de psicoterapia por la Sociedad de Logoterapia y Análisis Existencial de Viena.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
            </div>
        </div>
        <!-- End Culture -->

        <section id="Cursos_2">
            <div class="g-text-center--xs g-margin-b-80--xs">
                <p class=" g-font-size-36--xs g-font-weight--700 g-color--orange g-letter-spacing--2 g-margin-b-25--xs">Cursos</p>
            </div>
            <div class="containera">
                <img class="inicial nuevo"src="assets/images/slider/Imagen3.jpg" alt="curso" >
                <div class="centera">
                    <p> <a class="txt" data-toggle="modal"  href="#myModal1">Especialidad en Análisis Existencial</a> <br><br><a class="txt2 " data-toggle="modal"  href="#myModal1">Leer mas...</a>   </p>
                </div>
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--1"><a href="assets/pdf/especialidad.pdf" target="_blank">Descargar programa</a> </p>
            </div>
            <br><br><br>
            <div class="containera">
                <img class="inicial nuevo"src="assets/images/slider/Imagen2.jpg" alt="curso" >
                <div class="centera">
                    <p> <a class="txt" data-toggle="modal"  href="#myModal2">Consultoría y Coaching en Análisis<br>Existencial y Logoterapia</a>  <br><br><a class="txt2" data-toggle="modal"  href="#myModal2">Leer más...</a></p>
                </div>
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--1"><a href="assets/pdf/coaching.pdf" target="_blank">Descargar programa</a> </p>
            </div>
        </section>

        <!-- News -->
        <div>
            <!-- Modal -->
            <div class="modal fade" id="myModal1" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content ">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="g-font-size-25--xs">Especialidad en Análisis Existencial</h3>
                        </div>
                        <div class="scroll">
                            <div class="modal-body">
                                <div id="form1">
                                    <div class="container">
                                        <form class="form-signin">
                                            <div id="form">
                                                <form action='#' id="myForm" method='post' name="myForm">
                                                    <div class="g-media__body g-padding-x-10--xs">
                                                        <br><br><br>
                                                        <ul >
                                                            <li type="disc" class="g-margin-b-0--xs ">Modalidad :Una vez a la semana inicio miércoles 6 de septiembre de 16:00 a 20:00 hrs. </li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Cupo máximo: 18 personas. </li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Requisito: Entrevista personal, curriculum, autobiografía y carta de motivos.</li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Dirigido a: Psicólogos, psiquiatras.</li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Total de horas: 1.400 hrs.</li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Duración: 3 años y medio (teoría, auto-exploración grupal e individual, supervisión de casos).</li>
                                                        </ul>
                                                        <br><br><br>
                                                        <h3 class="g-font-size-20--xs">Objetivo del Análisis Existencial.</h3><br>
                                                        <p class="g-margin-b-0--xs justify" >Como psicoterapia fenomenológica-personal, el trabajo está dirigido hacia una vivencia libre (espiritual y emocional), hacia tomas de posición auténticas y hacia una relación auto responsable con la vida (propia) y el mundo.</p>
                                                        <br><br><br>
                                                        <h3 class="g-font-size-20--xs">Objetivo de la Formación.</h3><br>
                                                        <p class="g-margin-b-0--xs justify" >Una primera etapa ofrece una formación profunda, centrada en la antropología del Análisis Existencial, la fenomenología, la teoría procesal y estructural en que se fundamenta la psicoterapia analítico-existencial y en el tratamiento de situaciones límite y crisis existenciales (teoría existencial y del sentido). Se confrontan preguntas sobre la esencia del ser humano y del ser persona, sobre la motivación existencial, sobre los efectos y síntomas del fracaso existencial. El tratamiento de los contenidos corre paralelamente al desarrollo y maduración de la personalidad y a la aclaración existencial por medio de la auto-experiencia. <br>				La segunda etapa de la formación corresponde a la parte clínica, teniendo como objetivo temático capacitar en diagnósticos, psicogénesis, psicopatología y fenomenología de trastornos clínicos, nosología y terapia de los cuadros individuales.</p>
                                                        <br><br><br>
                                                        <h3 class="g-font-size-20--xs">Costos:</h3><br>
                                                        <ul>
                                                            <li type="disc" class="g-margin-b-0--xs "> Inscripción semestral: $1,000.00</li>
                                                            <li type="disc" class="g-margin-b-0--xs "> Colegiatura mensual: $2,900.00</li>
                                                        </ul>
                                                        <br><br>
                                                        <p class="g-margin-b-0--xs justify" >El costo incluye todos los manuales que se utilizarán durante la formación.</p>
                                                        <p class="g-margin-b-0--xs justify" >Período de inscripción: Julio 2017</p>
                                                        <div class="centers">
                                                            <br>
                                                            <button type="button" class="btn1 " data-toggle="modal" data-target="#myModalAE">REGISTRARSE</button>
                                                        </div>
                                                    </div>
                                                </form> 
                                            </div>
                                        </form> 
                                    </div>	 
                                </div> <!-- /container -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div><!-- end scroll-->
                    </div><!-- modal content-->
                </div><!-- modal dialog -->
            </div><!-- modal -->

            <!-- Modal -->
            <div class="modal fade" id="myModal2" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="g-font-size-25--xs">Consultoría y Coaching en Análisis <br>Existencial y Logoterapia</h3>
                        </div>
                        <div class="scroll">
                            <div class="modal-body">
                                <div id="form1">
                                    <div class="container">
                                        <form class="form-signin">
                                            <div id="form">
                                                <form action='#' id="myForm"  name="myForm">
                                                    <div class="g-media__body g-padding-x-10--xs">
                                                        <ul >
                                                            <li type="disc" class="g-margin-b-0--xs ">Modalidad: Una vez a la semana inicio miércoles 6 de septiembre de 16:00 a 20:00 hrs. </li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Cupo máximo: 18 personas. </li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Requisito: Entrevista personal, curriculum, autobiografía y carta de motivos.</li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Dirigido a: Profesionales</li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Total de horas: 650 hrs</li>
                                                            <li type="disc" class="g-margin-b-0--xs ">Duración: 2 años de formación básica (350 hrs), medio año de instrucción sobre la praxis (80 hrs), supervisión en grupos chicos y sesiones individuales (mín. 90 hrs), autoexploración individual (aprox. 30 hrs), lecturas, trabajo individual y final (aprox. 100 hrs)..</li>
                                                        </ul>
                                                        <h3 class="g-font-size-20--xs">Objetivo del Análisis Existencial.</h3>
                                                        <p class="g-margin-b-0--xs justify" >Como enfoque fenomenológico-personal, el trabajo está dirigido hacia una vivencia libre (espiritual y emocional), hacia tomas de posición auténticas y hacia una relación auto rresponsable con la vida (propia) y el mundo.</p>
                                                        <h3 class="g-font-size-20--xs">Objetivo de la Formación.</h3>
                                                        <p class="g-margin-b-0--xs justify" >Esta formación habilita para una consultoría y un acompañamiento analítico-existencial y logo terapéutico, en el ámbito de la consejería de vida o consultoría de personas. Se trata aquí principalmente del acompañamiento de personas que se encuentran en crisis, situaciones de conflicto, necesidades de orientación o patrones de conducta fijos (eventualmente con perturbaciones neuróticas leves o medias). Además, la formación proporciona medios de asistencia orientados al trato con situaciones de sufrimiento.<br>		El tratamiento de los contenidos corre paralelamente a la formación de la personalidad y a la aclaración existencial, por medio de la auto-experiencia. La formación se orienta principalmente a personas ocupadas en trabajos sociales o de ciencias humanas, tales como pedagogos, orientadores, médicos, psicólogos, trabajadores y asistentes sociales, labores de asistencia y cuidado, educadores, trabajos con recursos humanos, consultores en organizaciones y coach. También es apropiada por quienes buscan el propio desarrollo personal sin mayores metas laborales.</p>
                                                        <h3 class="g-font-size-20--xs">Costos:</h3>
                                                        <ul >
                                                            <li type="disc" class="g-margin-b-0--xs "> Inscripción semestral: $1,000.00</li>
                                                            <li type="disc" class="g-margin-b-0--xs "> Colegiatura mensual: $2,900.00)</li>
                                                        </ul>
                                                        <p class="g-margin-b-0--xs justify" >El costo incluye todos los manuales que se utilizarán durante la formación.</p>
                                                        <p class="g-margin-b-0--xs justify" >Período de inscripción: Julio 2017</p>
                                                        <div class="centers">
                                                            <br>
                                                            <button type="button" class="btn1 " data-toggle="modal" data-target="#myModalCC">REGISTRARSE</button>
                                                        </div>
                                                    </div>
                                                </form> 
                                            </div>
                                        </form> 
                                    </div>	 
                                </div> <!-- /container -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div><!-- end scroll-->
                    </div>
                </div>
            </div>

            <!-- Modal registro analíisis-existencial-->
            <div class="modal fade" id="myModalAE" role="dialog">
                <div class="modal-dialog ">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header g-media__body g-padding-x-35--xs">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Solo ingresa la siguiente información:</h4>
                        </div>
                        <div class="modal-body">
                            <div id="form1">
                                <div class="container">
                                    <div class="form-signin2 g-media__body g-padding-x-10--xs">
                                        <form method="post" id="formulariodos" action='#'>
                                            <br>
                                            <input type="text" name="name" id="namea" class="form-control" placeholder="Nombre Completo"/>
                                            <br>
                                            <input type="number" name="telefono" id="telefonoa" class="form-control" placeholder="Teléfono"  />
                                            <br>
                                            <input type="email" name="email" id="emaila" class="form-control" placeholder="Email" />
                                            <br>
                                            <button id="submita" type="button" class="btn1" >Enviar</button>
                                        </form>
                                    </div> 
                                </div>
                            </div> <!-- /container -->
                        </div>
                    </div>
                </div>
            </div>
            <!--- modal analisis existencial-->
            <div class="modal fade" id="myModalCC" role="dialog">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-header g-media__body g-padding-x-35--xs">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Solo ingresa la siguiente información:</h4>
                        </div>
                        <div class="modal-body">
                            <div id="form1">
                                <div class="container">
                                    <div class="form-signin2 g-media__body g-padding-x-20--xs">
                                        <form method="post" id="formulariotres" action='#'>
                                            <br><input type="text" name="name" id="nameb" class="form-control" placeholder="Nombre Completo"/>
                                            <br><input type="number" name="telefono" id="telefonob" class="form-control" placeholder="Teléfono"  />
                                            <br><input type="email" name="email" id="emailb" class="form-control" placeholder="Email" />
                                            <br><button id="submitb" type="button" class="btn1" >Enviar</button>
                                        </form>
                                    </div> 
                                </div>
                            </div>  <!-- /container -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal registro Consultoría y Coaching -->
        </div>
        <!-- End News -->

            <div id="" class="container g-padding-y-0--xs g-padding-y-0--sm">
                <div class="col-sm-6 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="clearfix">
                        <div class="wow bounceInLeft" >
                            <div class="g-media__body g-padding-x-20--xs">
                                <br>
                                <p class="g-margin-b-0--xs">
                                    <br><br><br>
                                    Certificación :  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img class="certificacion" src="assets/images/cert/certificacion.png"><br><br>
                                    5 exámenes - 4 parciales y  1 examen final .  <br>
                                    Se aprueba un examen con el 85 % pasado. <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="clearfix">
                        <div class="wow bounceInLeft" >
                            <div class="g-media__body g-padding-x-20--xs">
                                <br>
                                <p class="g-margin-b-0--xs">
                                    <br><br><br>
                                    Requisitos para graduarse :  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img class="certificacion1" src="assets/images/cert/requisitos.png"> <br><br>
                                    Escribir un trabajo de por lo menos 30 páginas. <br>
                                    El estudio de un caso o un trabajo de investigación usando el método Analítico Existencial. Un artículo publicado en una revista de carácter científico, sería suficiente para tomarlo en cuenta como un trabajo para graduarse.<br>						
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="clearfix">
                        <div class="wow bounceInDown" >
                            <div class="g-media__body g-padding-x-20--xs">
                                <br><br><br><br>
                                <p class="g-margin-b-0--xs ">
                                    Los estudiantes que reúnan los requisitos indicados obtendrán el diploma de <a href="http://www.existenzanalyse.org" target="_blank">GLE - International.</a> <br> <br>
                                    Este diploma de la Psicoterapia,  constituye la base para obtener , los certificados de las más destacadas asociaciones Psicoterapéuticas profesionales en Europa , la Asociación Europea de Psicoterapia y la Federación Internacional de Psicoterapia .<br>
                                    * La lista completa de criterios para obtener la acreditación puede ser encontrada en <a href="http://www.europsyche.org " target="_blank">EAP website.</a> 
                                    <br><br><br><br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row g-margin-b-60--xs g-margin-b-70--md">
                <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="clearfix">
                        <div class="">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".1s">
                                <img class="photod" src="assets/images/d7.jpeg" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="clearfix">
                        <div class=" ">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".2s">
                                <img class="photod" src="assets/images/d6.jpeg">
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="col-sm-4 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="clearfix">
                        <div class=" ">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".2s">
                                <img class="photod" src="assets/images/d5.jpeg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

         <div id="Terapias" class="centrado">
            <div class="container g-padding-y-80--xs g-padding-y-125--sm">
                <div class="g-text-center--xs g-margin-b-80--xs">
                    <p class=" g-font-size-36--xs g-font-weight--700 g-color--orange g-letter-spacing--2 g-margin-b-25--xs">Terapias</p>
                </div>
                <div id="form1" >
                    <form method="post" id="formulariocuatro">
                        <div class="centrado" style="background: url('<?php echo base_url("assets/images/terapia.jpg");?>');">
                            <div class=" row g-margin-b-40--xs">
                                <div class="col-sm-6 g-margin-b-20--xs g-margin-b-0--md col-md-push-3">
                                    <div class="g-margin-b-20--xs">
                                        <label></label>
                                        <input type="text" name="name" id="namec" class="form-control s-form-v2__input g-radius--50" placeholder="* Nombre Completo"/>
                                    </div>
                                    <div class="g-margin-b-20--xs">
                                        <label></label>
                                        <input type="email" name="email" id="emailc" class="form-control s-form-v2__input g-radius--50" placeholder="* Email " />
                                    </div>
                                    <div class="g-margin-b-20--xs">
                                        <label></label>
                                        <input type="number" name="edad" id="edadc" class="form-control s-form-v2__input g-radius--50" placeholder="* Edad"  />
                                    </div>
                                    
                                    <div class="g-margin-b-20--xs">
                                        <input type="text" name="sexo" id="sexoc" class="form-control s-form-v2__input g-radius--50" placeholder="* Sexo (Masculino/Femenino)"  />
                                    </div>
                                    <div class="g-margin-b-20--xs">
                                        <input type="text" name="estado" id="estadoc" class="form-control s-form-v2__input g-radius--50" placeholder="* Estado Civil"  />
                                    </div>
                                    <div class="g-margin-b-20--xs">
                                         <input type="text" name="escolaridad" id="escolaridadc" class="form-control s-form-v2__input g-radius--50" placeholder="* Escolaridad"  />
                                    </div>
                                    <div>
                                        <label></label>
                                        <input type="text" name="ocupacion" id="ocupacionc" class="form-control s-form-v2__input g-radius--50" placeholder="* Ocupación"  />
                                    </div>
                                    <div>
                                        <label></label>
                                        <input type="number" name="telefono" id="telefonoc" class="form-control s-form-v2__input g-radius--50" placeholder="* Teléfono"  />
                                    </div>
                                </div>
                            </div>
                            <div class="g-text-center--xs">
                                <br><button id="submitc" type="button" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-80--xs" >Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="Contacto" class="g-bg-color--sky-light">
            <div class="container g-padding-y-80--xs g-padding-y-125--sm">
                <div class="g-text-center--xs g-margin-b-80--xs">
                    <p class=" g-font-size-36--xs g-font-weight--700 g-color--orange g-letter-spacing--2 g-margin-b-25--xs">Contacto</p>
                </div>
                <form method="post" id="formulariocinco">
                    <div class="row g-margin-b-40--xs">
                        <div class="col-sm-6 g-margin-b-20--xs g-margin-b-0--md">
                            <div class="g-margin-b-20--xs">
                                <input type="text" name="name" id="named" class="form-control s-form-v2__input g-radius--50" placeholder="* Nombre Completo"/>
                            </div>
                            <div class="g-margin-b-20--xs">
                                <input type="email" name="email" id="emaild" class="form-control s-form-v2__input g-radius--50" placeholder="* Email " />
                            </div>
                            <input type="number" name="telefono" id="telefonod" class="form-control s-form-v2__input g-radius--50" placeholder="* Teléfono"  />
                        </div>
                        <div class="col-sm-6">
                            <textarea  type="text" name="mensaje" id="mensajed" class="form-control s-form-v2__input g-radius--10 g-padding-y-20--xs" rows="8"  placeholder="* Mensaje"/></textarea>
                       </div>
                    </div>
                    <div class="g-text-center--xs">
                        <br><button id="submitd" type="button" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-80--xs" >Enviar</button>
                    </div>
                </form>
            </div>
        </div>
        <!--========== END PAGE CONTENT ==========-->

        <footer  class="g-bg-color--dark">
            <div class="g-hor-divider__dashed--white-opacity-lightest">
                <div class="container g-padding-y-80--xs">
                    <div class="row">
                        <div class="col-md-12 col-sm-12  s-footer__logo g-padding-y-50--xs g-padding-y-0--md">
                            <img class="photolog" src="<?php echo base_url("assets/images/logo.png");?>" alt="Cemael Logo"align="middle">
                            <h3 class="g-font-size-20--xs g-color--white">CONTACTO:</h3>
                            <h3 class="g-font-size-18--xs g-color--white"> Heriberto Frias 715 - 2 Col. Del Valle, Cd. México.<BR> contacto@cemael.com<BR>Mon - Fri: 9:00 - 19:00<BR>55 2314 9747 / 55 1951 0110</h3>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="container g-padding-y-50--xs">
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-4 g-text-right--xs">
                        <p class="g-font-size-14--xs g-margin-b-0--xs g-color--white-opacity-light"><a href="">Cemael</a> Theme Powered by: <a href="http://cwa.mx/">CWA.mx</a></p>
                    </div>
                </div>
            </div>
        </footer>

        <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>
        <!-- Vendor -->
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.migrate.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/bootstrap/js/bootstrap.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.smooth-scroll.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.back-to-top.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/scrollbar/jquery.scrollbar.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/magnific-popup/jquery.magnific-popup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/swiper/swiper.jquery.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/waypoint.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/counterup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.parallax.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.wow.min.js");?>"></script>

        <script type="text/javascript" src="<?php echo base_url("assets/js/global.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/header-sticky.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/scrollbar.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/magnific-popup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/swiper.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/counter.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/portfolio-3-col.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/parallax.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/wow.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/scriptss.js");?>"></script> 
        <script type="text/javascript" src="<?php echo base_url("assets/jquery-validate/dist/jquery.validate.min.js");?>"></script> 
        <script type="text/javascript" src="<?php echo base_url("assets/jquery-validate/validaciones.js");?>"></script> 
    </body>
</html>