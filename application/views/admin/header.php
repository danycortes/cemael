<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Cemael</title>
  <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/animate.min.css"); ?>" rel="stylesheet"> 
  <link href="<?php echo base_url("assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/lightbox.css"); ?>" rel="stylesheet">
  
  <link id="css-preset" href="<?php echo base_url("assets/css/presets/preset1.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/responsive.css");?>" rel="stylesheet">
  <!--Ionicons-->
  <link href="<?php echo base_url("assets/admin/css/ionicons.min.css");?>" rel="stylesheet" type="text/css" /> 
  <!-- Morris chart -->
  <link href="<?php echo base_url("assets/admin/css/morris/morris.css");?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url("assets/admin/css/iCheck/all.css");?>" rel="stylesheet" type="text/css" />
  <!-- bootstrap wysihtml5 - text editor -->
  <!-- <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->
  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
  <!-- Theme style -->
  <link href="<?php echo base_url("assets/admin/css/style.css");?>" rel="stylesheet" type="text/css" />
  <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/admin/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/admin/js/respond.min.js");?>" ></script>
  <![endif]-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.png");?>">
</head> 
<body class="skin-black">
