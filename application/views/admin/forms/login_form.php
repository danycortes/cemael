    <style type="text/css">
        * {
          border-radius: 0 !important;
        }
        body, html {
            height: 100%;
            background-repeat: no-repeat;
            background-image: url("assets/images/slider/1.jpg");
        }
        .card-container.card {
            max-width: 350px;
            padding: 40px 40px;
        }
        .btn {
            font-weight: 700;
            height: 36px;
            -moz-user-select: none;
            -webkit-user-select: none;
            user-select: none;
            cursor: default;
        }
        /* * Card component*/
        .card {
            background-color: #FFF;
            /* just in case there no content*/
            padding: 20px 25px 30px;
            margin-top: 50px;
            /*margin: 0  25px;
            margin-top: 50px;
            /* shadows and rounded borders */
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
            -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        }
        .profile-img-card {
            width: 96px;
            height: 96px;
            margin: 0 auto 10px;
            display: block;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }
        /** Form styles */
        .profile-name-card {
           font-size: 16px;
            font-weight: bold;
            text-align: center;
            margin: 10px 0 0;
            min-height: 1em;
        }

        .reauth-email {
            display: block;
            color: #028fcc;
            line-height: 2;
            margin-bottom: 10px;
            font-size: 14px;
            text-align: center;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin #inputEmail,
        .form-signin #inputPassword {
            direction: ltr;
            height: 44px;
            font-size: 16px;
        }
        .form-signin input[type=email],
        .form-signin input[type=password],
        .form-signin input[type=text],
        .form-signin button {
            width: 100%;
            display: block;
            margin-bottom: 10px;
            z-index: 1;
            position: relative;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin .form-control:focus {
            border-color: rgb(104, 145, 162);
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
        }
        .btn.btn-signin {
            /*background-color: #4d90fe; */
            background-color: #028fcc;
            /* background-color: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
            padding: 0px;
            font-weight: 700;
            font-size: 14px;
            height: 36px;
            border: none;
            -o-transition: all 0.218s;
            -moz-transition: all 0.218s;
            -webkit-transition: all 0.218s;
            transition: all 0.218s;
        }
        .btn.btn-signin:hover,
        .btn.btn-signin:active,
        .btn.btn-signin:focus {
            background-color: rgb(0, 204, 0);
        }
        .forgot-password {
            color: #028fcc;
        }
        .forgot-password:hover,
        .forgot-password:active,
        .forgot-password:focus{
            color: #028fcc;
        }
        #usuario-error{
            color: red;
        }
    </style>
    <div class="container">
        <div class="col-md-4 col-md-offset-8  card card-container">
            <img id="profile-img" class="profile-img-card" src="<?php echo base_url("/assets/admin/img/Human.png");?>" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" method="post" id="login-form" role="form">
                <div id="error"></div>
                <span id="reauth-email" class="reauth-email"></span>
                <input type="email" id="usuario" name="usuario" class="form-control" placeholder="<?=lang('admin_login_email');?>" required autofocus>
                <input type="password" id="password" name="password" class="form-control" placeholder="<?=lang("admin_login_password");?>" required>
                <!--<div id="remember" class="checkbox">
                    <label>
                      <input type="checkbox" value="remember-me"><?=lang("admin_login_rememberme");?>
                    </label>
                </div>-->
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" value="login" id="btn-signin"><?=lang("admin_login_log_in");?></button>
            
                
            </form><!-- /form -->
            <a href="<?php echo base_url("/home");?>" class="forgot-password">
                <?=lang("admin_login_olvidopassword");?>
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->
    <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.js")?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.validate.min.js")?>"></script>