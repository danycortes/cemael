	<section class="panel">
    	<header class="panel-heading">
      		<?=lang("admin_perfil_title");?>
    	</header>
    	<div class="panel-body">
        	<div id="success"></div>
			<div id="error"></div>
		    <div class="form-group">
		     <label for="txtTitle" class="col-lg-2 col-sm-2 control-label">
		     <?=lang("admin_perfil_txt_perfil");?></label>
		      <div class="col-lg-5">
		          <input type="text" class="form-control" id="txtTitlePerfil" name="txtTitlePerfil" 
		          placeholder="<?=lang("admin_perfil_txt_perfil");?>" value="<?=$user->rol_nombre?>" readonly>
		      </div>
		    </div>
		    <br>
		    <div class="form-group">
		        <label for="txtApodo" class="col-lg-2 col-sm-2 control-label">
		        <?=lang("admin_perfil_txt_apodo");?>
		        </label>
		        <div class="col-lg-5">
		            <input type="text" class="form-control" id="txtApodo" name="txtApodo"
		            placeholder="<?=lang("admin_perfil_txt_apodo");?>" value="<?=$user->apodo?>" readonly>		         
		        </div>
		    </div>	
		    <br>
		    <div class="form-group">
		        <label for="txtIntro" class="col-lg-2 col-sm-2 control-label">
		        <?=lang("admin_perfil_txt_nombre");?>
		        </label>
		        <div class="col-lg-5">
		            <input type="text" class="form-control" id="txtIntro" name="txtIntro"
		            placeholder="<?=lang("admin_perfil_txt_nombre");?>" value="<?=$user->nombre?>" readonly>		         
		        </div>
		    </div>
		    <br>
		    <div class="form-group">
		        <label for="txtIntro" class="col-lg-2 col-sm-2 control-label">
		        <?=lang("admin_perfil_txt_email");?>
		        </label>
		        <div class="col-lg-5">
		            <input type="text" class="form-control" id="txtIntro" name="txtIntro"
		            placeholder="<?=lang("admin_perfil_txt_email");?>" value="<?=$user->email?>" readonly>
		        </div>
		    </div>  
        </div>
  	</section>