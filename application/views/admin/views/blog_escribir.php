  <section class="panel">
    <header class="panel-heading">
      <?=lang("admin_escribir_title");?>
    </header>
    <div class="panel-body">
    <div id="success"></div>
    <div id="error"></div>
      <form class="form-horizontal form-escribir" id="escribir-form" role="form" method="POST"  enctype="multipart/form-data">
        <div class="form-group">
         <label for="txtTitle" class="col-lg-2 col-sm-2 control-label">
         <?=lang("admin_principal_txt_title");?></label>
          <div class="col-lg-5">
              <input type="text" class="form-control" id="txtTitle" name="txtTitle" 
              placeholder="<?=lang("admin_principal_txt_title");?>">
              <p class="help-block"><?=lang("admin_principal_txt_title_ayuda");?></p>
          </div>
        </div>
        <div class="form-group">
            <label for="txtIntro" class="col-lg-2 col-sm-2 control-label">
            <?=lang("admin_principal_txt_intro");?>
            </label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="txtIntro" name="txtIntro"
                placeholder="<?=lang("admin_principal_txt_intro");?>">
                <p class="help-block"><?=lang("admin_principal_txt_intro_ayuda");?></p>
            </div>
        </div>
        <div class="form-group">
         <label for="fileImagen" class="col-lg-2 col-sm-2 control-label">
         <?=lang("admin_principal_img_txt");?></label>
          <div class="col-lg-5">
              <input type="file" class="form-control" id="fileImagen" name="fileImagen"
              placeholder="<?=lang("admin_principal_img_txt");?>">
              <p class="help-block"><?=lang("admin_principal_img_ayuda");?></p>
          </div>
        </div>
        <div class="form-group">
          <label for="txtMensaje" class="col-lg-2 col-sm-2 control-label">
            <?=lang("admin_principal_txt_mensaje");?>
            </label>
          <div class="col-lg-10">
             <textarea class="for-control" id="txtMensaje" name="txtMensaje"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-offset-2 col-lg-10">
            <button type="submit" class="btn btn-success">
              <?=lang("admin_principal_btn_post");?>  
            </button>
          </div>
       </div>
      </form>
    </div>
  </section>
