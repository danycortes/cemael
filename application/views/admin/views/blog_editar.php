	<section class="panel">
    	<header class="panel-heading">
      		<?=lang("admin_actualizar_title");?>
    	</header>
    	<div class="panel-body">
	    <div id="success"></div>
		<div id="error"></div>
		<form class="form-horizontal form-escribir" id="editar-form" role="form" method="POST"  enctype="multipart/form-data">
			<input type="hidden" name="txtId" value="<?=$post["post_id"]?>">
		    <div class="form-group">
		     <label for="txtTitle" class="col-lg-2 col-sm-2 control-label">
		     <?=lang("admin_editar_txt_title");?></label>
		      <div class="col-lg-5">
		          <input type="text" class="form-control" id="txtTitle" name="txtTitle" value="<?=$post["post_title"];?>" placeholder="<?=lang("admin_editar_txt_title");?>">
		          <p class="help-block"><?=lang("admin_editar_txt_title_ayuda");?></p>
		      </div>
		    </div>
		    <div class="form-group">
		        <label for="txtIntro" class="col-lg-2 col-sm-2 control-label">
		        <?=lang("admin_editar_txt_intro");?>
		        </label>
		        <div class="col-lg-10">
		            <input type="text" class="form-control" id="txtIntro" name="txtIntro" value="<?=$post["post_intro"];?>"
		            placeholder="<?=lang("admin_editar_txt_intro");?>">
		            <p class="help-block"><?=lang("admin_editar_txt_intro_ayuda");?></p>
		        </div>
		    </div>
		    <div class="form-group">
		     <label for="fileImagen" class="col-lg-2 col-sm-2 control-label">
		     <?=lang("admin_editar_img_txt");?></label>
		      <div class="col-lg-5 col-sm-5">
		          <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#imgModal"><span class="fa fa-file-image-o"></span> <?=$post["ubicacion"]?></button>
		          <p class="help-block"><?=lang("admin_editar_img_ayuda");?></p>
		      </div>
		      <div class="col-lg-5 col-sm-2">
		      	<img src="<?php echo base_url().$post["ubicacion"];?>" id="imagenBlog" style="height: 40%; width: 50%;">
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="txtMensaje" class="col-lg-2 col-sm-2 control-label">
		        <?=lang("admin_editar_txt_mensaje");?>
		        </label>
		      <div class="col-lg-10">
		         <textarea class="for-control" id="txtMensaje" name="txtMensaje"><?=$post["post_content"]?></textarea>
		      </div>
		    </div>
		    <div class="form-group">
		      <div class="col-lg-offset-2 col-lg-10">
		        <button type="submit" class="btn btn-success">
		          <?=lang("admin_editar_btn_post");?>  
		        </button>
		      </div>
		   </div>
		  </form>
        </div>
    </section>
    <!-- Modal -->
  	<div class="modal fade" id="imgModal" role="dialog">
    	<div class="modal-dialog">
      	<!-- Modal content-->
	      	<div class="modal-content">
	        	<div class="modal-header">
	          		<button type="button" class="close" data-dismiss="modal">&times;</button>
	          		<h4 class="modal-title">Cambiar imagen</h4>
	        	</div>
	        	<div class="modal-body">
	        	<div id="success-img"></div>
				<div id="error-img"></div>
	          		<form class="form-horizontal form-img" id="img-form" role="form" method="POST"  enctype="multipart/form-data">
		          		<div class="form-group">
		          			<label for="fileImagen" class="col-lg-2 col-sm-2 control-label">
				     		<?=lang("admin_editar_img_txt");?></label>
				      		<div class="col-lg-10 col-sm-10">
				          		<input type="file" class="form-control" id="fileImagen" name="fileImagen" placeholder="<?=lang("admin_editar_img_txt");?>">
				          		<p class="help-block"><?=lang("admin_editar_img_ayuda");?></p>
				      		</div>
				      	</div>
				      	<div class="form-group">
				      		<label for="fileImagen" class="col-lg-2 col-sm-2 control-label"></label>
				      		<div class="col-lg-8 col-sm-8">
				      			<img src="<?php echo base_url().$post["ubicacion"];?>" id="imagenChanged">
				      		</div>
				      	</div>
				      	<input type="hidden" name="post_id" value="<?=$post["post_id"]?>"></input>
				      	<input type="hidden" name="imagen" value="<?=$post["ubicacion"]?>"></input>
				      	<div class="form-group">
				      		<div class="col-lg-2 col-sm-2 col-lg-offset-2">
				      			<button type="submit" class="btn btn-primary" style="visibility: hidden;"  id="btn_enviar_img">Cambiar imagen</button>
				      		</div>
				      	</div>
	          		</form>
	        	</div>
	        	<div class="modal-footer">
	          		<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
	        	</div>
	     	</div>
    	</div>
    </div>