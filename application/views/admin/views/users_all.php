        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/css/datatables/dataTables.bootstrap.css")?>">			
            <section class="panel">
            	<header class="panel-heading">
              		<?=lang("admin_todo_title");?>
            	</header>
            	<div class="panel-body table-responsive">
	              <table class="table table-hover" id="allposts">
                    <thead>
                        <tr>
                            <th><?=lang("admin_principal_id");?></th>
                            <th><?=lang("admin_principal_name");?></th>
                            <th><?=lang("admin_principal_date");?></th>
                            <th><?=lang("admin_principal_status");?></th>
                            <th><?=lang("admin_principal_actualizar");?></th>
                            <th><?=lang("admin_principal_eliminar");?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($posts as $post) { ?>
                    	<tr>
	                        <td><?=$post["post_id"]; ?></td>
	                        <td><?=$post["post_title"]; ?></td>
	                        <td><?=$post["post_date"]; ?></td>
	                        <td><?php if($post["active"] == 1 ){?><span class="label label-success"><?=lang("admin_principal_aprobado");?></span><?php }else{ ?><span class="label label-warning"><?=lang("admin_principal_noaprobado");?></span>
	                        	<?php } ?>
	                        </td>
	                        <td><span class="label label-primary">actualizar</span></td>
	                        <td><span class="label label-danger">eliminar</span></td>
	                    </tr>
                    <?php } ?>
                    </tbody>
            	</table>
    	        </div>
          </section>