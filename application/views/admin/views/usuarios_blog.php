	<section class="panel">
    	<header class="panel-heading ">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#usuarios">Usuarios</a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#nuevo">Nuevo usuario</a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#editar">Editar usuario</a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#eliminar">Eliminar Usuario</a>
                </li>
            </ul>
        </header>
        <div class="panel-body"><!--Usuarios Lista de usuarios-->
            <div class="tab-content">
                <div id="usuarios" class="tab-pane active">
                    <div class="panel-body table-responsive">
			            <table class="table table-hover" id="allposts">
			                <thead>
			                	<tr>
			                        <th><?=lang("admin_usuario_id");?></th>
			                        <th><?=lang("admin_usuario_nombre");?></th>
			                        <th><?=lang("admin_usuario_email");?></th>
			                        <th><?=lang("admin_usuario_apodo");?></th>
			                        <th><?=lang("admin_usuario_tipo");?></th>
			                        <th>Password</th>
			                    </tr>
			                </thead>
			                <tbody>
			                <?php foreach ($usuarios as $usuario) { ?>
			                	<tr>
			                        <td><?=$usuario["usuario_id"]; ?></td>
			                        <td><?=$usuario["nombre"]; ?></td>
			                        <td><?=$usuario["email"]; ?></td>
			                        <td><?=$usuario["apodo"]; ?></td>
			                        <td><?=$usuario["rol_nombre"]; ?></td>
			                        <td><?=$usuario["password"]; ?></td>
			                    </tr>
			                <?php } ?>
			                </tbody>
			        	</table>
    	    		</div>
            	</div><!--Usuarios Lista de usuarios-->
                <div id="nuevo" class="tab-pane">
                	<div id="success-crear"></div>
					<div id="error-crear"></div>
					<form class="form-horizontal form-escribir" id="newUser-form" role="form" method="POST">
					    <div class="form-group"><!-- select perfil user-->
					    	<label for="txtTitle" class="col-lg-2 col-sm-2 control-label">
					    		<?=lang("admin_usuario_txt_perfil");?>
					     	</label>
					      	<div class="col-lg-5">
					          <select class="form-control" id="txtPerfil" name="txtPerfil">
					          <option value="" disabled selected><?=lang("admin_usuario_txt_perfil");?></option>
					          <?php foreach($roles as $role){?>				          	
					          	<option value="<?=$role["rol_id"]; ?>"> <?=$role["rol_nombre"];?></option>	
					          <?php } ?>
					          </select>					       
					      </div>
					    </div>
					     <div class="form-group">
					     <label for="txtPartner" class="col-lg-2 col-sm-2 control-label">Partner</label>
					      <div class="col-lg-5">
					          <select class="form-control" id="txtPartner" name="txtPartner">
					          <option value="0" disabled selected>Partner</option>
					          <?php foreach($cat_partners as $partner){?>				          	
					          	<option value="<?=$partner["partnerId"]; ?>"> <?=$partner["razonSocial"];?></option>	
					          <?php } ?>
					          </select>					       
					      </div>
					    </div>
					    <br>
					    <div class="form-group"><!-- Nick name-->
					        <label for="txtApodo" class="col-lg-2 col-sm-2 control-label">
					        <?=lang("admin_usuario_txt_apodo");?>
					        </label>
					        <div class="col-lg-5">
					            <input type="text" class="form-control" id="txtApodo" name="txtApodo"
					            placeholder="<?=lang("admin_usuario_txt_apodo");?>">		         
					        </div>
					    </div>	
					    <br>
					    <div class="form-group"><!--Nombre-->
					        <label for="txtIntro" class="col-lg-2 col-sm-2 control-label">
					        <?=lang("admin_usuario_txt_nombre");?>
					        </label>
					        <div class="col-lg-5">
					            <input type="text" class="form-control" id="txtNombre" name="txtNombre"
					            placeholder="<?=lang("admin_usuario_txt_nombre");?>">		         
					        </div>
					    </div>
					    <br>
					    <div class="form-group">
					        <label for="txtIntro" class="col-lg-2 col-sm-2 control-label">
					        <?=lang("admin_usuario_txt_email");?>
					        </label>
					        <div class="col-lg-5">
					            <input type="text" class="form-control" id="txtEmail" name="txtEmail"
					            placeholder="<?=lang("admin_usuario_txt_email");?>">		         
					        </div>
					    </div> 
					    <div class="form-group">
					    	<div class="col-lg-2 col-sm-2 col-lg-offset-5">
					    		<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" value="crear_usuario" id="btn-signin"><?=lang('admin_usuario_btn_crear');?></button>
					    	</div>
					    </div> 
					</form>
                </div>
                <div id="editar" class="tab-pane">
               		<div id="success-editar"></div>
					<div id="error-editar"></div>
					<!-- search form -->
			    	<form class="form-horizontal form-escribir" id="searchEdit-form" role="form" method="POST">
			          	<div class="input-group col-lg-5 col-lg-offset-2">
			              	<input type="email" name="email" id="email" class="form-control" placeholder="<?=lang("admin_usuario_buscar")?>"/>
			              	<span class="input-group-btn">
			                	<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i> </button>
			              	</span>
			          	</div>
			      	</form>
			      	<br/>
					<form class="form-horizontal form-escribir" id="editUser-form" role="form" method="POST">
				    	<div class="form-group">
				     		<label for="txtTitle" class="col-lg-2 col-sm-2 control-label">
				     			<?=lang("admin_usuario_txt_perfil");?></label>
				      		<div class="col-lg-5">
				        	  	<select class="form-control" id="txtPerfil_edit" name="txtPerfil_edit">
					          		<option value="" disabled selected><?=lang("admin_usuario_txt_perfil");?></option>
					          		<?php foreach($roles as $role){?>				          	
					          			<option value="<?=$role["rol_id"]; ?>"> <?=$role["rol_nombre"];?></option>	
					          		<?php } ?>
					          </select>
				      		</div>
				    	</div>
				    	<br>
				    	<input type="hidden" name="txtUsuario_id_edit" id="txtUsuario_id_edit">
				    	<div class="form-group">
				        	<label for="txtApodo_edit" class="col-lg-2 col-sm-2 control-label">
				        		<?=lang("admin_usuario_txt_apodo");?>
				        	</label>
				        	<div class="col-lg-5">
				            	<input type="text" class="form-control" id="txtApodo_edit" name="txtApodo_edit" placeholder="<?=lang("admin_usuario_txt_apodo");?>">		         
				        	</div>
				    	</div>	
				    	<br>
				    	<div class="form-group">
				        	<label for="txtNombre_edit" class="col-lg-2 col-sm-2 control-label">
				        		<?=lang("admin_usuario_txt_nombre");?>
				        	</label>
					        <div class="col-lg-5">
					            <input type="text" class="form-control" id="txtNombre_edit" name="txtNombre_edit"
					            placeholder="<?=lang("admin_usuario_txt_nombre");?>">		         
					        </div>
				    	</div>
				    	<br>
				    	<div class="form-group">
				        	<label for="txtEmail_edit" class="col-lg-2 col-sm-2 control-label">
				        		<?=lang("admin_usuario_txt_email");?>
				        	</label>
				        	<div class="col-lg-5">
					            <input type="text" class="form-control" id="txtEmail_edit" name="txtEmail_edit" placeholder="<?=lang("admin_usuario_txt_email");?>">
					         </div>
				    	</div> 
				    	<div class="form-group">
				    		<div class="col-md-3 col-sm-3 col-lg-offset-2">
					    		<button class="btn btn-lg btn-success btn-block btn-signin" type="submit" value="login" id="btn-signin"><?=lang('admin_usuario_btn_newpass');?>
					    		</button>
					    	</div>
				    		<div class="col-md-2 col-sm-2 ">
				    			<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" value="login" id="btn-signin"><?=lang('admin_usuario_btn_editar');?>
				    			</button>
				    		</div>
				    	</div>
					</form>               		
                </div>
                <div id="eliminar" class="tab-pane">
                	<div id="success-eliminar"></div>
					<div id="error-eliminar"></div>
					<!-- search form -->
			    	<form class="form-horizontal form-escribir"  id="searchDel-form" role="form" method="POST">
			          	<div class="input-group col-lg-5 col-lg-offset-2">
			              	<input type="email" name="email" id="email" class="form-control" placeholder="<?=lang("admin_usuario_buscar")?>"/>
			              	<span class="input-group-btn">
			                	<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
			              	</span>
			          	</div>
			      	</form>
			      	<br/>
					<form class="form-horizontal form-escribir" id="userDel-form" role="form" method="POST">
				    	<div class="form-group">
				        	<label for="txtNombre_del" class="col-lg-2 col-sm-2 control-label">
				        		<?=lang("admin_usuario_txt_nombre");?>
				        	</label>
					        <div class="col-lg-5">
					            <input type="text" class="form-control" id="txtNombre_del" name="txtNombre_del" placeholder="<?=lang("admin_usuario_txt_nombre");?>" readonly>		         
					        </div>
				    	</div>
				    	<br>
				    	<input type="hidden" name="txtUsuario_id_del" id="txtUsuario_id_del">
				    	<div class="form-group">
				        	<label for="txtEmail_del" class="col-lg-2 col-sm-2 control-label">
				        		<?=lang("admin_usuario_txt_email");?>
				        	</label>
				        	<div class="col-lg-5">
					            <input type="text" class="form-control" id="txtEmail_del" name="txtEmail_del" placeholder="<?=lang("admin_usuario_txt_email");?>" readonly>
					         </div>
				    	</div> 
				    	<div class="form-group">
				    		<div class="col-md-3 col-sm-3 col-md-offset-4">
				    			<button class="btn btn-lg btn-danger btn-block btn-signin" type="submit" value="login" id="btn-signin"><?=lang('admin_usuario_btn_eliminar');?>
				    			</button>
				    		</div>
				    	</div>
					</form>               		
                </div>
            </div>
        </div>        
  	</section>