  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/admin/css/datatables/dataTables.bootstrap.css")?>">
  <!-- Right side column. Contains the navbar and content of the page -->
          <section class="panel">
            <header class="panel-heading">
              <?=lang("admin_principal_title");?>
            </header>
            <div class="panel-body">
               <table id="misBlogs" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                        <th><?=lang("admin_principal_id");?></th>
                        <th><?=lang("admin_principal_name");?></th>
                        <th><?=lang("admin_principal_date");?></th>
                        <th><?=lang("admin_principal_status");?></th>
                    </tr>
                  </thead>
                    <tbody>
                    <?php foreach ($posts as $post) { ?>
                    	<tr>
	                        <td><?=$post["post_id"]; ?></td>
	                        <td><?=$post["post_title"]; ?></td>
	                        <td><?=$post["post_date"]; ?></td>
	                        <td><?php if($post["active"] == 1 ){?>
	                        	<span class="label label-success"><?=lang("admin_principal_aprobado");?></span>
	                        	<?php }else{ ?>
	                        		<span class="label label-warning"><?=lang("admin_principal_noaprobado");?></span>
	                        	<?php } ?>
	                        </td>
	                    </tr>
                    <?php } ?>
                    </tbody>
            	</table>
            </div>
          </section>

