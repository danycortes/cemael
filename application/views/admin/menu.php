    <!-- header logo: style can be found in header.less -->
  <header class="header">
    <a href="/dashboard" class="logo"><img src="<?php echo base_url('assets/admin/img/Human.png');?>"></a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </a>
      <div class="navbar-right">
          <ul class="nav navbar-nav">
          <!--<li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope"></i>
                  <span class="label label-success">4</span>
              </a>funciona por si se quieren poner mensajes
          </li>-->
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-user"></i>
                      <span><?=$usuario->apodo?> <i class="caret"></i></span>
                  </a>
                  <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                      <li class="dropdown-header text-center"><?=lang('admin_menu_cuenta');?></li>
                      <li>
                          <a href="#">
                              <i class="fa fa-envelope-o fa-fw pull-right"></i>
                              <!--<span class="badge badge-danger pull-right">5</span>-->
                              <?=lang('admin_menu_mensaje');?>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="#">
                          <i class="fa fa-user fa-fw pull-right"></i>
                              <?=lang('admin_menu_perfil');?>
                          </a>
                          <a data-toggle="modal" href="#modal-user-settings">
                          <i class="fa fa-cog fa-fw pull-right"></i>
                          <?=lang('admin_menu_config');?>
                          </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="<?php echo base_url('dashboard/salir/');?>"><i class="fa fa-ban fa-fw pull-right"></i><?=lang('admin_menu_salir');?></a>
                      </li>
                  </ul>
              </li>
          </ul>
      </div>
    </nav>
  </header>
  <div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar"><!-- sidebar: style can be found in sidebar.less -->
      <div class="user-panel"><!-- Sidebar user panel -->
        <div class="pull-left image">
            <img src="<?php echo base_url('assets/admin/img/avatar5.png');?>" class="img-circle" alt="User Image" />
        </div>
        <div class="pull-left info">
            <p><?=lang("admin_menu_hello");?>, <?=$usuario->apodo?></p>
        </div>
      </div>
      <!-- search form 
      <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                  <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
          </div>
      </form>/.search form -->
      <ul class="sidebar-menu"><!-- sidebar menu: : style can be found in sidebar.less -->
          <?php
         // var_dump($_SESSION);
          $permiso_user = $_SESSION['permiso']->rol_nombre;  
          if($permiso_user == "admin"){?>
          <li class="<?=$todo_blogs_active?>">
              <a href="<?php echo base_url('dashboard/todos_blogs')?>">
                  <i class="fa fa-home"></i><span><?=lang('admin_todo_title');?></span>
              </a>
          </li>
          <?php }?>
          <?php if($permiso_user == "admin" || $permiso_user == "supervisor" || $permiso_user == "escritor" ){?>
          <li class="<?=$mis_blogs_active?>">
              <a href="<?php echo base_url("dashboard/mis_blogs");?>">
                  <i class="fa fa-list-alt"></i> <span><?=lang('admin_principal_title');?></span>
              </a>
          </li>
          <li class="<?=$escribir_blogs_active?>">
              <a href="<?php echo base_url("dashboard/escribir/");?>"> 
                  <i class="fa fa-pencil"></i><span><?=lang('admin_escribir_title');?></span>
              </a>
          </li>
          <li class="<?=$perfil_blogs_active?>">
              <a href="<?php echo base_url("dashboard/perfil/");?>"> 
                  <i class="fa fa-user"></i><span><?=lang('admin_perfil_title');?></span>
              </a>
          </li>
          <?php }?>
          <?php if($permiso_user == "admin"){?>
          <li class="<?=$usuarios_blogs_active?>">
              <a href="<?php echo base_url("dashboard/usuarios/");?>">
                  <i class="fa fa-users"></i> <span><?=lang('admin_usuario_title');?></span>
              </a>
          </li>
          <?php }?>
          <?php if($permiso_user == "admin"){?>
          <?php }?>
          
      </ul>
    </section>
  </aside>