﻿<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cemael-Revistas</title>
        <meta name="keywords" content="HTML5 Theme" />
        <meta name="description" content="Cemael">
        <meta name="author" content="cwa.mx">

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">

        <link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/animate.min.css");?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/themify/themify.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/scrollbar/scrollbar.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/magnific-popup/magnific-popup.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/swiper/swiper.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/cubeportfolio/css/cubeportfolio.min.css");?>"  rel="stylesheet" type="text/css"/>

        <link href="<?php echo base_url("assets/css/style.css");?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/global.css");?>" rel="stylesheet" type="text/css"/>

        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    </head>

    <body>
        <header class="navbar-fixed-top s-header js__header-sticky js__header-overlay">
            <div class="s-header__navbar">
                <div class="s-header__container">
                    <div class="s-header__navbar-row">
                        <div class="s-header__navbar-row-col">
                            <div class="s-header__logo">
                                <a href="" class="s-header__logo-link">
                                <img class="s-header__logo-img s-header__logo-img-default" src="<?php echo base_url("assets/images/logo.png");?>" alt="Cemael Logo">
                                    <img class="s-header__logo-img s-header__logo-img-shrink" src="<?php echo base_url("assets/images/logo-dark.png");?>" alt="Cemael Logo">
                                </a>
                            </div>
                        </div>
                        <div class="s-header__navbar-row-col"></div>
                        <div>
                            <br><br>
                            <a class="text-uppercase g-font-size-600--xs g-font-weight--700 g-color--orange g-letter-spacing--2 " href="<?php echo base_url() ?>">Regresar</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div id="Libros" class="js__parallax-window magazines-container">
            <div class="container g-padding-y-10--xs g-padding-y-125--sm">
                <h2>Revista</h2>
                <div class="magazines">
                    <a href="http://online.fliphtml5.com/ultx/iigj/">
                        <div class="cover">
                            <img src="assets/images/revistas/2019-ene-26.png" alt="">
                        </div>
                        <h3>El hallazgo al encontrarte</h3>
                    </a>
                    <a href="http://online.fliphtml5.com/ultx/rvia/">
                        <div class="cover">
                            <img src="assets/images/revistas/2018-06jun-24.png" alt="">
                        </div>
                        <h3>Aprovechar las oportunidades de nuestras propias vidas: Un llamado a la creatividad</h3>
                    </a>
                    <a href="http://online.fliphtml5.com/ultx/iftu/">
                        <div class="cover">
                            <img src="assets/images/revistas/2018-03mar-23.png" alt="">
                        </div>
                        <h3>La apertura a ser fecundados: La apertura hacia sí mismo</h3>
                    </a>
                    <a href="http://online.fliphtml5.com/ultx/qzck/">
                        <div class="cover">
                            <img src="assets/images/revistas/2017-12dic-22.png" alt="">
                        </div>
                        <h3>El sentido: Una pregunta que lleva tu nombre</h3>
                    </a>
                    <a href="http://online.fliphtml5.com/ultx/totr/">
                        <div class="cover">
                            <img src="assets/images/revistas/2017-09sep-21.png" alt="">
                        </div>
                        <h3>Intimidad e identidad</h3>
                    </a>
                    <a href="http://online.fliphtml5.com/ultx/bzun/">
                        <div class="cover">
                            <img src="assets/images/revistas/2017-06jun-20.png" alt="">
                        </div>
                        <h3>Sentir la vida</h3>
                    </a>
                </div>
            </div>
        </div>

        <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>

        <!-- Vendor -->
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.migrate.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/bootstrap/js/bootstrap.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.smooth-scroll.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.back-to-top.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/scrollbar/jquery.scrollbar.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/magnific-popup/jquery.magnific-popup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/swiper/swiper.jquery.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/waypoint.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/counterup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.parallax.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.wow.min.js");?>"></script>

        <!-- General Components and Settings -->
        <script type="text/javascript" src="<?php echo base_url("assets/js/global.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/header-sticky.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/scrollbar.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/magnific-popup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/swiper.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/counter.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/portfolio-3-col.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/parallax.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/google-map.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/wow.min.js");?>"></script>

        <script type="text/javascript" src="<?php echo base_url("assets/js/scriptss.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/script.js");?>"></script>
    </body>
</html>
