﻿<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- Begin Head -->
    <head>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cemael-Links</title>
        <meta name="keywords" content="HTML5 Theme" />
        <meta name="description" content="Cemael">
        <meta name="author" content="cwa.mx">

        <!-- Web Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">

        <!-- Vendor Styles -->
        <link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/animate.min.css");?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/themify/themify.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/scrollbar/scrollbar.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/magnific-popup/magnific-popup.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/swiper/swiper.min.css");?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/vendor/cubeportfolio/css/cubeportfolio.min.css");?>"  rel="stylesheet" type="text/css"/>

        <!-- Theme Styles -->
        <link href="<?php echo base_url("assets/css/style.css");?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url("assets/css/global.css");?>" rel="stylesheet" type="text/css"/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    </head>
    <!-- End Head -->

    <!-- Body -->
    <body>

        <!--========== HEADER ==========-->
             <!--========== HEADER ==========-->
        <header class="navbar-fixed-top s-header js__header-sticky js__header-overlay">
            <!-- Navbar -->
            <div class="s-header__navbar">
                <div class="s-header__container">
                    <div class="s-header__navbar-row">
                        <div class="s-header__navbar-row-col">
                            <!-- Logo -->
                            <div class="s-header__logo">
                                <a href="" class="s-header__logo-link">
                                    <img class="s-header__logo-img s-header__logo-img-default" src="<?php echo base_url("assets/images/logo.png");?>" alt="Cemael Logo">
                                    <img class="s-header__logo-img s-header__logo-img-shrink" src="<?php echo base_url("assets/images/logo-dark.png");?>" alt="Cemael Logo">
                                </a>
                            </div>
                            <!-- End Logo -->
                        </div>
                        <div class="s-header__navbar-row-col">
                            <!-- Trigger -->
                            <!-- <a href="javascript:void(0);" class="s-header__trigger js__trigger">
                                <span class="s-header__trigger-icon"></span>
                                <svg x="0rem" y="0rem" width="3.125rem" height="3.125rem" viewbox="0 0 54 54">
                                    <circle fill="transparent" stroke="#fff" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
                                </svg>
                            </a> -->
                            <!-- End Trigger -->
                        </div>
			<div >
						
						<br><br><a class="text-uppercase g-font-size-200--xs g-font-weight--700 g-color--orange g-letter-spacing--2 " href="<?php echo base_url() ?>">Regresar</a>   
						
						</div>
                    </div>
                </div>
            </div>
            <!-- End Navbar -->

            
        </header>

           
        <!--========== END HEADER ==========-->
		<div class="container">
  			
			 <div class="g-text-center--xs g-margin-b-80--xs">
			 <br><br> <br><br>
                    <p class=" g-font-size-36--xs g-font-weight--700 g-color--orange g-letter-spacing--2 g-margin-b-25--xs">Links</p>
                  <!--   <h2 class="g-font-size-32--xs g-font-size-30--md">Contáctanos</h2> -->
             </div>
			 
			 <div class="row g-margin-b-60--xs g-margin-b-70--md">
                <div class="col-sm-12 g-margin-b-60--xs g-margin-b-0--md">
				<table style="width:100%">
							  <tr>
								<th>Organización </th>
								<th>Sitio Intenet</th> 
							   
							  </tr>
							  <tr>
								<td>Alfried Längle</td>
								<td><a class="orange" href="http://www.laengle.info" target="_blank">www.laengle.info</a> </td>
							  </tr>
							  <tr>
								<td>Sociedad Internacional de Análisis Existencial y Logoterapia (Viena)</td>
								<td><a class="orange" href="http://www.logoterapia.org" target="_blank">www.logoterapia.org</a></td>
							  </tr>
							  <tr>
								<td>GLE-Internacional</td>
								<td><a class="orange" href="http://www.existenzanalyse.org" target="_blank">www.existenzanalyse.org</a><br><a class="orange" href="http://www.existential-analysis.org" target="_blank">www.existential-analysis.org</a> </td>
							  </tr>
							  <tr>
								<td>GLE-Austria</td>
								<td><a class="orange" href="http://www.gle.at" target="_blank">www.gle.at</a></td>
							  </tr>
							  <tr>
								<td>GLE-Alemania</td>
								<td><a class="orange" href="http://www.gle-d.de" target="_blank">www.gle-d.de</a></td>
							  </tr>
							  <tr>
								<td>Gesellschaft für Existenzanalyse Schweiz-Suiza</td>
								<td><a class="orange" href="http://www.existenzanalyse.ch‬‬" target="_blank">www.existenzanalyse.ch‬‬</a></td>
							  </tr>
							  <tr>
								<td>SLEA-Rep. Checa</td>
								<td><a class="orange" href="http://www.slea.cz" target="_blank">www.slea.cz</a> </td>
							  </tr>
							  <tr>
								<td>GLE-Rusia</td>
								<td><a class="orange" href="http://www.ieapp.ru" target="_blank">www.ieapp.ru</a> </td>
							  </tr>
							  <tr>
								<td>GLE México</td>
								<td><a class="orange" href="http://www.analisisexistencial.org" target="_blank">www.analisisexistencial.org</a> </td>
							  </tr>
							  <tr>
								<td>GLE-Ö Vorarlber</td>
								<td> <a class="orange" href="http://www.existenzanalyse-vorarlberg.at" target="_blank">www.existenzanalyse-vorarlberg.at</a></td>
							  </tr>
							  <tr>
								<td>DDr. Alfried y Dr. Silvia Längle</td>
								<td><a class="orange" href="http://www.laengle.info" target="_blank">www.laengle.info</a> </td>
							  </tr>
							  <tr>
								<td>Academia Alemana de Existencialismo y Logoterapia</td>
								<td><a class="orange" href="http://www.existenzanalyse.com" target="_blank">www.existenzanalyse.com</a></td>
							  </tr>
							  <tr>
								<td>Instituto de Logoterapia y Existencialismo de Salzburgo</td>
								<td><a class="orange" href="http://www.existenzanalyse-salzburg.at" target="_blank">www.existenzanalyse-salzburg.at</a></td>
							  </tr>
							  <tr>
								<td>Istituto di Analisi Relazionale (I.A.R. Esistenziale)Italia</td>
								<td><a class="orange" href="http://www.regazzo.org" target="_blank">www.regazzo.org</a></td>
							  </tr>
							   <tr>
								<td>Instituto Chileno de Análisis Existencia (ICAE) Chile</td>
								<td><a class="orange" href="http://www.icae.cl" target="_blank">www.icae.cl</a></td>
							  </tr>
							   <tr>
								<td>GLE Argentina</td>
								<td><a class="orange" href="http://www.gle-argentina.com.ar" target="_blank">www.gle-argentina.com.ar</a></td>
							  </tr>
							   <tr>
								<td>GLE – Polonia</td>
								<td><a class="orange" href="http://www.analiza-egzystencjalna.pl" target="_blank">www.analiza-egzystencjalna.pl</a></td>
							  </tr>
							   <tr>
								<td>SEAL Rumania</td>
								<td><a class="orange" href="http://www.analizaexistentiala.ro" target="_blank">www.analizaexistentiala.ro</a> </td>
							  </tr>
							   <tr>
								<td>GLE Canadá</td>
								<td><a class="orange"href="http://www.existentialanalysis.ca" target="_blank">www.existentialanalysis.ca</a></td>
							  </tr>
							  
							 
				
				</table>		
                           
				
							
							
                        </div>
						</div>
                    </div>
                </div>
                
            </div>
  			<div class="row">
  				
  				<div class="col-md-12">
  				
				
				
  				</div> 
 	  			
  			</div>
    	</div>
        
      

        

      
       
        <!--========== END PAGE CONTENT ==========-->

      

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>

        <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
        <!-- Vendor -->
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.migrate.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/bootstrap/js/bootstrap.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.smooth-scroll.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.back-to-top.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/scrollbar/jquery.scrollbar.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/magnific-popup/jquery.magnific-popup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/swiper/swiper.jquery.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/waypoint.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/counterup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.parallax.min.js");?>"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsXUGTFS09pLVdsYEE9YrO2y4IAncAO2U"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/vendor/jquery.wow.min.js");?>"></script>

        <!-- General Components and Settings -->
        <script type="text/javascript" src="<?php echo base_url("assets/js/global.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/header-sticky.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/scrollbar.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/magnific-popup.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/swiper.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/counter.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/portfolio-3-col.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/parallax.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/google-map.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/components/wow.min.js");?>"></script>
		
		 <script type="text/javascript" src="<?php echo base_url("assets/js/scriptss.js");?>"></script>
		 <script type="text/javascript" src="<?php echo base_url("assets/js/script.js");?>"></script>
        <!--========== END JAVASCRIPTS ==========-->

    </body>
    <!-- End Body -->
</html>
