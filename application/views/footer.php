 <footer id="footer">
    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
      <div class="container text-center">
        <div class="footer-logo">
          <a href="index.html"><img class="img-responsive" src="<?php echo base_url("assets/images/logo.png")?>" alt=""></a>
        </div>
        <div class="social-icons">
          <ul>
              <li><a class="envelope" href="mailto:contacto@travaller.mx"><i class="fa fa-envelope"></i></a></li>
              <li><a class="twitter" href="https://twitter.com/TravellerMex"><i class="fa fa-twitter"></i></a></li> 
              <li><a class="facebook" href="https://www.facebook.com/travellermex/"><i class="fa fa-facebook"></i></a></li>
              <li><a class="instagram" href="https://www.instagram.com/travellermex/"><i class="fa fa-instagram"></i></a></li>
              <li><a class="pinterest" href="https://es.pinterest.com/travellermex/"><i class="fa fa-pinterest"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <p class="footer_aviso"><?=lang('footer_aviso');?></p>
          </div>
          <div class="col-sm-6">
            <p>&copy; 2016 Traveller.mx</p>
          </div>
          <div class="col-sm-6">
            <p class="pull-right">Powered by <a href="http://www.cwa.mx/">CWA.MX</a></p>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.js")?>"></script>
  <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.min.js")?>"></script>
  <!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
  <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.inview.min.js")?>"></script>
  <script type="text/javascript" src="<?php echo base_url("assets/js/wow.min.js")?>"></script>
  <script type="text/javascript" src="<?php echo base_url("assets/js/mousescroll.js")?>"></script>
  <script type="text/javascript" src="<?php echo base_url("assets/js/smoothscroll.js")?>"></script>
  <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.countTo.js")?>"></script>
  <script type="text/javascript" src="<?php echo base_url("assets/js/lightbox.min.js")?>"></script>
  <script type="text/javascript" src="<?php echo base_url("assets/js/main.js")?>"></script>

</body>
</html>