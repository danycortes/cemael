<!DOCTYPE html>
<html lang="en">
<head>
  



	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>CEMAEL | Articulos</title>
	<link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/animate.min.css"); ?>" rel="stylesheet"> 
	<link href="<?php echo base_url("assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/lightbox.css"); ?>" rel="stylesheet">
	<!-- <link href="<?php echo base_url("assets/css/main.css"); ?>" rel="stylesheet">
	<link id="css-preset" href="<?php echo base_url("assets/css/presets/preset1.css"); ?>" rel="stylesheet"> -->
	<link href="<?php echo base_url("assets/css/responsive.css")?>" rel="stylesheet">
	
	 <link href="<?php echo base_url("assets/css/style.css");?>" rel="stylesheet" type="text/css"/>
	  <link href="<?php echo base_url("assets/css/global.css");?>" rel="stylesheet" type="text/css"/>
	   
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="images/favicon.ico">
</head>
<body>
	 <header class="navbar-fixed-top s-header js__header-sticky js__header-overlay">
				<!-- Navbar -->
				<div class="s-header__navbar">
					<div class="s-header__container">
						<div class="s-header__navbar-row">
							<div class="s-header__navbar-row-col">
								<!-- Logo -->
								<div class="s-header__logo">
									<a href="" class="s-header__logo-link">
										<img class="s-header__logo-img s-header__logo-img-default" src="<?php echo base_url("assets/images/logo.png");?>" alt="Cemael Logo">
										<img class="s-header__logo-img s-header__logo-img-shrink" src="<?php echo base_url("assets/images/logo-dark.png");?>" alt="Cemael Logo">
									</a>
								</div>
								<!-- End Logo -->
							</div>
							<div class="s-header__navbar-row-col">
								<!-- Trigger -->
								<!-- <a href="javascript:void(0);" class="s-header__trigger js__trigger">
									<span class="s-header__trigger-icon"></span>
									<svg x="0rem" y="0rem" width="3.125rem" height="3.125rem" viewbox="0 0 54 54">
										<circle fill="transparent" stroke="#fff" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
									</svg>
								</a> -->
								<!-- End Trigger -->
							</div>
							<div >
							
							<br><br><a class="text-uppercase g-font-size-200--xs g-font-weight--700 g-color--orange g-letter-spacing--2 " href="<?php echo base_url() ?>">Regresar</a>   
							
							</div>
						</div>
					</div>
				</div>
				<!-- End Navbar -->

				
			</header>
		
	<!-- <header id="home"> -->
		<!-- <div class="main-nav"> -->
  			<!-- <div class="container"> -->
    			<!-- <div class="navbar-header"> -->
      				<!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> -->
            			<!-- <span class="sr-only">Toggle navigation</span> -->
            			<!-- <span class="icon-bar"></span> -->
            			<!-- <span class="icon-bar"></span> -->
            			<!-- <span class="icon-bar"></span> -->
      				<!-- </button> -->
      				<!-- <a class="navbar-brand" href="<?php echo base_url() ?>"> -->
        			<!-- <h1><img class="img-responsive" src="<?php echo base_url("assets/images/logo.png")?>" alt="logo"></h1> -->
      				<!-- </a>  -->
					
						<!-- <br><br><a class="izq text-uppercase g-font-size-200--xs g-font-weight--700 g-color--orange g-letter-spacing--2 " href="<?php echo base_url() ?>">Regresar</a>    -->
    			<!-- </div> -->
				        
				
        		<!-- <!-- <div class="collapse navbar-collapse"> -->
	          		<!-- <ul class="nav navbar-nav navbar-right">                  -->
	            		<!-- <li class="scroll"><a href="<?php echo base_url() ?>#home"><?=lang("menu_home");?></a></li> -->
			            <!-- <li class="scroll"><a href="<?php echo base_url() ?>#portfolio"><?=lang("menu_planes");?></a></li> -->
			            <!-- <li class="scroll"><a href="<?php echo base_url() ?>#pricing"><?=lang("menu_precios");?></a></li> -->
			            <!-- <li class="scroll"><a href="<?php echo base_url() ?>#about-us"><?=lang("menu_nosotros");?></a></li>  -->
			            <!-- <li class="scroll active"><a href="<?php echo base_url() ?>#blog"><?=lang("menu_blog");?></a></li> -->
			            <!-- <li class="scroll"><a href="<?php echo base_url() ?>#contact"><?=lang("menu_contacto");?></a></li> -->
          			<!-- </ul> -->
        		<!-- </div> --> 
				
  			<!-- </div> -->
		<!-- </div><!--/#main-nav--> 
  	<!-- </header><!--/#home--> 
		<br><br> <br><br>
  		<div class="container">
  			<h2 class="page-header">  ARTICULOS</h2>
  			<div class="row">
  				<div class="col-md-8" id="paginas">
  				<?=$pages?>
  				</div>
  				<div class="col-md-8">
  				<?Php foreach($posts as $post) { ?>
					<h1><a href="<?php echo base_url('blog/');?><?=$post['post_slug'];?>"><?=$post["post_title"]; ?></post></a></h1>
					<p>
	                 <?=$post["post_intro"]; ?>
	                </p>
					<p class="lead">
						<!-- by <a href="index.php"><?=$post["nombre"]?></a> -->
	                </p>
	              <!--   <p><span class="glyphicon glyphicon-time"></span><?=$post["post_date"];?></p> -->
	                <hr>
	                <img class="img-responsive" src="<?php echo base_url().$post['ubicacion']; ?>" alt="">
	                <hr>
	                
	               <a class="btn1" href="<?php echo base_url('blog/');?><?=$post['post_slug'];?>">LEER<span class="glyphicon glyphicon-chevron-right"></span></a>
	                <hr>
	                <?Php } ?>
  				</div> 
 	  			<div class="col-md-4"><!-- Blog Sidebar Widgets Column -->
		        	<div class="well siguenos" id="side-siguenos"><!-- Blog Search Well -->
	    	            <h4><?=lang("blog_siguenos");?></h4>
	        	        <div class="social-icons">
			          		<ul style="list-style-type:none">
									<!-- <li><a class="envelope" href="mailto:contacto@travaller.mx"><i class="fa fa-envelope"></i></a></li> -->
					            	<li class="Artic"><a class="twitter" href=""><i class=" orange fa fa-twitter"></i></a></li> 
					            	<li class="Artic"><a class="facebook" href="https://www.facebook.com/CEMAELPosgrados/?ref=br_rs"><i class="orange fa fa-facebook"></i></a></li>
					            	<li class="Artic"><a class="instagram" href=""><i class="orange fa fa-instagram"></i></a></li>
					             	<li class="Artic"><a class="pinterest" href=""><i class="orange fa fa-pinterest"></i></a></li>
			          		</ul>
		          		</div>
	            	</div>
            		<!-- <div class="well"> -->
	            	<!-- <h4>Blog Categories</h4>
			            <div class="row">
			                <div class="col-lg-6">
			                    <ul class="list-unstyled">
			                        <li><a href="#">Category Name</a>
			                        </li>
			                        <li><a href="#">Category Name</a>
			                        </li>
			                        <li><a href="#">Category Name</a>
			                        </li>
			                        <li><a href="#">Category Name</a>
			                        </li>
			                    </ul>
			                </div><!-- /.col-lg-6 -->
			              <!--   <div class="col-lg-6">
			                    <ul class="list-unstyled">
			                        <li><a href="#">Category Name</a>
			                        </li>
			                        <li><a href="#">Category Name</a>
			                        </li>
			                        <li><a href="#">Category Name</a>
			                        </li>
			                        <li><a href="#">Category Name</a>
			                        </li>
			                    </ul>
			                </div> --><!-- /.col-lg-6 -->
			           <!--  </div> --><!-- /.row -->
	        		<!-- </div>  -->
  				</div>
  			</div>
    	</div>
  	<footer id="footer">
	    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
	    	<div class="container text-center">
	        	<div class="footer-logo">
	          		<a href="<?php echo base_url() ?>"><img class="img-responsive" src="<?php echo base_url("assets/images/logo.png")?>" alt=""></a>
	        	</div>
	        	<div class="social-icons">
	          	<!--	<ul style="list-style-type:none">
        				<!- <li><a class="envelope" href="mailto:contacto@travaller.mx"><i class="fa fa-envelope"></i></a></li> -
		            	<li><a class="twitter" href="https://twitter.com/TravellerMex"><i class="orange fa fa-twitter"></i></a></li> 
		            	<li><a class="facebook" href="https://www.facebook.com/travellermex/"><i class=" orange fa fa-facebook"></i></a></li>
		            	<li><a class="instagram" href="https://www.instagram.com/travellermex/"><i class="orange fa fa-instagram"></i></a></li>
		             	<li><a class="pinterest" href="https://es.pinterest.com/travellermex/"><i class="orange fa fa-pinterest"></i></a></li>
	          		</ul>-->
	        	</div>
	      	</div>
	    </div>
<!-- 	    <div class="footer-bottom">
	    	<div class="container">
	        	<div class="row">
	          		<div class="col-sm-12">
	            		<p class="footer_aviso"><?=lang('footer_aviso');?></p>
	          		</div>
		          	<div class="col-sm-6">
		            	<p>&copy; 2016 Traveller.mx</p>
		          	</div>
		          	<div class="col-sm-6">
		            	<p class="pull-right">Powered by <a href="http://www.cwa.mx/">CWA.MX</a></p>
		          	</div>
	        	</div>
	      	</div>
	    </div> -->
	</footer>
	<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.min.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.inview.min.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/wow.min.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/mousescroll.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/smoothscroll.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.countTo.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/lightbox.min.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/blog.js")?>"></script>
</body>
</html>