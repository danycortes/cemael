<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="cemael.mx">
	<meta property="og:url" content="<?php echo base_url('/blog')."/".$post['post_title'];?>"/>
	
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?=$post['post_title'];?>" />
	<meta property="og:description"  content="<?=$post['post_intro'];?>" />
	<meta property="og:image" content="<?php echo base_url().$post['ubicacion']?>" />

	<title>CEMAEL | <?=$post['post_title'];?></title>
	<link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/animate.min.css"); ?>" rel="stylesheet"> 
	<link href="<?php echo base_url("assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/lightbox.css"); ?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/main.css"); ?>" rel="stylesheet">
	<link id="css-preset" href="<?php echo base_url("assets/css/presets/preset1.css"); ?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/responsive.css")?>" rel="stylesheet">
	
	 <link href="<?php echo base_url("assets/css/style.css");?>" rel="stylesheet" type="text/css"/>
	
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="images/favicon.ico">
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div><!--Loader-->
	
	  
        </header>
	
	<header id="home">
		<div class="main-nav">
  			<div class="container">
    			<div class="navbar-header">
      				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            			<span class="sr-only">Toggle navigation</span>
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>
      				</button>
      				<a class="navbar-brand" href="<?php echo base_url() ?>">
        			<h1><img class="img-responsive" src="<?php echo base_url("assets/images/logo.png")?>" alt="logo"></h1>
      				</a> 
					
    			</div>
        	<!-- 	<div class="collapse navbar-collapse">
	          		<ul class="nav navbar-nav navbar-right">                 
	            		<li class="scroll"><a href="<?php echo base_url() ?>#home"><?=lang("menu_home");?></a></li>
			            <li class="scroll"><a href="<?php echo base_url() ?>#portfolio"><?=lang("menu_planes");?></a></li>
			            <li class="scroll"><a href="<?php echo base_url() ?>#pricing"><?=lang("menu_precios");?></a></li>
			            <li class="scroll"><a href="<?php echo base_url() ?>#about-us"><?=lang("menu_nosotros");?></a></li> 
			            <li class="scroll active"><a href="<?php echo base_url() ?>#blog"><?=lang("menu_blog");?></a></li>
			            <li class="scroll"><a href="<?php echo base_url() ?>#contact"><?=lang("menu_contacto");?></a></li>
          			</ul>
        		</div> -->
  			</div>
		</div><!--/#main-nav-->
  	</header><!--/#home-->
		<br><br><br>  
  		<div class="container">
  			<div class="row">
  				<div class="col-md-8">
				
	  				<h1><?=$post['post_title'];?></h1><!-- Title -->
	              <!--   <p class="lead">por <a href="#"><?=$post['nombre'];?></a></p> --><!-- Author -->
	                <hr>
	                	<img class="img-responsive" src="<?php echo base_url().$post['ubicacion']?>" alt="">
	                <hr>
	                <p class="lead"><?=$post['post_intro'];?></p>
	                <p><?=$post['post_content']?></p>
			
	                <hr>
  				</div> 
 	  			<div class="col-md-4"><!-- Blog Sidebar Widgets Column -->
 	  				<div class="row">
			        	<div class="well siguenos" id="side-siguenos"><!-- Blog Search Well -->
		    	            <h4><?=lang("blog_siguenos");?></h4>
		        	        <div class="social-icons">
				          		<ul style="list-style-type:none">
					            <!-- 	<li><a class="envelope" href="mailto:contacto@travaller.mx"><i class="fa fa-envelope"></i></a></li>
					            	<li><a class="twitter" href="https://twitter.com/TravellerMex"><i class="fa fa-twitter"></i></a></li> 
					            	<li><a class="facebook" href="https://www.facebook.com/CEMAELPosgrados/?ref=br_rs"><i class="fa fa-facebook"></i></a></li>
					            	<li><a class="instagram" href="https://www.instagram.com/travellermex/"><i class="fa fa-instagram"></i></a></li>
					             	<li><a class="pinterest" href="https://es.pinterest.com/travellermex/"><i class="fa fa-pinterest"></i></a></li> -->
									
					            	<li class="Artic"><a class="twitter" href=""><i class="orange fa fa-twitter"></i></a></li> 
					            	<li class="Artic"><a class="facebook" href="https://www.facebook.com/CEMAELPosgrados/?ref=br_rs"><i class="orange fa fa-facebook"></i></a></li>
					            	<li class="Artic"><a class="instagram" href=" "><i class="orange fa fa-instagram"></i></a></li>
					             	<li class="Artic"><a class="pinterest" href=" "><i class="orange fa fa-pinterest"></i></a></li> 
				          		</ul>
			          		</div>
		            	</div>
            			<div class="well">
			            	<h4>Reciente en Facebook</h4>
				            <div class="row">
				                <div class="col-lg-12">
				                    <!-- <div class="fb-page" data-href="https://www.facebook.com/Traveller-714058665426272/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Traveller-714058665426272/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Traveller-714058665426272/">Traveller</a></blockquote></div> -->
									<div class="fb-page" data-href="https://www.facebook.com/CEMAELPosgrados/?ref=br_rs" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/CEMAELPosgrados/?ref=br_rs" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/CEMAELPosgrados/?ref=br_rs">CEMAEL</a></blockquote></div>
								</div><!-- /.col-lg-6 -->
			            	</div><!-- /.row -->
	        			</div>
	        		</div>
  				</div>
  				<div class="col-md-8">
  					<div class="fb-comments" data-href="http://cwa.mx/cemael/blog/<?=$post['post_slug'];?>" data-numposts="10" width="100%"></div>
  				</div>
  			</div>
    	</div>
  	<footer id="footer">
	    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
	    	<div class="container text-center">
	        	<div class="footer-logo">
	          		<a href="index.html"><img class="img-responsive" src="<?php echo base_url("assets/images/logo.png")?>" alt=""></a>
	        	</div>
	        	<div class="social-icons">
	          	<!--	<ul style="list-style-type:none">
				 	<li><a class="envelope" href="mailto:contacto@travaller.mx"><i class="fa fa-envelope"></i></a></li>
		            	<li><a class="twitter" href="https://twitter.com/TravellerMex"><i class="fa fa-twitter"></i></a></li> 
		            	<li><a class="facebook" href="https://www.facebook.com/CEMAELPosgrados/?ref=br_rs"><i class="fa fa-facebook"></i></a></li>
		            	<li><a class="instagram" href="https://www.instagram.com/travellermex/"><i class="fa fa-instagram"></i></a></li>
		             	<li><a class="pinterest" href="https://es.pinterest.com/travellermex/"><i class="fa fa-pinterest"></i></a></li> 
        			
		            	<li><a class="twitter" href=""><i class="orange fa fa-twitter"></i></a></li> 
		            	<li><a class="facebook" href="https://www.facebook.com/CEMAELPosgrados/?ref=br_rs"><i class="orange fa fa-facebook"></i></a></li>
		            	<li><a class="instagram" href=""><i class="orange fa fa-instagram"></i></a></li>
		             	<li><a class="pinterest" href=""><i class="orange fa fa-pinterest"></i></a></li>
	          		</ul>-->
	        	</div>
	      	</div>
	    </div>
	  <!--   <div class="footer-bottom">
	    	<div class="container">
	        	<div class="row">
	          		<div class="col-sm-12">
	            		<p class="footer_aviso">< ?=lang('footer_aviso');?></p>
	          		</div>
		          	<div class="col-sm-6">
		            	<p>&copy; 2016 Traveller.mx</p>
		          	</div>
		          	<div class="col-sm-6">
		            	<p class="pull-right">Powered by <a href="http://www.cwa.mx/">CWA.MX</a></p>
		          	</div>
	        	</div>
	      	</div>
	    </div> -->
	</footer>
	<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.min.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.inview.min.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/wow.min.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/mousescroll.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/smoothscroll.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.countTo.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/lightbox.min.js")?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/blog.js")?>"></script>
</body>
</html>