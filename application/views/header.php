<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Traveller</title>
  <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/animate.min.css"); ?>" rel="stylesheet"> 
  <link href="<?php echo base_url("assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/lightbox.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/main.css"); ?>" rel="stylesheet">
  <link id="css-preset" href="<?php echo base_url("assets/css/presets/preset1.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/responsive.css")?>" rel="stylesheet">

  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->
  
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.png");?>">
</head><!--/head-->

<body>

  <!--.preloader-->
  <div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>
  <!--/.preloader-->

  <header id="home">
<!--/#home-slider-->
    <div class="main-nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>">
            <h1><img class="img-responsive" src="<?php echo base_url("assets/images/logo.png")?>" alt="logo"></h1>
          </a>                    
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">                 
            <li class="scroll active"><a href="<?php echo base_url();?>#home"><?=lang("menu_home");?></a></li>
            <li class="scroll"><a href="<?php echo base_url();?>#portfolio"><?=lang("menu_planes");?></a></li>
            <li class="scroll"><a href="<?php echo base_url();?>#pricing"><?=lang("menu_precios");?></a></li>
            <li class="scroll"><a href="<?php echo base_url();?>#about-us"><?=lang("menu_nosotros");?></a></li>                     
            <li class="scroll"><a href="<?php echo base_url();?>#blog"><?=lang("menu_blog");?></a></li>
            <li class="scroll"><a href="<?php echo base_url();?>#contact"><?=lang("menu_contacto");?></a></li>       
          </ul>
        </div>
      </div>
    </div><!--/#main-nav-->
  </header><!--/#home-->