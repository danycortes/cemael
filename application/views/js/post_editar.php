<script src="<?php echo base_url('assets/admin/js/tinymce/tinymce.min.js');?>" type="text/javascript">
</script>
<script type="text/javascript">
	$('document').ready(function(){ 
		tinymce.init({
			selector:'#txtMensaje'
		});

		$("#editar-form").validate({
      		rules:{
			   	txtTitle: {
			   		required: true,
			   		},
			   	txtIntro: {
			   		required: true,
			   		},
		    	txtMensaje:{
		    			required:true,
		    		},
    		},
       		messages:
    		{	
            	txtTitle:{
                      required: "<?=lang('admin_principal_error_title');?>"
                    },
                txtIntro:{
                	required: "<?=lang('admin_principal_error_intro');?>"
                },
            	fileImage:{
                      required: "<?=lang('admin_principal_error_imagen');?>",
                      extension:"mal"
                    },
            	txtMensaje: {
            		required : "<?=lang('admin_principal_error_mensaje');?>"
            	}
       		},
    		submitHandler: updatePostForm 
       		});  
    	/* validation */
    	/* Post update submit */
    	function updatePostForm(){  
    		tinymce.triggerSave();
   			var data =  new FormData($('form')[0]); // $("#escribir-form").serialize();
   			// save TinyMCE instances before serialize
   			$.ajax({  
   				type : 'POST',
   				url  : '<?php echo base_url("ValidationBlog/update_post/");?>',
   				data : data,
   				contentType: false,
   				processData: false,
   				beforeSend: function(){ 
    				$("#error").fadeOut();
    				$("#success").fadeOut();
   				},
   				success :  function(response){      
     			if(response == "success"){
     				$("#success").fadeIn(1000, function(){      
    				$("#success").html('<div class="alert alert-success">&nbsp; '+response+' !</div>');
    			});
     			}else{
      					$("#error").fadeIn(1000, function(){      
    					$("#error").html('<div class="alert alert-danger">&nbsp; '+response+'</div>');
     				});
     			}
  			 }
  			});
    		return false;
  		}
      /*onchage file input file put visible button*/
      $('#fileImagen').on("change", function(){ 
        if($("#fileImagen").get(0).files.length ===0){
           $("#btn_enviar_img").css("visibility","hidden");
        }else{
          $("#btn_enviar_img").css("visibility","visible");
        }
      });
      /*Validar form imagen */
      $("#img-form").validate({
          rules:{
          fileImage:{
              required:true,
            }
        },
          messages:{ 
            fileImage:{
              required: "<?=lang('admin_principal_error_imagen');?>"
            }
        },
        submitHandler: updateImageForm 
        });  

      /*Update Imagen post*/
      function updateImageForm(){
        var data = new FormData(document.getElementById("img-form"));

        $.ajax({
          type : 'POST',
          url  : '<?php echo base_url("ValidationBlog/update_imagen/");?>',
          data : data,
          contentType: false,
          processData: false,
          beforeSend: function(){ 
            $("#error-img").fadeOut();
            $("#success-img").fadeOut();
          },
          success :  function(response){      
          if(!response.includes("Error")){
            $("#imagenBlog").attr("src","<?php echo base_url();?>"+response);
            $("#imagenChanged").attr("src", "<?php echo base_url();?>"+response);
            $("#success-img").fadeIn(1000, function(){      
            $("#success-img").html('<div class="alert alert-success">&nbsp; La imagen se actualizo correctamente!</div>');
          });
          }else{
                $("#error-img").fadeIn(1000, function(){      
              $("#error-img").html('<div class="alert alert-danger">&nbsp; '+response+'</div>');
            });
          }
         }
        });
      }
	});
</script>