<script type="text/javascript">
		$("#searchUser-form").validate({
      		rules:{
		    	  email:{
		    		  required:true,
              email: true
		    	   }
    		  },
       		messages:{	
            	email: {
            		required : "Email es requerido para la busqueda",
                email: "No parece un email correcto"
            	}
       		},
    		submitHandler: submitSearch 
       		});  
    	/* validation */
    	/* search submit */
    	function submitSearch(){  
   			var data =  new $("#searchUser-form").serialize();
   			$.ajax({  
   				type : 'POST',
   				url  : '<?php echo base_url("user/search/");?>',
   				data : data,

   				beforeSend: function(){ 
    				$("#error-editar").fadeOut();
    				$("#successeditar").fadeOut();
   				},
   				success :  function(response){      
          var usuario = JSON.parse(response);
     			if(usuario != null && usuario.usuario_id > 0){
            $("#txtApodo_edit").val(usuario.apodo);
            $("#txtNombre_edit").val(usuario.nombre);
            $("#txtEmail_edit").val(usuario.email);
            $("#txtTitlePerfil_edit").val(usuario.rol_id);

     			}else{
      					$("#error-editar").fadeIn(1000, function(){      
    					$("#error-editar").html('<div class="alert alert-danger">&nbsp; Usuario no encontrado! </div>');
     				});
     			}
  			 }
  			});
  		}
</script>