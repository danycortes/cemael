<script type="text/javascript" src="<?php echo base_url("assets/admin/js/plugins/datatables/jquery.dataTables.js")?>">
</script>
<script type="text/javascript" src="<?php echo base_url("assets/admin/js/plugins/datatables/dataTables.bootstrap.js")?>">
</script>
<script type="text/javascript">
$(document).ready(function() {
    var table = $('#misBlogs').DataTable({
    	"iDisplayLength": 5
    	,"language":{
    		"decimal":        "", 
    		"emptyTable":     "No hay datos para mostrar",
    		"info":           "Mostrando desde _START_ hasta _END_ de  _TOTAL_ blogs.",
			"infoEmpty":      "Mostrando 0 de 0 de 0 blogs",
			"infoFiltered":   "(Flitrado de _MAX_ total de blogs)",
			"infoPostFix":    "",
			"thousands":      ",",
			"lengthMenu":     "Mostrando _MENU_ Blogs",
			"loadingRecords": "Cargando...",
			"processing":     "Procesando...",
			"search":         "Buscar : ",
			"zeroRecords":    "No se encontraron blogs",
			"paginate": {
				    "first":      "Primero",
				    "last":       "Ultimo",
				    "next":       "Siguiente",
				    "previous":   "Previo"
				}
		}
    });

    $('#misBlogs tbody').on('dblclick', 'tr', function () {
        var data = table.row( this ).data();
        //alert( 'You clicked on '+data[0]+'\'s row' );
        if(data[3] != '<span class="label label-success">Aprobado</span>'){
        	window.location.href="<?php echo base_url();?>"+"dashboard/editar/"+data[0];
    	}else{
    		alert("Un blog aprobado no puede ser actualizado.");
    	}
    } );
} );
</script>
