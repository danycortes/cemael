<script type="text/javascript">
    //Create user area
    $("#newUser-form").validate({//Validacion de formulario crear usuario
          rules:{
            txtEmail:{
              required:true,
              email: true
             },
             txtApodo:{
              required:true
             },
             txtNombre:{
              required:true
             },
             txtPerfil:{
               required:true 
             }
          },
          messages:{  
              email: {
                required : "Email es requerido para crear un usuario",
                email: "No parece un email correcto"
              },
              txtPerfil:{
                required : "Debe seleccionar un perfil."
              }
              ,
              txtNombre:{
                required : "Debe escribir un nombre de usuario."
              }
              ,
              txtApodo:{
                required : "Debe escribir un nickname para el usuario"
              }
          },
        submitHandler: submitCreate
          });  
    //submit form 
    function submitCreate(){  
        var data =  new $("#newUser-form").serialize();
        $.ajax({  
          type : 'POST',
          url  : '<?php echo base_url("user/create/");?>',
          data : data,

          beforeSend: function(){ 
            $("#error-crear").fadeOut();
            $("#success-crear").fadeOut();
          },
          success :  function(response){      
          var response = JSON.parse(response);
          if(response.status == "success"){
              $("#success-crear").fadeIn(1000, function(){      
              $("#success-crear").html('<div class="alert alert-success">&nbsp; '+response.mensaje +'</div>');
              });
          }else{
              $("#error-crear").fadeIn(1000, function(){      
              $("#error-crear").html('<div class="alert alert-danger">&nbsp;'+response.mensaje+'</div>');
            });
          }
         }
        });
      }
    //End Create user Area
    //Edit user area
    $("#editUser-form").validate({//Validacion de formulario crear usuario
          rules:{
            txtEmail_edit:{
              required:true,
              email: true
             },
             txtApodo_edit:{
              required:true
             },
             txtNombre_edit:{
              required:true
             },
             txtPerfil_edit:{
               required:true 
             }
          },
          messages:{  
              txtEmail: {
                required : "Email es requerido para crear un usuario",
                email: "No parece un email correcto"
              },
              txtPerfil_edit:{
                required : "Debe seleccionar un perfil."
              }
              ,
              txtNombre_edit:{
                required : "Debe escribir un nombre de usuario."
              }
              ,
              txtApodo_edit:{
                required : "Debe escribir un nickname para el usuario"
              }
          },
        submitHandler: submitEdit
          });  
    //submit form 
    function submitEdit(){  
        var data =  new $("#editUser-form").serialize();
        $.ajax({  
          type : 'POST',
          url  : '<?php echo base_url("user/edit/");?>',
          data : data,

          beforeSend: function(){ 
            $("#error-editar").fadeOut();
            $("#success-editar").fadeOut();
          },
          success :  function(response){      
          var response = JSON.parse(response);
          if(response.status == "success"){
              $("#success-editar").fadeIn(1000, function(){      
              $("#success-editar").html('<div class="alert alert-success">&nbsp; '+response.mensaje +'</div>');
              });
          }else{
              $("#error-editar").fadeIn(1000, function(){      
              $("#error-editar").html('<div class="alert alert-danger">&nbsp;'+response.mensaje+'</div>');
            });
          }
         }
        });
      }
    //End Edit area
    //validation for search an user
    $("#searchEdit-form").validate({
          rules:{
            email:{
              required:true,
              email: true
             }
          },
          messages:{  
              email: {
                required : "Email es requerido para la busqueda",
                email: "No parece un email correcto"
              }
          },
        submitHandler: submitSearch 
          });  
      /* validation */
      /* search submit */
      function submitSearch(){  
        var data =  new $("#searchEdit-form").serialize();
        $.ajax({  
          type : 'POST',
          url  : '<?php echo base_url("user/search/");?>',
          data : data,

          beforeSend: function(){ 
            $("#error-editar").fadeOut();
            $("#success-editar").fadeOut();
          },
          success :  function(response){      
          var usuario = JSON.parse(response);
          if(usuario != null && usuario.usuario_id > 0){
            $("#txtApodo_edit").val(usuario.apodo);
            $("#txtNombre_edit").val(usuario.nombre);
            $("#txtEmail_edit").val(usuario.email);
            $("#txtPerfil_edit").val(usuario.rol_id);
            $("#txtUsuario_id_edit").val(usuario.usuario_id);
          }else{
                $("#error-editar").fadeIn(1000, function(){      
              $("#error-editar").html('<div class="alert alert-danger">&nbsp; Usuario no encontrado! </div>');
            });
          }
         }
        });
      }
      //search Delete 
      //validation for search an user
    $("#searchDel-form").validate({
          rules:{
            email:{
              required:true,
              email: true
             }
          },
          messages:{  
              email: {
                required : "Email es requerido para la busqueda",
                email: "No parece un email correcto"
              }
          },
        submitHandler: submitSearchDel 
          });  
      /* validation */
      /* search submit */
      function submitSearchDel(){  
        var data =  new $("#searchDel-form").serialize();
        $.ajax({  
          type : 'POST',
          url  : '<?php echo base_url("user/search/");?>',
          data : data,

          beforeSend: function(){ 
            $("#error-eliminar").fadeOut();
            $("#success-eliminar").fadeOut();
          },
          success :  function(response){      
          var usuario = JSON.parse(response);
          if(usuario != null && usuario.usuario_id > 0){
            $("#txtNombre_del").val(usuario.nombre);
            $("#txtEmail_del").val(usuario.email);
            $("#txtUsuario_id_del").val(usuario.usuario_id);
          }else{
                $("#error-eliminar").fadeIn(1000, function(){      
              $("#error-eliminar").html('<div class="alert alert-danger">&nbsp; Usuario no encontrado! </div>');
            });
          }
         }
        });
      }
      //delete Area 
       //search Delete 
      //validation for search an user
    $("#userDel-form").validate({
          rules:{
            email:{
              required:true,
              email: true
             },
            nombre:{
              required:true,
            }

          },
          messages:{  
              email: {
                required : "Debe buscar antes de eliminar!",
                email: "No parece un email correcto"
              },
              nombre: {
                required : "Debe buscar antes de eliminar!"
              }
          },
        submitHandler: submitDel 
          });  
      /* validation */
      /* search submit */
      function submitDel(){  
        var data =  new $("#userDel-form").serialize();
        $.ajax({  
          type : 'POST',
          url  : '<?php echo base_url("user/delete/");?>',
          data : data,
          beforeSend: function(){ 
            $("#error-eliminar").fadeOut();
            $("#success-eliminar").fadeOut();
          },
          success :  function(response){      
            var response = JSON.parse(response);
            if(response.status == "success"){
                $("#success-eliminar").fadeIn(1000, function(){      
                $("#success-eliminar").html('<div class="alert alert-success">&nbsp; '+response.mensaje +'</div>');
                });
            }else{
                $("#error-eliminar").fadeIn(1000, function(){      
                $("#error-eliminar").html('<div class="alert alert-danger">&nbsp;'+response.mensaje+'</div>');
              });
            }
          }
        });
      }

</script>