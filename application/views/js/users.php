<script src="<?php echo base_url('assets/admin/js/tinymce/tinymce.min.js');?>" type="text/javascript">
</script>
<script type="text/javascript">
	$('document').ready(function(){ 
		tinymce.init({
			selector:'#txtMensaje'
		});

		$("#escribir-form").validate({
      		rules:{
			   	txtTitle: {
			   		required: true,
			   		},
			   	txtIntro: {
			   		required: true,
			   		},
			   	fileImage: {
                	required: true, 
                	extension: "png|jpeg|jpg",
            	},
		    	txtMensaje:{
		    			required:true,
		    		},
    		},
       		messages:
    		{	
            	txtTitle:{
                      required: "<?=lang('admin_principal_error_title');?>"
                    },
                txtIntro:{
                	required: "<?=lang('admin_principal_error_intro');?>"
                },
            	fileImage:{
                      required: "<?=lang('admin_principal_error_imagen');?>",
                      extension:"mal"
                    },
            	txtMensaje: {
            		required : "<?=lang('admin_principal_error_mensaje');?>"
            	}
       		},
    		submitHandler: submitForm 
       		});  
    	/* validation */
    	/* login submit */
    	function submitForm(){  
    		tinymce.triggerSave();
   			var data =  new FormData($('form')[0]); // $("#escribir-form").serialize();
   			// save TinyMCE instances before serialize

   			$.ajax({  
   				type : 'POST',
   				url  : '<?php echo base_url("ValidationBlog/save_post/");?>',
   				data : data,
   				contentType: false,
   				processData: false,
   				beforeSend: function(){ 
    				$("#error").fadeOut();
    				$("#success").fadeOut();
   				},
   				success :  function(response){      
     			if(response == "success"){
     				$("#success").fadeIn(1000, function(){      
    				$("#success").html('<div class="alert alert-success">&nbsp; '+response+' !</div>');
    			});
     			}else{
      					$("#error").fadeIn(1000, function(){      
    					$("#error").html('<div class="alert alert-danger">&nbsp; '+response+'</div>');
     				});
     			}
  			 }
  			});
    		return false;
  		}

	});
</script>