<script type="text/javascript" src="<?php echo base_url("assets/admin/js/plugins/datatables/jquery.dataTables.js")?>">
</script>
<script type="text/javascript" src="<?php echo base_url("assets/admin/js/plugins/datatables/dataTables.bootstrap.js")?>">
</script>
<script type="text/javascript">
$(document).ready(function() {
    var table = $('#allposts').DataTable({
    	"iDisplayLength": 5
    	,"language":{
    		"decimal":        "", 
    		"emptyTable":     "No hay datos para mostrar",
    		"info":           "Mostrando desde _START_ hasta _END_ de  _TOTAL_ blogs.",
			"infoEmpty":      "Mostrando 0 de 0 de 0 blogs",
			"infoFiltered":   "(Flitrado de _MAX_ total de blogs)",
			"infoPostFix":    "",
			"thousands":      ",",
			"lengthMenu":     "Mostrando _MENU_ Blogs",
			"loadingRecords": "Cargando...",
			"processing":     "Procesando...",
			"search":         "Buscar : ",
			"zeroRecords":    "No se encontraron blogs",
			"paginate": {
				    "first":      "Primero",
				    "last":       "Ultimo",
				    "next":       "Siguiente",
				    "previous":   "Previo"
				}
		}
    });
    $('#allposts tbody').on('click', 'td', function () {
        var data = table.cell( this ).data();
        var datarow = table.row(this).data();
        
        switch(data){
        	case '<span class="label label-warning">No aprobado</span>':
        		//window.location.href="<?php echo base_url();?>"+"ValidationBlog/cambio/"+datarow[0]+ "/"+1;
        		var values = {"post_id":datarow[0], "aprobar": 1};
        		$.ajax({
        			url:"<?php echo base_url();?>"+"ValidationBlog/aprobar/",
        			type:"POST",
        			data:values,
        			success:function(response){
        				alert(response);
        				location.reload();
        			},
        			error:function(response){
        				alert(response);
        			}
        		});
        	break;
        	case '<span class="label label-primary">actualizar</span>':
        		window.location.href="<?php echo base_url();?>"+"dashboard/editar/"+datarow[0];
        	break;
        	case '<span class="label label-danger">eliminar</span>':
        		$.ajax({
        			url:"<?php echo base_url();?>/ValidationBlog/delete_post/"+datarow[0],
        			success:function(response){
        				alert(response);
        				location.reload();
        			},
        			error:function(response){
        				alert(response);
        			}
        		});
        	//window.location.href="<?php echo base_url();?>"+"ValidationBlog/delete_post/"+datarow[0];
        	break;
          	case '<span class="label label-success">Aprobado</span>':
        		var values = {"post_id":datarow[0], "aprobar": 0};
        		$.ajax({
        			url:"<?php echo base_url();?>"+"ValidationBlog/aprobar/",
        			type:"POST",
        			data:values,
        			success:function(response){
        				alert(response);
        				location.reload();
        			},
        			error:function(response){
        				alert(response);
        			}
        		});
        	break;
        }
    } );
} );
</script>
