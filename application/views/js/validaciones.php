<script type="text/javascript">
  $('document').ready(function(){ 
      /* validation */
      $("#login-form").validate({
          rules:{
          password: {
          required: true,
          },
        email: {
              required: true,
              email: true
              },
        },
          messages:
        { 
              password:{
                      required: "<?=lang('admin_login_password_error');?>"
                    },
              usuario: {
                required : "<?=lang('admin_login_email_error');?>",
                email:"<?=lang('admin_login_email_novalid');?>introduce un email valido"
              }
          },
        submitHandler: submitForm 
          });  
      /* validation */
      /* login submit */
      function submitForm(){  
        var data = new $("#login-form").serialize();
        $.ajax({  
          type : 'POST',
          url  : '<?php echo base_url("/administrador");?>',
          data : data,
          beforeSend: function(){ 
            $("#error").fadeOut();
            $("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
          },
          success :  function(response){      
          if(response=="success"){
              window.location.href = "<?php echo base_url("/dashboard");?>";
          }else{
                $("#error").fadeIn(1000, function(){      
              $("#error").html('<div class="alert alert-danger">&nbsp; '+response+' !</div>');
                  $("#btn-signin").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
                    });
            }
          }
         });
        return false;
      }
    });
</script>