<?php
$lang['title'] = 'Traveller';
$lang['copy'] = '&copy; 2017 Traveller.mx';
$lang['powered'] = 'Powered by <a href="http://www.cwa.mx/">CWA.MX';

#sliders secctions
$lang['slider_button'] = "Aseg&uacute;rate";
$lang['slider_title_1'] = "BIENVENIDO A ";
$lang['slider_text_1'] = "NO SEAS UN TURISTA SE UN TRAVELLER";
$lang['slider_title_2'] = "WELCOME TO ";
$lang['slider_text_2'] = "Don't be a tourist be a Traveller";
$lang['slider_title_3'] = "BIENVENUE AU ";
$lang['slider_text_3'] = "NE SOYEZ PAS UN TOURISTE ÊTRE UN TRAVELLER";
$lang['slider_title_4'] = "WE ARE ";
$lang['slider_text_4'] = "Don't be a tourist be a Traveller";
$lang['slider_title_5'] = "WILLKOMMEN BEI ";
$lang['slider_text_5'] = "SEI KEIN TOURIST SEI EIN TRAVELLER";
# Home Lista de textos para la pagina principal  
$lang['menu_home'] = "inicio";
$lang['menu_planes']= "planes";
$lang['menu_precios']= "tarifas	";
$lang['menu_nosotros'] =  "nosotros";
$lang['menu_blog'] = "blog";
$lang['menu_contacto'] = "contacto";
$lang['menu_comprar'] = "comprar";
#Seccion Planes
$lang['planes_title']="Planes";
$lang['planes_text'] = "Traveller te acompa&ntilde;ar&aacute; en todos tus viajes para disfrutes sin preocupaciones, solo elige el seguro que necesites seg&uacute;n tu destino.";
$lang['planes_caption_1']="Disfruta con tranquilidad todos los estados de la república mexicana con nuestra cobertura nacional.";
$lang['planes_caption_2']="Confía en nuestro respaldo para tus viajes programados a todas las ciudades de Estados Unidos y Canadá.";
$lang['planes_caption_3']="Viaja a Europa siempre seguro de que nuestra cobertura te dará la protección que requieres en el viejo continente.";
$lang['planes_caption_4']="Descubre el mundo y sus maravillas protegido siempre con Traveller.";
#Seccion Tarifas 
$lang['tarifas_title']="Tarifas";
$lang['tarifas_text']="Estas son las tarifas y coberturas a las que tendr&aacute;s alcance para tu tranquilidad mientras disfrutas de tu viaje siempre protegido por Traveller.";
#Seccion Nosotros 
$lang['nosotros_title'] = "TRAVELLER";
$lang['nosotros_text'] = "Es una empresa dedicada a brindar asistencias a viajeros nacionales con presencia mundial a trav&eacute;s de la red Allianz Global Assistance para hacer de sus experiencias momentos únicos con sus coberturas Nacional, Estados Unidos y Canad&aacute;, Europa e Internacional.<br/>Nuestra cobertura de hasta 95 d&iacute;as consecutivos, sin deducible, ni coaseguro hacen de este servicio una forma f&aacute;cil y r&aacute;pida de viajar seguro a cualquier destino en el mundo.<br/>No seas un turista, se un TRAVELLER";
$lang['nosotros_skill_1'] = "Eficiencia";
$lang['nosotros_skill_1_%'] = "92";
$lang['nosotros_skill_2'] = "Confianza";
$lang['nosotros_skill_2_%'] = "90";
$lang['nosotros_skill_3'] = "Cobertura Mundial";
$lang['nosotros_skill_3_%'] = "99";
$lang['nosotros_skill_4'] = "Usuarios Satisfechos";
$lang['nosotros_skill_4_%'] = "90";
//fun facts
$lang['nosotros_fact_1'] = "34";
$lang['nosotros_fact_1_detalle'] = "Países en Operación";
$lang['nosotros_fact_2'] = "400,000";
$lang['nosotros_fact_2_detalle'] = "Proveedores en el Mundo";
$lang['nosotros_fact_3'] = "55";
$lang['nosotros_fact_3_detalle'] = "Programas locales, Regionales y Mundiales";
$lang['nosotros_fact_4'] = "24/7";
$lang['nosotros_fact_4_detalle'] = "Atención los 365 días del año";
//Tips Traveller
$lang['tip_uno']="Otra opción es dejar una copia de tu pasaporte e itinerario con tu familia o amigos para que puedan mandártelo por mail en caso de emergencia.";
$lang['tip_dos']="Te recomendamos llevar una muda de ropa en tu equipaje de mano, en caso de que tu equipaje se pierda.";
$lang['tip_tres']="Pon tu celular en “Modo Avión” para ahorrar batería y cargarlo más rápido.";
//Contactanos Messages 
$lang['contacto_title'] = "Cont&aacute;tanos";
$lang['contacto_subtitle'] = "Para nosotros es un placer atenderte. Por eso, te ofrecemos diferentes medios para apoyarte en tu compra.";
$lang['contacto_horario'] = "Horario de Atención de 9AM a 6PM";
$lang['contacto_email_value'] = "contacto@traveller.mx";
$lang['contacto_tel_value'] = "1234567890";
$lang['contacto_web_value'] = "www.traveller.mx";
$lang['contacto_form_nombre'] = "Nombre";
$lang['contacto_form_email'] = "Email";
$lang['contacto_form_asunto'] = "Asunto";
$lang['contacto_form_mensaje'] = "Mensaje";
$lang['contacto_form_enviar'] = "Enviar";
$lang['lbl_phone']= "Tel&eacute;fono";
$lang['lbl_website']= "Pagina web";
$lang['lbl_mail']= "E-mail";
#listado de productos
//Nacional
$lang['tarifa_nacional'] = "291.75";
$lang['tarifa_inter'] = "372.57";
$lang['tarifa_europa'] = "649.33";
$lang['tarifa_usa'] = "755.09";
//Nacional
$lang['cobertura_1_1']= "Asistencia Medica $25,000 MXN";
$lang['cobertura_1_2']= "Vuelo Cancelado $10,000 MXN";
$lang['cobertura_1_3']= "Perdida de Equipaje $5,000 MXN";
$lang['cobertura_1_4']= "Concierge Incluido";
//EUA Canada
$lang['cobertura_2_1']= "Asistencia Medica $20,000 USD";
$lang['cobertura_2_2']= "Vuelo Cancelado $2,000 USD";
$lang['cobertura_2_3']= "Perdida de Equipaje $500 USD";
$lang['cobertura_2_4']= "Concierge Incluido";
//Europa
$lang['cobertura_3_1']= "Cancelacion de Vuelo $2,000 USD";
$lang['cobertura_3_2']= "Vuelo Cancelado $2,000 USD";
$lang['cobertura_3_3']= "Perdida de Equipaje $500 USD";
$lang['cobertura_3_4']= "Concierge Incluido";
//Internacional
$lang['cobertura_4_1']= "Asistencia Medica $30,000 EUR";
$lang['cobertura_4_2']= "Vuelo Cancelado $1,200 EUR";
$lang['cobertura_4_3']= "Perdida de Equipaje $500 EUR";
$lang['cobertura_4_4']= "Concierge Incluido";
#Section Blog
$lang['blog_title']= "Traveller Blog";
$lang['blog_subtitle'] = "Enterate de destinos, tips y mas ..." ;
$lang['blog_mas'] = "Entrar";
$lang['blog_todos']= "Ver todos";

#footer 
$lang['footer_aviso'] = 'Esta asistencia es operada por Allianz Global Asistance para traveller.mx. Allianz Global Asistance es responsable frente a los beneficiarios del cabal cumplimiento de los Términos y Condiciones de la asistencia, razón por la cual, traveller.mx no asume ninguna responsabilidad sobre las mismas. Te invitamos a leer el aviso de privacidad de Allianz Global Asistance.

AWP México, S. A. de C. V. (es el administrador de este plan, bajo la marca de Allianz Global Assistance), con domicilio ubicado en Insurgentes Sur 1602 Piso 3-302 Col. Crédito Constructor CP 03940 México DF, le informa que es responsable del tratamiento de los datos personales de nuestros usuarios y usuarios potenciales, mismos que son tratados de forma estrictamente privada y confidencial. Los datos personales que recabaremos de usted, los utilizaremos para las siguientes finalidades: brindar asistencia sobre los servicios que ofrecemos, así como atender y operar los mismo; así como para fines distintos o secundarios limitados a evaluación de la calidad de los servicios brindados, publicidad de nuestros servicios, realización de estudios de mercado y fines ';
########################Pagina blog tags 
$lang['blog_title_page'] = "Historias";
$lang['blog_subtitle_page'] = "traveller";
$lang['blog_siguenos'] = "s&iacute;guenos";
########################################## Admin labels
$lang['admin_login_email'] = "Email";
$lang['admin_login_password'] = "Contrase&ntilde;a";
$lang['admin_login_rememberme'] = "recuerda me";
$lang['admin_login_olvidopassword'] = "Recuperar contrase&ntilde;a";
$lang['admin_login_log_in'] = "entrar";
$lang['admin_login_log_out'] = "salir";
$lang['admin_login_error'] = "El usuario o contrase&ntilde;a son incorrectos";
$lang['admin_login_email_error'] = "introduce tu email";
$lang['admin_login_email_novalid'] = "email invalido";
$lang['admin_login_password_error'] = "introduce tu password";
#Menu
$lang['admin_menu_hello']="Hola";
$lang['admin_menu_cuenta'] = "Cuenta";
$lang['admin_menu_perfil'] = "perfil";
$lang['admin_menu_mensaje'] = "mensjes";
$lang['admin_menu_config'] = "Configuracion";
$lang['admin_menu_salir'] = "Salir";
#todo
$lang['admin_todo_title'] = "All blogs";
#Principal 
$lang['admin_principal_title'] = "Mis blogs";
$lang['admin_principal_id'] = "Id";
$lang['admin_principal_name'] = "Nombre";
$lang['admin_principal_date'] = "Fecha";
$lang['admin_principal_intro'] = "Intro";
$lang['admin_principal_ver'] = "Ver";
$lang['admin_principal_status'] = "Estatus";
$lang['admin_principal_actualizar'] = "Actulizar";
$lang['admin_principal_eliminar'] = "Eliminar";
$lang['admin_principal_tabla'] = "Mis blogs";
$lang['admin_principal_aprobado'] = "Aprobado";
$lang['admin_principal_noaprobado'] = "No aprobado";
$lang['admin_principal_txt_title'] = "Titulo";
$lang['admin_principal_txt_title_ayuda'] = "Inserte aqui el titulo principal del post.";
$lang['admin_principal_txt_intro'] = "Introduccion";
$lang['admin_principal_txt_intro_ayuda'] = "Introduce una breve descripcion del post.";
$lang['admin_principal_txt_mensaje'] = "Mensaje";
$lang['admin_principal_img_txt'] = "Imagen principal";
$lang['admin_principal_img_ayuda'] = "Selecciona una imagen de tus documentos.";
$lang['admin_principal_btn_post'] = "Crear post";
	#Principal #Errores
$lang['admin_principal_error_title'] = "El titulo es requerido";
$lang['admin_principal_error_intro'] = "El intro es requerido";
$lang['admin_principal_error_imagen'] = "Necesitas revisar la imagen";
$lang['admin_principal_error_mensaje'] = "Necesitas completar el mensaje";
#Escribir
$lang['admin_escribir_title'] = "Escribir blogs";
#usuarios
$lang['admin_usuario_title'] = "Usuarios";
$lang['admin_usuario_id'] = "Id";
$lang['admin_usuario_nombre'] = "Nombre";
$lang['admin_usuario_apodo'] = "Apodo";
$lang['admin_usuario_tipo'] = "Tipo Usuario";
$lang['admin_usuario_email'] = "Email";
$lang['admin_usuario_txt_perfil'] = "Tipo usuario";
$lang['admin_usuario_txt_nombre'] = "Nombre de usuario";
$lang['admin_usuario_txt_email'] = "Email";
$lang['admin_usuario_txt_apodo'] = "Nickname";
$lang['admin_usuario_btn_crear'] = "Crear";
$lang['admin_usuario_btn_editar'] = "Guardar";
$lang['admin_usuario_btn_newpass'] = "Restaurar password";
$lang['admin_usuario_btn_eliminar'] = "Eliminar usuario";
$lang['admin_usuario_buscar'] = "Buscar por email ...";
#partners
$lang['admin_partners_title'] = "Partners";
$lang['admin_partners_rfc'] = "RFC";
$lang['admin_partners_razon'] = "RAZON SOCIAL";
$lang['admin_partners_email'] = "EMAIL";
$lang['admin_partners_responsable'] = "RESPONSABLE";
#Sales
$lang['admin_sales_title'] = "Venta";


#perfil
$lang['admin_perfil_title'] = "Perfil";
$lang['admin_perfil_txt_perfil'] = "Tipo usuario";
$lang['admin_perfil_txt_nombre'] = "Nombre de usuario";
$lang['admin_perfil_txt_email'] = "Email Registrado";
$lang['admin_perfil_txt_apodo'] = "Nickname";
#Editar
$lang['admin_actualizar_title'] = "Actualizar blogs";
$lang['admin_editar_txt_title'] = "Cambiar titulo";
$lang['admin_editar_txt_title_ayuda'] = "Solo borra el texto actual e introduce el nuevo titulo";
$lang['admin_editar_txt_intro'] = "Cambia el intro";
$lang['admin_editar_txt_intro_ayuda'] = "Solo borra el texto actual e intrudce el nuevo intro";
$lang['admin_editar_img_txt'] = "Cambiar imagen";
$lang['admin_editar_img_ayuda'] = "Selecciona una imagen de tu computadora par actualizar la imagen del post";
$lang['admin_editar_txt_mensaje'] = "Puedes modificar el contenido de tu post";
$lang['admin_editar_btn_post'] = "Guardar Cambios";


#Genericos

$lang['id'] = "Id";
$lang['eliminar'] = "Eliminar";
$lang['nuevo'] = "Nuevo";
$lang['editar'] = "Editar";
$lang['ver'] = "Ver";
$lang['status'] = "Estatus";
$lang['crear'] = "Crear";


