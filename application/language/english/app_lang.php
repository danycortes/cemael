<?php

$lang['title'] = 'Traveller';
#sliders secctions
$lang['slider_button'] = "Aseg&uacute;rate";
$lang['slider_title_1'] = "BIENVENIDO A ";
$lang['slider_text_1'] = "NO SEAS UN TURISTA SE UN TRAVELLER";
$lang['slider_title_2'] = "WELCOME TO ";
$lang['slider_text_2'] = "Don't be a tourist be a Traveller";
$lang['slider_title_3'] = "BIENVENUE AU ";
$lang['slider_text_3'] = "NE SOYEZ PAS UN TOURISTE ÊTRE UN TRAVELLER";
$lang['slider_title_4'] = "WE ARE ";
$lang['slider_text_4'] = "Don't be a tourist be a Traveller";
$lang['slider_title_5'] = "WILLKOMMEN BEI ";
$lang['slider_text_5'] = "SEI KEIN TOURIST SEI EIN TRAVELLER";
# Home Lista de textos para la pagina principal  
$lang['menu_home'] = "inicio";
$lang['menu_planes']= "planes";
$lang['menu_precios']= "tarifas	";
$lang['menu_nosotros'] =  "nosotros";
$lang['menu_blog'] = "blog";
$lang['menu_contacto'] = "contacto";
#Seccion Planes
$lang['planes_title']="Planes";
$lang['planes_text'] = "Traveller te acompa&ntilde;ar&aacute; en todos tus viajes para disfrutes sin preocupaciones, solo elige el seguro que necesites seg&uacute;n tu destino.";
$lang['planes_caption_1']="Disfruta con tranquilidad todos los estados de la república mexicana con nuestra cobertura nacional.";
$lang['planes_caption_2']="Confía en nuestro respaldo para tus viajes programados a todas las ciudades de Estados Unidos y Canadá.";
$lang['planes_caption_3']="Viaja a Europa siempre seguro de que nuestra cobertura te dará la protección que requieres en el viejo continente.";
$lang['planes_caption_4']="Descubre el mundo y sus maravillas protegido siempre con Traveller.";
#Seccion Tarifas 
$lang['tarifas_title']="Tarifas";
$lang['tarifas_text']="Estas son las tarifas y coberturas a las que tendr&aacute;s alcance para tu tranquilidad mientras disfrutas de tu viaje siempre protegido por Traveller.";
#Seccion Nosotros 
$lang['nosotros_title'] = "TRAVELLER";
$lang['nosotros_text'] = "Es una empresa dedicada a brindar asistencias a viajeros nacionales con presencia mundial a trav&eacute;s de la red Allianz Global Assistance para hacer de sus experiencias momentos únicos con sus coberturas Nacional, Estados Unidos y Canad&aacute;, Europa e Internacional.<br/>Nuestra cobertura de hasta 95 d&iacute;as consecutivos, sin deducible, ni coaseguro hacen de este servicio una forma f&aacute;cil y r&aacute;pida de viajar seguro a cualquier destino en el mundo.<br/>No seas un turista, se un TRAVELLER";
$lang['nosotros_skill_1'] = "Eficiencia";
$lang['nosotros_skill_1_%'] = "92";
$lang['nosotros_skill_2'] = "Confianza";
$lang['nosotros_skill_2_%'] = "90";
$lang['nosotros_skill_3'] = "Cobertura Mundial";
$lang['nosotros_skill_3_%'] = "99";
$lang['nosotros_skill_4'] = "Usuarios Satisfechos";
$lang['nosotros_skill_4_%'] = "90";
//fun facts
$lang['nosotros_fact_1'] = "34";
$lang['nosotros_fact_1_detalle'] = "Países en Operación";
$lang['nosotros_fact_2'] = "400,000";
$lang['nosotros_fact_2_detalle'] = "Proveedores en el Mundo";
$lang['nosotros_fact_3'] = "55";
$lang['nosotros_fact_3_detalle'] = "Programas locales, Regionales y Mundiales";
$lang['nosotros_fact_4'] = "24/7";
$lang['nosotros_fact_4_detalle'] = "Atención los 365 días del año";

#listado de productos
//Nacional
$lang['tarifa_nacional'] = "291.75";
$lang['tarifa_inter'] = "372.57";
$lang['tarifa_europa'] = "649.33";
$lang['tarifa_usa'] = "755.09";
//Nacional
$lang['cobertura_1_1']= "Asistencia Medica $25,000 MXN";
$lang['cobertura_1_2']= "Vuelo Cancelado $10,000 MXN";
$lang['cobertura_1_3']= "Perdida de Equipaje $5,000 MXN";
$lang['cobertura_1_4']= "Concierge Incluido";
//EUA Canada
$lang['cobertura_2_1']= "Asistencia Medica $20,000 USD";
$lang['cobertura_2_2']= "Vuelo Cancelado $2,000 USD";
$lang['cobertura_2_3']= "Perdida de Equipaje $500 USD";
$lang['cobertura_2_4']= "Concierge Incluido";
//Europa
$lang['cobertura_3_1']= "Cancelacion de Vuelo $2,000 USD";
$lang['cobertura_3_2']= "Vuelo Cancelado $2,000 USD";
$lang['cobertura_3_3']= "Perdida de Equipaje $500 USD";
$lang['cobertura_3_4']= "Concierge Incluido";
//Internacional
$lang['cobertura_4_1']= "Asistencia Medica $30,000 EUR";
$lang['cobertura_4_2']= "Vuelo Cancelado $1,200 EUR";
$lang['cobertura_4_3']= "Perdida de Equipaje $500 EUR";
$lang['cobertura_4_4']= "Concierge Incluido";
# Seccion contacto
$lang['lbl_phone']= "Tel&eacute;fono";
$lang['lbl_website']= "Pagina web";
$lang['lbl_mail']= "e-mail";

#footer 
$lang['footer_aviso'] = 'Esta asistencia es operada por <a href="https://www.allianz-assistance.com.mx">Allianz Global Asistance</a> para traveller.mx. Allianz Global Asistance es responsable frente a los beneficiarios del cabal cumplimiento de los T&eacute;rminos y Condiciones de la asistencia, raz&oacute;n por la cual, traveller.mx no asume ninguna responsabilidad sobre las mismas. Te invitamos a leer el aviso de privacidad de Allianz Global Asistance.';




