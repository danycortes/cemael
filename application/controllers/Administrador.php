<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //loading language
        $this->tpl = array();
        $this->load->helper('language','url');
        $this->load->library('Language');
        $this->language->set();
        $this->load->model('usuarios_model');
        $this->load->library('Acl');
        //End Language
    }
    /**
    *@author Heriberto Martinez
    *Genera el login del usuario
    **/
    public function index(){
        if($this->acl->logged_in()){
            redirect('dashboard','refresh');
        }else{
            if($this->input->server('REQUEST_METHOD') == 'POST'){
                $usuario = $this->input->post("usuario");
                $password = $this->input->post("password");
                $result = $this->acl->login($usuario,$password);
                if($this->acl->logged_in()){
                    $respuesta_usuario['status'] = "success";
                    $respuesta_usuario['mensaje'] = "Login correcto";
                    echo json_encode($respuesta_usuario);
                }else{
                    $respuesta_usuario['status'] = "error";
                    $respuesta_usuario['mensaje'] = $this->lang->line('admin_login_error');
                    echo json_encode($respuesta_usuario);
                }
            }else{
                $this->load->view("admin/header");
                $this->load->view("admin/forms/login_form");
                $this->load->view("js/user_login.php");
            }
            
        }
    }
}