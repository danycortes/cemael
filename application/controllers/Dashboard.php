<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		//loading language
		$this->tpl = array();
		$this->load->helper('language');
        $this->load->library('Language');
        $this->load->library('Tools');
        $this->load->library('Acl');
        $this->load->model('post_model');
        $this->load->model('usuarios_model');
        $this->load->model('partners_model');
        $this->load->model('catalogos_model');
        $this->language->set();
        if (!$this->acl->logged_in())
            redirect('/administrador');
        $this->tpl['usuario'] = $this->session->usuario;
        //End Language
	}

    /**
    *@author Heriberto Martinez
    *redirecciona a la pagina principal para los usuarios
    **/
	public function index()	{
            $this->mis_blogs();
	}
    public function escribir(){
            $this->load->view("admin/header",$this->tools->setPage('escribir_blogs'));
            $this->load->view("admin/menu",$this->tpl);
            $this->tpl['js'] = $this->load->view("js/blog_create","",TRUE);
            $this->tpl['current_view'] = $this->load->view('admin/views/blog_escribir',$this->tpl,TRUE);
            $this->load->view("admin/content",$this->tpl);
            $this->load->view("admin/footer");
    }
    /**
    *@author Heriberto Martinez
    * Permite la edicion de un post
    **/
    public function editar($post_id){
            $data["post"] = $this->post_model->get_post($post_id);
            #var_dump($data);exit();
            $this->load->view("admin/header",$this->tools->setPage('editar_blogs'));
            $this->tpl['js'] = $this->load->view("js/post_editar","",TRUE);
            $this->tpl['current_view'] = $this->load->view('admin/views/blog_editar',$data,TRUE);
            $this->load->view("admin/menu",$this->tpl);
            $this->load->view("admin/content");
            $this->load->view("admin/footer");
    }

    /**
    *@author Heriberto Martinez
    *Eliminara el post seleccionado por el usuarios 
    **/
    public function eliminar($post_id){
                echo "eliminado";
    }
    /**
    *@author Heriberto Martinez
    *muestra solo los blog de la persona que se logueo 
    **/
    public function mis_blogs(){
        $data['posts'] = $this->post_model->get_posts_user($this->session->usuario->usuario_id);
        $this->tpl['current_view'] = $this->load->view('admin/views/blog_usuario',$data,TRUE);
        $this->tpl["js"] = $this->load->view("js/blog_user","",TRUE);
        $this->load->view("admin/header",$this->tools->setPage('mis_blogs'));
        $this->load->view("admin/menu",$this->tpl);
        $this->load->view("admin/content");
        $this->load->view("admin/footer");
    }
    /**
    *@author Heriberto Martinez
    *Muestra todos los blog de los usuarios y permite actualizarlos eliminarlos y ddejar que se *puedan ver en la vista
    **/
    public function todos_blogs(){
        $this->tpl['posts'] = $this->post_model->get_posts_admin($this->session->usuario->usuario_id);
        $this->tpl['current_view'] = $this->load->view('admin/views/blog_all',$this->tpl,TRUE);
        $this->tpl["js"] = $this->load->view("js/blog_all","",TRUE);
            $this->load->view("admin/header",$this->tools->setPage('todo_blogs'));
            $this->load->view("admin/menu");
            $this->load->view("admin/content",$this->tpl);
            $this->load->view("admin/footer");
    }
    /**
    *@author Heriberto Martinez
    * Muestra el perfil de la persona que esta logueada
    **/
    public function perfil(){
            $this->tpl['user'] = $this->usuarios_model->getUsuario($this->session->usuario->usuario_id);
            $this->tpl['current_view'] = $this->load->view('admin/views/blog_perfil',$this->tpl,TRUE);
            $this->load->view("admin/header",$this->tools->setPage('perfil_blogs'));
            $this->load->view("admin/menu");
            $this->load->view("admin/content",$this->tpl);
            $this->load->view("admin/footer");
    }
    /**
    *@author Heriberto Martinez
    *Muestra la lista de usuarios para poder editarlos
    **/
    public function usuarios(){
        $this->tpl['roles'] = $this->catalogos_model->getRoles();
        $this->tpl['cat_partners'] = $this->catalogos_model->getPartners();
        $this->tpl['usuarios'] = $this->usuarios_model->getUsuarios($this->session->usuario->usuario_id);
        $this->tpl['current_view'] = $this->load->view('admin/views/usuarios_blog',$this->tpl,TRUE);
        $this->tpl["js"] = $this->load->view("js/users_blog","",TRUE);   
        $this->load->view("admin/header",$this->tools->setPage('usuarios_blogs'));
        $this->load->view("admin/menu");
        $this->load->view("admin/content",$this->tpl);
        $this->load->view("admin/footer");
    }



    /**
    *@author Heriberto Martinez
    *Cierra la session y re direcciona a la vista del login 
    **/
    public function salir(){
        $this->acl->logout();
        redirect('dashboard','refresh');
    }
}