<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SendEmail extends CI_Controller {

	public function __construct(){
		parent::__construct();
		//loading language
		$this->tpl = array();
		$this->load->helper('language');
        $this->load->library('Language');
        $this->load->library('Tools');
        $this->load->library('Acl');
        $this->load->model('usuarios_model');
        $this->language->set();
        if (!$this->acl->logged_in())
            redirect('/administrador');
        $this->tpl['usuario'] = $this->session->usuario;
        //End Language
	}

    /**
    *@author Heriberto Martinez
    *redirecciona a la pagina principal para los usuarios
    **/
	public function index()	{
            
	}

    public function sendPassword(){

    }
  
}