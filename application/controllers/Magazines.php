﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Magazines extends CI_Controller {

    /**
    * Magazines page
    * @author Daniela Cortés
    **/
    public function index(){
        $this->load->view('magazines');
    }
}