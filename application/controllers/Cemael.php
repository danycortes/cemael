<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cemael extends CI_Controller {

	public function __construct(){
		parent::__construct();
		//loading language
		$this->load->helper('language');
        $this->load->library('Language');
        $this->language->set();
        //End Language
        $this->load->model('post_model');
	}

	/**
	*Pagina principal de traveller
	*@author Heriberto Martínez Ramírez
	**/
	public function index(){

		#mostrar post en la pagina principal de traveller
		$data['posts'] = $this->post_model->get_posts(3, 0);
		$this->load->view('home',$data);
	}

}
