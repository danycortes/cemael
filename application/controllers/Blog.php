<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct(){
		parent::__construct();
		//loading language
		$this->load->helper('language');
        $this->load->library('Language');
        $this->language->set();
        $this->load->model('post_model');
        $this->load->library('pagination');
        //End Language
	}
	public function index(){
		$this->posts();
	}

	public function posts($start=0)
	{
		$data['posts'] = $this->post_model->get_posts(3, $start);
        
        //pagination configuration
        $config['base_url'] = base_url().'blog/posts/';//url to set pagination
        $config['total_rows'] = $this->post_model->get_post_count();
        $config['per_page'] = 3; 
         //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config); 
        $data['pages'] = $this->pagination->create_links(); //Links of pages
     
		$this->load->view('blog/todos',$data);
	}
    /**
    *@author Heriberto martinez
    *El metodo id recibe un id de un blog 
    **/
	public function id($blod_id){
        $data['post'] =  $this->post_model->get_post($blod_id);
        $this->load->view('blog/post',$data);
	}

        /**
    *@author Heriberto martinez
    *Resuelve el slug del post solicitado
    **/
    public function get_slug($slug){
        $data['post'] =  $this->post_model->get_post_slug(urldecode($slug));
        $this->load->view('blog/post',$data);
    }
}
