<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ValidationBlog extends CI_Controller {

	public function __construct(){
		parent::__construct();
		//loading language
		$this->load->helper('language');
        $this->load->library('Language');
        $this->language->set();
        $this->load->model('post_model');
        $this->load->model('imagenes_model');
        $this->load->library('Acl');
        if (!$this->acl->logged_in())
            redirect('/administrador');
        
        //End Language
	}
	public function index(){
		echo json_encode("No directed scripts allowed");
	}

    /**
    *@author Heriberto Martinez
    *Metodo que guarda el post generado en la tabla de post y en la tabla de imagenes la imagen referente al post
    **/
	public function save_post(){
        date_default_timezone_set('America/Mexico_City'); // CDT
        $path = "assets/blogs/";
        $current_date = date('YmdHis');
        $config['upload_path']          = $path;
        $config['allowed_types']        = 'jpg|png';
       /** $config['max_size']             = 1000;
        $config['max_width']            = 1004;
        $config['max_height']           = 600; **/
		$config['max_size']             = 1000;
        $config['max_width']            = 1440;
        $config['max_height']           = 810;
        $config['file_name']           = $this->session->usuario->usuario_id."_".$current_date;
        $this->load->library('upload', $config);
		if($this->input->server('REQUEST_METHOD') == 'POST'){
            $post['post_title'] = $this->input->post("txtTitle");
            $post['post_slug'] =  str_replace(' ', '-',$post['post_title']);
	    $ers = array('?', ',', '(','-','.' ,':', ')' );
			$post['post_slug'] =  strtolower(str_replace($ers, '-',$post['post_title']));
            $post['post_intro'] = $this->input->post("txtIntro");
            $post['post_content'] = $this->input->post("txtMensaje");
            $post['post_autor'] = $this->session->usuario->usuario_id;
            $post['post_date'] = date('Y-m-d H:i:s');
            $post['active'] = 0;
            $imagenPost =  $config["file_name"];
            if($this->upload->do_upload('fileImagen')){
                $imagen['nombre'] = $post['post_title'];
                $imagen['ubicacion']= $path.$this->upload->data('file_name'); 
                $idpost = $this->post_model->insert_post($post);
                $imagen['post_id']= $idpost;
                $idimagen = $this->imagenes_model->insert_imagen($imagen);
                if($idimagen > 0 && $idpost > 0 ){
                    echo "success";
                }else{
                    echo "no se pudo crear tu post";
                }
            }else{
                $error = array('error' => $this->upload->display_errors());
                echo $error['error'];
            }
        }else{
            redirect("/dashboard");
        }

	}

     /**
    *@author Heriberto Martinez
    *Metodo actualiza un post generado con anterioridad el post generado en la tabla de post 
    **/
    public function update_post(){
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $post_id = $this->input->post("txtId");
            $post['post_title'] = $this->input->post("txtTitle");
            $post['post_slug'] =  str_replace(' ', '-', $post['post_title']);
	    $ers = array('?', ',', '(','-','.' ,':','+', ')' );
			$post['post_slug'] =  strtolower(str_replace($ers, '-',$post['post_title']));
            $post['post_intro'] = $this->input->post("txtIntro");
            $post['post_content'] = $this->input->post("txtMensaje");
            $post['post_autor'] = $this->session->usuario->usuario_id;
            $post['post_date'] = date('Y-m-d H:i:s');
            #$post['active'] = $this->input->post("txtActive") = 1 ?  1 : 0;
            $idpost = $this->post_model->update_post($post_id,$post);
            if($idpost){
                    echo "success";
                }else{
                    echo "no se pudo Actualizar";
                }
        }else{
            redirect("/dashboard");
        }
    }

    /**
    *@author Heriberto Martinez
    *Metodo que realiza la eliminacion de un post al recibir la solicitud 
    **/
    public function delete_post($post_id){
        try{
            $imagen =  $this->imagenes_model->get_imagen($post_id);
            $this->post_model->delete_post($post_id);
            
            if(file_exists($imagen["ubicacion"]) == TRUE){
                unlink($imagen["ubicacion"]);
                $this->imagenes_model->delete_imagen($post_id);
                echo "Eliminado";            
            }else{
                echo "Error: I00NFD-1";            
                #echo base_url().$imagen["ubicacion"];
            }
        }catch(Exception $e){
            echo 'Error al eliminar : '+ $e->getMessage();
        }
    }
    /**
    *@author Heriberto Martinez 
    *Metodo actualiza el estado de los post publicados en el blog
    **/
    public function aprobar(){
        try{
        $post_id = $this->input->post("post_id");
        $aprobar = $this->input->post("aprobar");
        $post["active"]=$aprobar;
        $this->post_model->update_post($post_id,$post);
            if($aprobar == 1){
                echo "Aprobado!";
            }else{
                echo "No aprobado!";
            }
        }catch(Exception $e){
                echo "Error : " + $e->getMessage();
        }
    }


    /**
    *@author Heriberto Martinez 
    *Metodo actualiza la imagen deque esta asocciada a un post
    **/
    public function update_imagen(){
        #configuracion requerida para la carga de imagenes
        date_default_timezone_set('America/Mexico_City'); // CDT
        $path = "assets/blogs/";
        $current_date = date('YmdHis');
        $config['upload_path']          = $path;
        $config['allowed_types']        = 'jpg|png';
      /*   $config['max_size']             = 1000;
        $config['max_width']            = 824;
        $config['max_height']           = 304; */
		  $config['max_size']             = 1000;
        $config['max_width']            = 1440;
        $config['max_height']           = 810;
        $config['file_name']           = $this->session->usuario->usuario_id."_".$current_date;
        $this->load->library('upload', $config);
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $post_id = $this->input->post("post_id");
            $imagen_eliminar = $this->input->post("imagen");
            
            if($this->upload->do_upload('fileImagen')){
                $imagen['ubicacion']= $path.$this->upload->data('file_name'); 
                $resultado = $this->imagenes_model->update_imagen($post_id,$imagen);
                if($resultado){
                    if(file_exists($imagen_eliminar)){
                        unlink($imagen_eliminar);
                        echo $imagen['ubicacion'];
                    }else{
                        echo "Error Code:I00NFD";#no se elimino la imagen
                    }
                }else{
                    echo "Error al cargar la imagen Code:I00NFU";#nose cargo la imagen al servidor
                }
            }else{
                $error = array('error' => $this->upload->display_errors());
                echo $error['error']; #Error no controlado
            }
        }else{
            redirect("/dashboard");
        }


    }
}
