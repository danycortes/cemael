<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->tpl = array(); //for passing all data to the view
		//loading language
		$this->load->helper('language');
        $this->load->library('Language');
        //End Language
        $this->load->library('Tools');
        $this->load->library('Acl');
        $this->load->model('usuarios_model');
        $this->language->set();
        if (!$this->acl->logged_in())
            redirect('/administrador');
        $this->tpl['usuario'] = $this->session->usuario;
	}

    /**
    *@author Heriberto Martinez
    *redirecciona a la pagina principal para los usuarios
    **/
	public function search(){
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $email = $this->input->post("email");
            $usuario = $this->usuarios_model->getUsuarioEmail($email);
            echo json_encode($usuario);
        }
	}

    /**
    *@author Heriberto Martinez
    *redirecciona a la pagina principal para los usuarios
    **/
    public function create(){
        $respuesta_usuario;
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $perfil = $this->input->post("txtPerfil");
            $usuario['email'] = $this->input->post("txtEmail");
            $usuario['nombre'] = $this->input->post("txtNombre");
            $usuario['apodo'] = $this->input->post("txtApodo");
            $usuario['password'] = $this->acl->create_password();
            if($this->usuarios_model->checkEmail($usuario['email'])){ 
                $respuesta =  $this->usuarios_model->insert_usuario($usuario);
                if($respuesta > 0){
                    $usuario_rol['usuario_id']= $respuesta;
                    $usuario_rol['rol_id']= $perfil;
                    $respuesta_perfil = $this->usuarios_model->insert_permission($usuario_rol);
                    if($respuesta_perfil){
                        $respuesta_usuario['status'] = "success";
                        $respuesta_usuario['mensaje'] = "el usuario se genero correctamente.";                    
                    }else{
                        $respuesta_usuario['status'] = "error";
                        $respuesta_usuario['mensaje'] = "Se genero un usuario con errores Reporte el error";
                    }
                }else{
                    $respuesta_usuario['status'] = "error";
                    $respuesta_usuario['mensaje'] = "Error al crear el usuario.";
                }
            }else{
                  $respuesta_usuario['status'] = "error";
                    $respuesta_usuario['mensaje'] = "El email ya se encuentra en la base de datos.";
            }
        }else{
            $respuesta_usuario['status'] = "error";
            $respuesta_usuario['mensaje'] = "Bad request";
        }
         echo json_encode($respuesta_usuario);
    }

    public function edit(){
        $respuesta_usuario;
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $perfil = $this->input->post("txtPerfil_edit");
            $usuario_id = $this->input->post("txtUsuario_id_edit");
            $usuario['email'] = $this->input->post("txtEmail_edit");
            $usuario['nombre'] = $this->input->post("txtNombre_edit");
            $usuario['apodo'] = $this->input->post("txtApodo_edit");
            $respuesta =  $this->usuarios_model->update_usuario($usuario_id,$usuario);
            if($respuesta > 0){
                $respuesta_perfil = $this->usuarios_model->update_permission($usuario_id,$perfil);
                if($respuesta_perfil){
                    $respuesta_usuario['status'] = "success";
                    $respuesta_usuario['mensaje'] = "El usuario se actualizo correctamente.";
                }else{
                    $respuesta_usuario['status'] = "error";
                    $respuesta_usuario['mensaje'] = "Se actualizo el usuario con errores Reporte el error";
                }
            }else{
                $respuesta_usuario['status'] = "error";
                $respuesta_usuario['mensaje'] = "Error al actualizar el usuario.";
            }
        }else{
            $respuesta_usuario['status'] = "error";
            $respuesta_usuario['mensaje'] = "Bad request";
        }
         echo json_encode($respuesta_usuario);
    }       


    public function delete(){
        $respuesta_usuario;
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $usuario_id = $this->input->post("txtUsuario_id_del");
            if($this->usuarios_model->delete_permission($usuario_id) > 0){
                $respuesta = $this->usuarios_model->delete_usuario($usuario_id);
                if($respuesta > 0){
                    $respuesta_usuario['status'] = "sucess";
                    $respuesta_usuario['mensaje'] = "Se elimino el usuario.";
                }else{
                    $respuesta_usuario['status'] = "error";
                    $respuesta_usuario['mensaje'] = "Error  al eliminar el usuario.";
                }
            }else{
                $respuesta_usuario['status'] = "error";
                $respuesta_usuario['mensaje'] = "Error al eliminar permisos";
            }   
        }else{
            $respuesta_usuario['status'] = "error";
            $respuesta_usuario['mensaje'] = "Bad request!";
        }
         echo json_encode($respuesta_usuario);   
    }
}