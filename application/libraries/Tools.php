<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tools
{

    function __construct($config = array())
    {
        $this->_ci =& get_instance();
        log_message('debug', 'Language Class Initialized');
    }

    public function setPage($pagina){
               
        $pagina_activa = array(
            'todo_blogs_active'=>'', 
            'mis_blogs_active' =>'', 
            'escribir_blogs_active' => '',
            'editar_blogs_active'=>'',
            'eliminar_blogs_active'=>'',
            'usuarios_blogs_active'=>'',
            'perfil_blogs_active'=>'',
            'sales_active'=>'',
            'reports_active'=>'',
            'partners_active'=>'');

        switch ($pagina) {
            case "todo_blogs":
                $pagina_activa["todo_blogs_active"] = "active";
                break;
            case "mis_blogs":
                $pagina_activa["mis_blogs_active"] = "active";
                break;
            case "escribir_blogs":
                $pagina_activa["escribir_blogs_active"] = "active";
                break;
            case "editar_blogs":
                $pagina_activa["editar_blogs_active"] = "active";
                break;
            case "usuarios_blogs":
                $pagina_activa["usuarios_blogs_active"] = "active";
                break;
            case "perfil_blogs":
                $pagina_activa["perfil_blogs_active"] = "active";
                break;
            case 'eliminar_blogs':
                $pagina_activa["eliminar_blogs_active"] = "active";
                break;
            case 'partners':
                $pagina_activa["partners_active"] = "active";
                break;
            case 'sales':
                $pagina_activa["sales_active"] = "active";
                break;
            case 'reports':
                $pagina_activa["reports_active"] = "active";
                break;

        }
        #print_r($pagina_activa);exit();
        return $pagina_activa;
        

    }

}