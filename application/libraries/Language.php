<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Language
{

    function __construct($config = array())
    {
        $this->_ci =& get_instance();
        log_message('debug', 'Language Class Initialized');
    }

    function set(){
            $contract_name = "";

            // set browser lang to fr
            $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

            if(!isset($_COOKIE['language'])){
                if($browser_lang=="fr")
                    {setcookie('language', 'french', null, '/');  $_COOKIE['language']='french';}
                else if($browser_lang=="en")
                    {setcookie('language', 'english', null, '/');$_COOKIE['language']='english';}
                else if($browser_lang=="es")
                    {setcookie('language', 'spanish', null, '/');$_COOKIE['language']='spanish';}
                else{setcookie('language', 'french', null, '/');  $_COOKIE['language']='french';}
            }
                        
           else{
                setcookie('language', $_COOKIE['language'], null, '/');
            }
            //$this->_ci->lang->load("app", $_COOKIE['language']);//para tomar el idioma dependiendo la region 
            $this->_ci->lang->load("app", 'spanish');
            /*if($_SESSION['contract_id']==1) //intact
                $contract_name = "intact";
            else if($_SESSION['contract_id']==2) //belair
                $contract_name = "belair";
            $this->_ci->lang->load($contract_name, $_COOKIE['language']);*/

    }

}