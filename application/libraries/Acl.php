<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acl{
	public function __construct(){
        $this->_ci =& get_instance();
		$this->_ci->load->helper(array('cookie', 'language','url'));
		$this->_ci->load->library('session');
		$this->_ci->load->model('Usuarios_model');
	}

	public function login($user, $password){
		$usuario = $this->_ci->usuarios_model->getUsuarioLogin($user, $password);		
		$permiso = $this->_ci->usuarios_model->getUsuarioPermisos($usuario->usuario_id);
		if($usuario->email == $user){
			$this->_ci->session->set_userdata('usuario',$usuario);
			$this->_ci->session->set_userdata('permiso',$permiso);
			$this->_ci->session->set_userdata('logged_in',TRUE);
			return true ;
		}else{
			return false;
		}	
		
	}
	/**
	*logged_in
	*@return bool
	*@author Heriberto
	**/
	public function logged_in(){
		return (bool) $this->_ci->session->logged_in;
	}

	public function create_password(){
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$length = 8;
		$len = strlen($chars);
		$pw = '';
		for ($i=0;$i<$length;$i++)
		        $pw .= substr($chars, rand(0, $len-1), 1);
		$pw = str_shuffle($pw);
		return $pw;
	}

	public function logout()
	{
		$this->_ci->session->unset_userdata('usuario');
		$this->_ci->session->unset_userdata('logged_in');

		//Destroy the session
		$this->_ci->session->sess_destroy();

		#$this->set_message('logout_successful');
		return TRUE;
	}
}