<?php
class Post_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }
    /**
    *@author Heriberto Martinez
    *devuelve todos los posts haciendo una paginacion de los posts
    **/
    function get_posts($number = 10, $start = 0){
        $query =$this->db->query(" SELECT post.post_id , post.post_title,post.post_slug, post.post_intro, post.post_content ,usuarios.nombre, post.post_date ,ubicacion from  post left join usuarios on post.post_autor = usuarios.usuario_id 
            left join imagenes on post.post_id = imagenes.post_id where post.active = 1 order by post.post_date 
            desc limit ".$number." offset ".$start);
        return $query->result_array();
    }
    #cuenta el numero de post existentes para paginarlos
    function get_post_count(){
        $query = $this->db->query("SELECT post_id FROM post where active = 1");
        $num = $query->num_rows();
        //print_r($num);exit();
        return $num;
    }
    #seleccciona un post con el id recibido
    function get_post($post_id)
    {
      $query =$this->db->query(" SELECT post.post_id, post.post_title, post.post_intro, post.post_content ,usuarios.nombre, post.post_date, imagenes.ubicacion from  post left join usuarios on post.post_autor = usuarios.usuario_id
            left join imagenes on post.post_id = imagenes.post_id
            where post.post_id = ".$post_id);
        return $query->first_row('array');
    }

    #seleccciona un post en base al slug seleccionado
    function get_post_slug($slug)
    {
      $query =$this->db->query(" SELECT post.post_id, post.post_slug, post.post_title, post.post_intro, post.post_content ,usuarios.nombre, post.post_date, imagenes.ubicacion from  post left join usuarios on post.post_autor = usuarios.usuario_id
            left join imagenes on post.post_id = imagenes.post_id
            where post.post_slug = '".$slug."'");
        return $query->first_row('array');
    }

    #seleccciona un post con el id recibido
    function get_posts_user($user_id)
    {
        $query =$this->db->query(" SELECT post.post_id , post.post_title, post.post_intro, post.post_content ,usuarios.nombre, post.post_date, post.active from  post 
            left join usuarios on post.post_autor = usuarios.usuario_id
            where post.post_autor = ".$user_id);
        return $query->result_array();
    }

        #seleccciona todos los post para el admin
    function get_posts_admin($user_id)
    {
        $query =$this->db->query(" SELECT post.post_id , post.post_title, post.post_intro, post.post_content ,usuarios.nombre, post.post_date, post.active from  post left join usuarios on post.post_autor = usuarios.usuario_id order by post.post_date desc");
        return $query->result_array();
    }


    function insert_post($data)
    {
        $this->db->insert('post',$data);
        return $this->db->insert_id();
    }
    
    function update_post($post_id, $data)
    {
        $this->db->where('post_id',$post_id);
        $this->db->update('post',$data);
        $result = $this->db->affected_rows() > 0 ?  TRUE :  FALSE;
        return $result;
    }
    
    function delete_post($post_id)
    {
        $this->db->where('post_id',$post_id);
        $this->db->delete('post');
    }
}
