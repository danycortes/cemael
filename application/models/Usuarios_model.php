<?php
class Usuarios_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }

    #Selecciona usuario por el email y el password
    function getUsuarioLogin($usuario, $password){
    	$query =$this->db->query("SELECT usuarios.usuario_id, usuarios.email, usuarios.nombre, usuarios.apodo,roles.rol_id  FROM usuarios LEFT JOIN usuario_rol ON usuario_rol.usuario_id = usuarios.usuario_id LEFT JOIN roles ON roles.rol_id= usuario_rol.rol_id WHERE email = '".$usuario."' AND password = '".$password."'");
        return $query->first_row();
    }
    #Devuelve los permisos de los usuarios por el id de usuario recibido
    function getUsuarioPermisos($usuario_id){
        $query =$this->db->query("SELECT usuario_rol.rol_id , roles.rol_nombre FROM usuario_rol 
            LEFT JOIN roles ON roles.rol_id = usuario_rol.rol_id 
            WHERE usuario_id = '".$usuario_id."'");
        return $query->first_row();       
    }
    #Trae usuario por el id
    function getUsuario($usuario_id){
        $query =$this->db->query("SELECT roles.rol_nombre, usuarios.email, usuarios.nombre, usuarios.apodo FROM usuarios LEFT JOIN usuario_rol ON usuario_rol.usuario_id = usuarios.usuario_id 
            LEFT JOIN roles ON usuario_rol.rol_id = roles.rol_id 
            WHERE  usuarios.usuario_id ='".$usuario_id."'");
        return $query->first_row();       
    }
    #Trae usuarios por el email
    function getUsuarioEmail($email){
        $query =$this->db->query("SELECT usuarios.usuario_id,roles.rol_id, roles.rol_nombre, usuarios.email, usuarios.nombre, usuarios.apodo, usuarios.email
        FROM usuarios LEFT JOIN usuario_rol ON usuarios.usuario_id = usuario_rol.usuario_id 
        LEFT JOIN roles ON usuario_rol.rol_id = roles.rol_id
        WHERE  usuarios.email ='".$email."'");
        return $query->first_row();       
    }
    #Devuelve la lista de usuarios de la base de datos
    function getUsuarios(){
        $query =$this->db->query("SELECT usuarios.usuario_id, roles.rol_nombre, usuarios.email, usuarios.nombre, usuarios.apodo, usuarios.password FROM usuarios LEFT JOIN usuario_rol ON usuario_rol.usuario_id = usuarios.usuario_id 
            LEFT JOIN roles ON usuario_rol.rol_id = roles.rol_id");
        return $query->result_array();       
    }

    #Permite la insercion de usuarios en el sistema 
    function insert_usuario($data){
        $this->db->insert('usuarios',$data);
        return $this->db->insert_id();   
    }
    #Permite la actualizacion de un usuario
    function update_usuario($usuario_id, $data){
        $this->db->where('usuario_id',$usuario_id);
        $this->db->update('usuarios',$data);
        return $this->db->affected_rows() >= 0;
    }
    #Permite la eliminacion de un usuario
    function delete_usuario($usuario_id){
        $this->db->where('usuario_id',$usuario_id);
        $this->db->delete('usuarios');
        return $this->db->affected_rows() >= 0;
    }
    #permite checar si el usuario ya fue registrado basado en el email
    function checkEmail($email){
        $this->db->where('email',$email);
        $query = $this->db->get('usuarios');
        if ($query->num_rows() > 0){
            return false;
        }
        else{
            return true;
        }
    }
    //Permission section
    function insert_permission($data){
        $this->db->insert('usuario_rol', $data);
       return $this->db->affected_rows() > 0;
    }
    #Permite la actualizacion de un usuario
    function update_permission($usuario_id, $perfil){
        print_r($this->db->query('UPDATE usuario_rol SET rol_id ='.$perfil.' WHERE  usuario_id = '.$usuario_id));
        return $this->db->affected_rows() >= 0;
    }
    #Permite la eliminacion de un usuario
    function delete_permission($usuario_id){
        try {
            $this->db->where('usuario_id',$usuario_id);
            $this->db->delete('usuario_rol');
            //var_dump($this->db->affected_rows());
            return $this->db->affected_rows() > 0;    
        } catch (Exception $e) {
            var_dump($e->message());exit();         
        }     
    }


}