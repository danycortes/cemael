<?php
class Partners_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }

    #Trae partner by email 
    function getPartnerEmail($email){
        $query =$this->db->query("SELECT partnerId, rfc ,razonSocial, email, responsable, fechaCreacion, estatus FROM partners WHERE  email ='".$email."'");
        return $query->first_row();       
    }

    #Trae partner por id
    function getPartner($partner_id){
        $query =$this->db->query("SELECT partnerId, rfc ,razonSocial, email, responsable, fechaCreacion, estatus FROM partners  
            WHERE  partnerId ='".$partner_id."'");
        return $query->first_row();       
    }
    #Devuelve la lista de usuarios de la base de datos
    function getPartners(){
        $query =$this->db->query("SELECT * FROM partners");
        return $query->result_array();       
    }

    #Permite la insercion de usuarios en el sistema 
    function insert_partner($data){
        $this->db->insert('partners',$data);
        return $this->db->insert_id();   
    }
    #Permite la actualizacion de un usuario
    function update_partner($usuario_id, $data){
        $this->db->where('partnerId',$usuario_id);
        $this->db->update('partners',$data);
        return $this->db->affected_rows() >= 0;
    }
    #Permite la eliminacion de un usuario
    function delete_partner($usuario_id){
        $this->db->where('partnerId',$usuario_id);
        $this->db->delete('partners');
        return $this->db->affected_rows() >= 0;
    }
    #permite checar si el usuario ya fue registrado basado en el email
    function checkEmail($email){
        $this->db->where('email',$email);
        $query = $this->db->get('partners');
        if ($query->num_rows() > 0){
            return false;
        }
        else{
            return true;
        }
    }

}