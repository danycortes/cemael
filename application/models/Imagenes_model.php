<?php
class Imagenes_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }
    #seleccciona una imagen con el id recibido
    function get_imagen($post_id)
    {
       $query =$this->db->query("SELECT nombre , ubicacion FROM  imagenes WHERE post_id = ".$post_id);
        return $query->first_row('array');
    }

    function insert_imagen($data)
    {
        $this->db->insert('imagenes',$data);
        return $this->db->insert_id();
    }
    
    function update_imagen($post_id, $data){
        $this->db->where('post_id',$post_id);
        $this->db->update('imagenes',$data);
        $result = $this->db->affected_rows() > 0 ? TRUE : FALSE;
        return $result;
    }
    #Elimina las imagen relacionada con el post 
    function delete_imagen($post_id)
    {
        $this->db->where('post_id',$post_id);
        $this->db->delete('imagenes');
    }
}
