<?php
class Catalogos_model extends CI_Model{

    public function __construct(){
        $this->load->database();
        parent::__construct();
    }

    /**
    *@author Heriberto Martinez
    **/
    function getRoles(){
    	$query =$this->db->query("SELECT rol_id, rol_nombre FROM roles");
    	return $query->result_array();
    }

    /**
    *@author Heriberto Martinez
    **/
    function getPartners(){
        $query =$this->db->query("SELECT partnerId, razonSocial FROM partners");
        return $query->result_array();
    }
}
