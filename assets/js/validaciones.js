$(document).ready(function () {
    $("#form").validate({
        rules: {
            "name": {
                required: true,
                minlength: 5
            },
            "telefono":{
            		required:true,
                minlength: 10
            },
            "email": {
                required: true,
                email: true
            }
        },
        messages: {
            "name": {
                required: "Please, enter a name"
            },
            "email": {
                required: "Please, enter an email",
                email: "Email invalido"
            }
        },
        submitHandler: function (form) {
        // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });

});