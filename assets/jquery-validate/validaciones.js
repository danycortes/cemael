$(document).ready(function () {
    $("#formularioUno").validate({
        rules: {
            "name": {
                required: true,
                minlength: 5
            },
            "telefono":{
            	required:true,
                minlength: 10
            },
            "email": {
                required: true,
                email: true
            }
        },
        messages: {
            "name": {
                required: "Ingrese su Nombre",
				 minlength: "Nombre Invalido"
            },
			"telefono":{
            	required:"ingrese su Telefono",
                minlength: "Telefono Invalido"
            },
            "email": {
                required: "Ingrese su Email",
                email: "Email Invalido"
            }
        },
		 // submitHandler: enviaruno
      
	
        // // $.ajax({
            // // url: "registro.php",
            // // data:'Nombre2='+$("#name").val()+'&Email2='+
            // // $("#email").val()+'&Telefono2='+
            // // $("telefono").val(),
            // // type: "POST",
            // // success: function(result){
			// // alert(result);}
        // // });
    

	   // // function (form) {  
			// // alert('valid form submitted'); // for demo
            // // return false; // for demo
			// // // }

    });
	// function enviaruno(){
		  // var data = new $("#formularioUno").serialize();
        // $.ajax({  
          // type : 'POST',
          // data : data,
          // url  : "registro.php",
		  // success: function(result){
			// alert(result);}
		// })
	// }



	

	
	
	  $("#formulariodos").validate({
        rules: {
            "name": {
                required: true,
                minlength: 5
            },
            "telefono":{
            	required:true,
                minlength: 10
            },
            "email": {
                required: true,
                email: true
            }
        },
        messages: {
            "name": {
                required: "Ingrese su Nombre",
				 minlength: "Nombre Invalido"
            },
			"telefono":{
            	required:"Ingrese su Telefono",
                minlength: "Telefono Invalido"
            },
            "email": {
                required: "Ingrese su Email",
                email: "Email Invalido"
            }
        },
        // submitHandler: function (form) {
        // // for demo
            // alert('valid form submitted'+ name); // for demo
            // return false; // for demo
        // }
    });

	 $("#formulariotres").validate({
        rules: {
            "name": {
                required: true,
                minlength: 5
            },
            "telefono":{
            	required:true,
                minlength: 10
            },
            "email": {
                required: true,
                email: true
            }
        },
        messages: {
            "name": {
                required: "Ingrese su Nombre",
				 minlength: "Nombre Invalido"
            },
			"telefono":{
            	required:"Ingrese su Telefono",
                minlength: "Telefono Invalido"
            },
            "email": {
                required: "Ingrese su Email",
                email: "Email Invalido"
            }
        },
        // submitHandler: function (form) {
        // // for demo
            // alert('valid form submitted'); // for demo
            // return false; // for demo
		
        // }
    });
	
	 $("#formulariocuatro").validate({
        rules: {
            "name": {
                required: true,
                minlength: 5
            },
			 "email": {
                required: true,
                email: true
            },
			"edad":{
            	required:true,
                minlength: 1
            },
             "sexo": {
                required: true,
                minlength: 5
            },
            "estado": {
                required: true,
                minlength: 5
            },
            "escolaridad": {
                required: true,
                minlength: 5
            },
           
			 "ocupacion": {
                required: true,
                minlength: 5
            },
            "telefono":{
            	required:true,
                minlength: 10
            },
           
        },
        messages: {
            "name": {
                required: "Ingrese su Nombre",
				 minlength: "Nombre Invalido"
            },
			"email": {
                required: "Ingrese su Email",
                email: "Email Invalido"
            },
			"edad":{
            	required:"Ingrese su Edad",
                minlength: "Edad Invalido"
            },
             "sexo": {
                required: "Ingrese su sexo",
                minlength: "Sexo invalido"
                
            },
            "estado": {
                required: "Ingrese su estado civil",
                minlength: "Estado invalido"
            },
            "escolaridad": {
                required: "Ingrese su escolaridad",
                minlength: "Escolaridad invalida"
            },
           
            
            "ocupacion": {
                required: "Ingrese su Ocupacion",
				 minlength: "Ocupacion Invalido"
            },
			"telefono":{
            	required:"Ingrese su Telefono",
                minlength: "Telefono Invalido"
            },
            
        },
        // submitHandler: function (form) {
        // // for demo
            // alert('valid form submitted'); // for demo
            // return false; // for demo
        // }
    });
	$("#formulariocinco").validate({
        rules: {
            "name": {
                required: true,
                minlength: 5
            },
            "telefono":{
            	required:true,
                minlength: 10
            },
            "email": {
                required: true,
                email: true
            },
			"mensaje": {
                required: true,
                minlength: 10
            },
        },
        messages: {
            "name": {
                required: "Ingrese su Nombre",
				 minlength: "Nombre Invalido"
            },
			"telefono":{
            	required:"ingrese su Telefono",
                minlength: "Telefono Invalido"
            },
            "email": {
                required: "Ingrese su Email",
                email: "Email Invalido"
            },
			"mensaje": {
                required: "Ingrese su Mensaje",
				 minlength: "Mensaje Invalido"
            },
			
			
        },
        // submitHandler: function (form) {
        // // for demo
            // alert('valid form submitted'); // for demo
            // return false; // for demo
        // }
    });
	
	
});